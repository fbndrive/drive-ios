#import <UIKit/UIKit.h>
#import "UserData.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>
//#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UserData *userData;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CMMotionManager *coreMotionManager;

//@property (strong, nonatomic) FBSession *session;

- (void)userDataChangedWith:(UserData *)userData;

@end
