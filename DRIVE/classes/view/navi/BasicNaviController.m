#import "BasicNaviController.h"

@interface BasicNaviController ()

@end

@implementation BasicNaviController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fadeTheViewInWithDelay: (float) delay {
    self.view.hidden = NO;
    
    [UIView animateWithDuration: 0.5f
                          delay: delay
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.view.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

- (void)fadeTheViewOutWithDelay: (float) delay {
    [UIView animateWithDuration: 0.4f
                          delay: delay
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         self.view.hidden = YES;
                     }];
}

- (void)remove {
    [UIView animateWithDuration:.4
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [self willMoveToParentViewController:nil];
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                     }];
}

@end
