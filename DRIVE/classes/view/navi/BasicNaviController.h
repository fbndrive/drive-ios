#import <UIKit/UIKit.h>

@interface BasicNaviController : UINavigationController

- (void)fadeTheViewInWithDelay: (float) delay;
- (void)fadeTheViewOutWithDelay: (float) delay;
- (void)remove;

@end
