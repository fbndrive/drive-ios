#import "AppDataManager.h"
#import "ApplicationBackgroundView.h"
#import "Constants.h"

@implementation ApplicationBackgroundView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self drawUI];
    }
    return self;
}

- (void) refresh {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIImage *img;
    
    if(adm.driveCharacteristic == DriveCharacteristic_ECO) {
        img = [UIImage imageNamed:@"bg-eco"];
    } else if(adm.driveCharacteristic == DriveCharacteristic_NORMAL) {
        img = [UIImage imageNamed:@"bg-neutral"];
    } else {
        img = [UIImage imageNamed:@"bg-v2"];
    }

    if(_backgroundView) {
        [_backgroundView removeFromSuperview];
        _backgroundView = nil;
    }
    
    _backgroundView = [[UIImageView alloc] initWithImage:img];
    
    [self addSubview: _backgroundView];
    //self.frame = CGRectMake(0, 0, img.size.width, img.size.height);
    
    //NSLog(@">> %@", img);
}

- (void) drawUI {
    [self refresh];
}

@end
