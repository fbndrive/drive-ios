#import "AppDataManager.h"
#import "CenteredSmallLabelHugeValueLabelValue.h"
#import "Constants.h"

@implementation CenteredSmallLabelHugeValueLabelValue

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 90];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
    self.valueLabel.textAlignment = NSTextAlignmentCenter;
}

- (void) applyFormatLabel {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_regularFace size: 15];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.labelLabel.font = fontFace;
    self.labelLabel.textColor = fontColor;
    self.labelLabel.textAlignment = NSTextAlignmentCenter;
}

- (void) sizeMe {
    [self.valueLabel sizeToFit];
    [self.labelLabel sizeToFit];
    
    self.labelLabel.frame = CGRectMake(0, 0, self.labelWidth, self.labelLabel.frame.size.height);
    self.valueLabel.frame = CGRectMake(0, self.labelLabel.frame.size.height - 14, self.labelWidth, self.valueLabel.frame.size.height);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.labelWidth, self.valueLabel.frame.size.height + self.labelLabel.frame.size.height);
}

- (void) drawUI {
    [super drawUI];
    
    self.valueLabel.layer.shadowRadius = 10.0f;
    self.valueLabel.layer.shadowOpacity = .6f;
}


@end
