#import "BasicChartView.h"

@interface PieChart : BasicChartView

@property (nonatomic) CGSize size;
@property (nonatomic) float percent;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) float value;

- (void) refresh;
- (void) arrange;
- (void) animateToPercent: (float) percent;

@end
