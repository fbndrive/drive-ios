#import "RegLabelBoldValueLabelValueView.h"

@interface CenteredLabelsLabelValueView : RegLabelBoldValueLabelValueView

@property (nonatomic) int labelWidth;

@end
