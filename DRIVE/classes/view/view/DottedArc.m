#import "DottedArc.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface DottedArc ()

@end

@implementation DottedArc

@synthesize finalAngle = _finalAngle;
@synthesize dotColor = _dotColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat lengths[2];
    lengths[0] = 0;
    lengths[1] = 1 * 4;
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, 1);
    CGContextSetLineDash(context, 0.0f, lengths, 2);
    CGContextSetStrokeColorWithColor(context,
                                     _dotColor.CGColor);
    CGContextMoveToPoint(context, 2, 2);
    CGContextAddArc(context, 0, 82, 80, -M_PI/2 , _finalAngle, 0);
    
    CGContextStrokePath(context);
}

@end
