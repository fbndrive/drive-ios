#import "Constants.h"
#import "MainMenuItemList.h"
#import "MainMenuOpenCloseButton.h"
#import "MainMenuView.h"

@implementation MainMenuView

@synthesize isOpened = _isOpened;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self drawUI];
        
        _isOpened = NO;
        _itemList.alpha = 0;
        self.frame = _openCloseButton.frame;
    }
    return self;
}

- (void) refresh {
    [_openCloseButton refresh];
    [_itemList refresh];
}

- (void)toggleState {
    if(_isOpened) {
        _isOpened = NO;
        self.frame = _openCloseButton.frame;
    } else {
        _isOpened = YES;
        self.frame = _itemList.frame;
    }
    
    NSNotification *mainMenuNotification = [[NSNotification alloc] initWithName:MainMenu_toggle object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:mainMenuNotification];
}

/*- (void)refreshView {
    int talpha;
    
    if(_isOpened) {
        talpha = 1;
        
    } else {
        talpha = 0;
        self.frame = _openCloseButton.frame;
    }
    
    [UIView animateWithDuration:.5
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _itemList.alpha = talpha;
                     }
                     completion:^(BOOL finished) {
                         NSNotification *mainMenuNotification;
                         
                         if (talpha == 1) {
                             mainMenuNotification = [[NSNotification alloc] initWithName:MainMenu_opened object:self userInfo:nil];
                         } else {
                             mainMenuNotification = [[NSNotification alloc] initWithName:MainMenu_closed object:self userInfo:nil];
                         }
                         
                         [[NSNotificationCenter defaultCenter] postNotification:mainMenuNotification];
                     }];
}
*/

- (void)openCloseButtonClick: (id) target {
    [self toggleState];
}

- (void) drawUI {
    MainMenuOpenCloseButton *openCloseButton = [[MainMenuOpenCloseButton alloc] init];
    MainMenuItemList *itemList = [[MainMenuItemList alloc] initWithFrame:self.frame];
    
    [self addSubview:itemList];
    [self addSubview:openCloseButton];
    
    [openCloseButton addTarget:self action:@selector(openCloseButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    _itemList = itemList;
    _openCloseButton = openCloseButton;
    
    //self.backgroundColor = [UIColor redColor];
}

@end
