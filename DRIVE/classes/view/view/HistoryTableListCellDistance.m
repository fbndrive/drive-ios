#import "AppDataManager.h"
#import "Constants.h"
#import "DriveData.h"
#import "HistoryTableListCellDistance.h"

@implementation HistoryTableListCellDistance

-(void) applyFormatting {
    self.font = [UIFont fontWithName: Font_regularFace size: 18];
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void) fillWithData: (DriveData *) data {
    AppDataManager *adm = [AppDataManager appDataManager];
    int distance = (int)[adm convertDistanceToUserUnit: data.distance];
    
    NSString *unit = [adm getUserDistanceUnitLabel];
    NSString *distanceString = [NSString stringWithFormat:@"%d %@", distance, [unit uppercaseString]];
    
    [self setTitle: distanceString];
}


/*-(void) fillWithData: (DriveData *) data {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setGroupingSeparator:@""];
    [numberFormatter setMinimumFractionDigits:0];
    [numberFormatter setMaximumFractionDigits:0];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    //[self setTitle: [numberFormatter stringFromNumber: [NSNumber numberWithInt:data.experienceGained]]];
    [self setTitle: @"test title"];
}

-(void) applyFormatting {
    self.font = [UIFont fontWithName: Font_regularFace size: 34];
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}*/

@end
