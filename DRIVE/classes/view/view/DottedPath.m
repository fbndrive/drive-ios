#import <CoreLocation/CoreLocation.h>
#import "DottedPath.h"

@interface DottedPath ()

@property (nonatomic) CLLocationCoordinate2D previousCoordinates;
@property (nonatomic) CGPoint shift;
@property (nonatomic, retain) NSMutableArray *points;

@end

@implementation DottedPath

@synthesize dotColor = _dotColor;
@synthesize scale = _scale;
@synthesize centerPoint = _centerPoint;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _scale = 10;
        self.centerPoint = CGPointZero;
        
    }
    
    return self;
}

- (void) setScale:(float)scale {
    _scale = scale;
}

- (void) setCenterPoint: (CGPoint)centerPoint {
    //_dotStartgingPoint = CGPointMake(75, 125);
    _centerPoint = centerPoint;
    
    _dotEdgeRect = UIEdgeInsetsMake(_centerPoint.y - 40, _centerPoint.x - 30, _centerPoint.y + 40, _centerPoint.x + 30);
    
    [self reset];
}

- (void) reset  {
    _shift = CGPointZero;
    _previousCoordinates = CLLocationCoordinate2DMake(0, 0);
    
    _points = [[NSMutableArray alloc] init];
    [_points addObject:[NSValue valueWithCGPoint: _centerPoint]];
}

- (void) pushWithCoordinates: (CLLocationCoordinate2D) coordinate  {
    if(_previousCoordinates.latitude == 0) {
        _previousCoordinates = coordinate;
        return;
    }
    
    //coordinate = CLLocationCoordinate2DMake(floor(coordinate.latitude*10000)/10000, floor(coordinate.longitude*10000)/10000);
    
    //CGPoint lastPoint = CGPointMake(_previousCoordinates.latitude, _previousCoordinates.longitude);
    //CGPoint newPoint = CGPointMake(coordinate.latitude, coordinate.longitude);
    
    // calculation of direction
    
    //float deltaY = coordinate.longitude - _previousCoordinates.longitude;
    //float deltaX = coordinate.latitude - _previousCoordinates.latitude;
    
    //float deltaY = newPoint.y - lastPoint.y;
    //float deltaX = newPoint.x - lastPoint.x;
    
    //float angle = atan2f(deltaY, deltaX) - M_PI/2;
    float distance = [[[CLLocation alloc] initWithLatitude:_previousCoordinates.latitude longitude:_previousCoordinates.longitude]
                      distanceFromLocation:[[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude]];
    
    //float pxDistance = distance / _scale;
    
    NSLog(@"distance %f,%f,%f,%f,%f", _previousCoordinates.latitude, _previousCoordinates.longitude, coordinate.latitude, coordinate.longitude, distance);
    //NSLog(@"angle: %f", angle);
    
    //CGPoint pt = [self newPointWithAngle:angle andDistance: pxDistance];
    //[self updateDrawShiftToPoint:pt];
    
    //_previousLocation = nil;
    //_previousLocation = coordinate;
    _previousCoordinates = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
    
    if([_points count] % 50 == 0) {
        //[self applyAndZeroShift];
    }
}

- (CGPoint) newPointWithAngle: (float) angle andDistance: (float) distance {
    CGPoint lastPointFromPath = [[_points lastObject] CGPointValue];
    //float radius = 2;
    
    //float newPointX = lastPointFromPath.x + radius * cos(angle);
    //float newPointY = lastPointFromPath.y + radius * sin(angle);
    
    float newPointX = lastPointFromPath.x + distance * cos(angle);
    float newPointY = lastPointFromPath.y + distance * sin(angle);
    
    CGPoint pointToAdd = CGPointMake(newPointX, newPointY);
    [_points addObject:[NSValue valueWithCGPoint:pointToAdd]];
    //NSLog(@"point: %f, %f, %f", pointToAdd.x, pointToAdd.y, distance);
    return pointToAdd;
}

- (void) applyAndZeroShift {
    NSLog(@"applyAndZeroShift!!!!");
    
    int i = -1;
    int q = (int)[_points count];
    CGPoint pt;
    
    while (++i < q) {
        pt = [[_points objectAtIndex:i] CGPointValue];
        pt.x += _shift.x;
        pt.y += _shift.y;
        
        [_points replaceObjectAtIndex:i withObject:[NSValue valueWithCGPoint:pt]];
    }
    
    _shift = CGPointMake(0, 0);
}

- (void) updateDrawShiftToPoint: (CGPoint) point {
    
    float xShift = 0;
    float yShift = 0;
    
    if(point.x < _dotEdgeRect.left) {
        xShift = _dotEdgeRect.left - point.x;
    }
    
    if(point.x > _dotEdgeRect.right) {
        xShift = _dotEdgeRect.right - point.x;
    }
    
    if(point.y < _dotEdgeRect.top) {
        yShift = _dotEdgeRect.top - point.y;
    }
    
    if(point.y > _dotEdgeRect.bottom) {
        yShift = _dotEdgeRect.bottom - point.y;
    }
    
    _shift = CGPointMake(xShift, yShift);
    //NSLog(@"pt: %f, %f --  shift: %f, %f", point.x, point.y, xShift, yShift);
}

- (void)drawRect:(CGRect)rect {
    int i;
    int pointsCount = (int)[_points count];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGPoint linePoints[pointsCount];
    for(i = 0; i < pointsCount; i++) {
        linePoints[i].x = [[_points objectAtIndex:i] CGPointValue].x + _shift.x;
        linePoints[i].y = [[_points objectAtIndex:i] CGPointValue].y + _shift.y;
    }
    
    CGFloat dotRadius = 2;
    
    CGFloat lengths[2];
    lengths[0] = 0;
    lengths[1] = 4;
    
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, 1);
    CGContextSetLineDash(context, 0.0f, lengths, 2);
    CGContextSetStrokeColorWithColor(context, _dotColor.CGColor);
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLines(context, linePoints, pointsCount);
    
    CGContextStrokePath(context);
    
    //CGRect rectangle = CGRectMake(linePoints[i].x-3,linePoints[i].y-3,80,80);
    CGRect rectangle = CGRectMake(linePoints[i-1].x - dotRadius, linePoints[i-1].y - dotRadius, dotRadius*2, dotRadius*2);
    
    CGContextSetLineWidth(context, 0);
    CGContextSetFillColorWithColor(context, _dotColor.CGColor);
    CGContextFillEllipseInRect(context, rectangle);
    
    CGContextStrokePath(context);
}

@end
