#import "BasicButton.h"
#import "Constants.h"
#import "MainMenuItemList.h"
#import "RegularFontLabel.h"

@implementation MainMenuItemList

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self drawUI];
    }
    return self;
}

- (void) refresh {
    int i = -1;
    int q = (int) [self.subviews count];
    
    UIView *menuItem;
    
    while (++i < q) {
        menuItem = [self.subviews objectAtIndex:i];
        
        if([menuItem isKindOfClass:[BasicButton class]]) {
            [(BasicButton *)menuItem refresh];
        } else if([menuItem isKindOfClass:[RegularFontLabel class]]) {
            [(RegularFontLabel *)menuItem refresh];
        }
    }
}

- (void)menuItemClick: (id) target {
    NSNotification *mainMenuItemClickNotification = [[NSNotification alloc] initWithName:MainMenu_itemClicked object:target userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:mainMenuItemClickNotification];
}

- (void) drawUI {
    
    NSArray *menuItems = [[NSArray alloc] initWithObjects:[NSArray arrayWithObjects:@"1", MainMenu_item_drive, nil], [NSArray arrayWithObjects:@"0", MainMenu_item_map, nil], [NSArray arrayWithObjects:@"1", MainMenu_item_history, nil], [NSArray arrayWithObjects:@"1", MainMenu_item_thropies, nil], [NSArray arrayWithObjects:@"0", MainMenu_item_friends, nil], [NSArray arrayWithObjects:@"1", MainMenu_item_logout, nil], nil];
    
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    int i = -1;
    int q = (int)[menuItems count];
    
    NSArray *menuItemData;
    
    RegularFontLabel *menuInactiveButton;
    BasicButton *menuButton;
    
    CGRect menuButtonFrame;
    
    float xpos = 0.0f;
    float ypos = 80.0f;
    
    while (++i < q) {
        menuItemData = [menuItems objectAtIndex:i];
        
        if([[menuItemData objectAtIndex:0] isEqualToString:@"1"]) {
            menuButton = [[BasicButton alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, 40)];
            [menuButton setTitle: [menuItemData objectAtIndex:1]];
            
            menuButtonFrame = menuButton.frame;
            xpos = floorf((screenFrame.size.width - menuButtonFrame.size.width) / 2);
            menuButton.frame = CGRectMake(xpos, ypos, menuButtonFrame.size.width, menuButtonFrame.size.height);
            
            [self addSubview:menuButton];
            
            [menuButton addTarget:self action:@selector(menuItemClick:) forControlEvents:UIControlEventTouchDown];
            ypos += menuButton.frame.size.height + 17;
        } else {
            menuInactiveButton = [[RegularFontLabel alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, 40)];
            menuInactiveButton.alpha = .5;
            [menuInactiveButton setTitle: [menuItemData objectAtIndex:1]];
            
            menuButtonFrame = menuInactiveButton.frame;
            xpos = floorf((screenFrame.size.width - menuButtonFrame.size.width) / 2);
            menuInactiveButton.frame = CGRectMake(xpos, ypos, menuButtonFrame.size.width, menuButtonFrame.size.height);
            
            [self addSubview:menuInactiveButton];
            
            ypos += menuInactiveButton.frame.size.height + 17;
        }

    }
    
    /*BasicButton *mapButton = [[BasicButton alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, screenFrame.size.height)];
    [loginButton setTitle:@"map"];
    
    CGRect buttonFrame = loginButton.frame;
    float buttonXPos = floorf((screenFrame.size.width - buttonFrame.size.width) / 2) - 2;
    loginButton.frame = CGRectMake(buttonXPos, screenFrame.size.height - 85, buttonFrame.size.width, buttonFrame.size.height);
    
    [loginButton animateIn];
    [loginButton addTarget:self action:@selector(loginButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    [self.view addSubview:loginButton];
    
    _loginButton = loginButton;*/
}

@end
