#import "AppDataManager.h"
#import "Constants.h"
#import "DriveExpLabelValueView.h"

@implementation DriveExpLabelValueView

- (void) setNumericValue:(float)numericValue {
    [super setNumericValue:floorf(numericValue)];
    
    NSString *sv = [self floatStringOfValue];
    [self setStringValue: sv];
}

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 40];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
}

- (void) sizeMe {
    [self.valueLabel sizeToFit];
    [self.labelLabel sizeToFit];
    
    
    self.valueLabel.frame = CGRectMake(self.labelLabel.frame.size.width - 2, 0, self.valueLabel.frame.size.width + 5, self.valueLabel.frame.size.height);
    self.labelLabel.frame = CGRectMake(0, self.valueLabel.frame.size.height - 16, self.labelLabel.frame.size.width, self.labelLabel.frame.size.height);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.valueLabel.frame.size.width + self.labelLabel.frame.size.width, self.valueLabel.frame.size.height + self.labelLabel.frame.size.height);
    
    //NSLog(@"frame width: %f", self.frame.size.width);
}

- (NSString *) floatStringOfValue {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setMaximumFractionDigits:0];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *commaString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat: self.numericValue]];
    
    return commaString;
}

@end
