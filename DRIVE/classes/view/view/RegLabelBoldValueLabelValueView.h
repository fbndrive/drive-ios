#import <UIKit/UIKit.h>
#import "BasicLabelValueView.h"

@interface RegLabelBoldValueLabelValueView : BasicLabelValueView

- (void) killMe;
- (void) sizeMe;
- (void) applyFormatValue;
- (void) applyFormatLabel;

-(void) randomBlink: (NSTimer *) timer;

@end
