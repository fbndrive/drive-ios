#import <UIKit/UIKit.h>

@interface TopEdgeSliderView : UIView

@property (nonatomic) float position;
@property (nonatomic, strong) UIColor *color;

@end
