#import <UIKit/UIKit.h>

@interface BasicButton : UIButton

-(void) animateIn;
-(void) animateOut;
-(void) setTitle:(NSString *)title;

-(void) refresh;
-(void) applyFormatting;

@end
