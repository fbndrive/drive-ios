#import "RMStaticMapView.h"

@interface DriveCustomMapView : RMStaticMapView

- (id)initWithFrame:(CGRect)frame mapID:(NSString *)mapID zoomWithLatitudeLongitudeBoundsSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast completionHandler:(void (^)(void))handler;

@end
