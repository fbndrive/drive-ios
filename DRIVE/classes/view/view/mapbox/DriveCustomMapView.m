#import "DriveCustomMapView.h"
#import "RMMapboxSource.h"

@implementation DriveCustomMapView

- (id)initWithFrame:(CGRect)frame mapID:(NSString *)mapID zoomWithLatitudeLongitudeBoundsSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast completionHandler:(void (^)(void))handler {
    
    if (!(self = [super initWithFrame:frame]))
        return nil;
    
    [self performCustomInitializationWithMapID:mapID zoomWithLatitudeLongitudeBoundsSouthWest:southWest northEast:northEast completionHandler:handler];
    
    return self;
}

- (void)performCustomInitializationWithMapID:(NSString *)mapID zoomWithLatitudeLongitudeBoundsSouthWest:(CLLocationCoordinate2D)southWest northEast:(CLLocationCoordinate2D)northEast completionHandler:(void (^)(void))handler {
    
    RMMapboxSource *tileSource = [[RMMapboxSource alloc] initWithMapID:(mapID ? mapID : kMapboxPlaceholderMapID) enablingDataOnMapView:self];
    self.tileSource = tileSource;
    
    //southWest = CLLocationCoordinate2DMake(southWest.latitude - 0.01f, southWest.longitude - 0.01f);
    //northEast = CLLocationCoordinate2DMake(northEast.latitude + 0.01f, northEast.longitude + 0.01f);
    
    [self zoomWithLatitudeLongitudeBoundsSouthWest: southWest northEast:northEast animated: NO];
    
    self.backgroundColor = [UIColor colorWithPatternImage:[RMMapView resourceImageNamed:@"LoadingTile.png"]];
    //self.backgroundColor = [UIColor colorWithPatternImage:[RMMapView resourceImageNamed:@"clear-bg.png"]];
    self.hideAttribution = YES;
    self.showsUserLocation = NO;
    self.userInteractionEnabled = NO;
    
    if (handler) {
        dispatch_async(tileSource.dataQueue, ^(void)
                       {
                           handler();
                       });
    }
}


@end
