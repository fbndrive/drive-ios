#import "DriveData.h"
#import <UIKit/UIKit.h>

@interface HistoryTableListCell : UITableViewCell

extern int const HISTORY_LIST_ROW_HEIGHT;

- (void) drawWithData:(DriveData *) data andRowIndex: (int) index;

@end
