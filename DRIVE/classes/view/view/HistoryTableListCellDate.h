#import "DriveData.h"
#import "RegularFontLabel.h"

@interface HistoryTableListCellDate : RegularFontLabel

-(void) fillWithData: (DriveData *) data;

@end
