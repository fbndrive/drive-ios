#import "CircleShapeNoStroke.h"

@implementation CircleShapeNoStroke

@synthesize fillColor = _fillColor;

- (id)initWithFrame:(CGRect)frame {
    _fillColor = [UIColor whiteColor];
    
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = NO;
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetStrokeColorWithColor(context, _fillColor.CGColor);
    CGContextSetFillColorWithColor(context, _fillColor.CGColor);
    
    CGContextAddEllipseInRect(context, rect);
    
    CGContextFillPath(context);
}

@end
