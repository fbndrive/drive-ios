#import <UIKit/UIKit.h>

@interface CircleShapeNoStroke : UIView

@property (nonatomic, strong) UIColor *fillColor;

@end
