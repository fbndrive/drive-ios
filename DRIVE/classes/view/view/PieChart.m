#import "AppDataManager.h"
#import "BoldFontLabel.h"
#import "Constants.h"
#import "LightFontLabel.h"
#import "PieChart.h"
#import "PieChartCircle.h"
#import "PieChartSlice.h"

@interface PieChart ()

@property (nonatomic) float animationDuration;

@property (nonatomic,strong) LightFontLabel *percentLabel;
@property (nonatomic,strong) LightFontLabel *nameLabel;
@property (nonatomic,strong) BoldFontLabel *selectedNameLabel;
@property (nonatomic,strong) PieChartSlice *pieSlice;
@property (nonatomic,strong) PieChartCircle *pieSliceBackground;

// animation helpers
@property (nonatomic, strong) NSDate *animationStartTime;
@property (nonatomic) float animStartValue;
@property (nonatomic) float animChangeValue;

@end

@implementation PieChart

@synthesize size = _size;
@synthesize value = _value;

- (id)initWithFrame:(CGRect)frame {
    _animationDuration = COMPLETE_ANIM_CYCLE_DURATION;
    _size = frame.size;
    _percent = 0.0f;
    _value = 0.0f;
    _name = @"test label";
    
    self = [super initWithFrame:frame];
    
    return self;
}

- (void) refresh {
    [_pieSliceBackground setNeedsDisplay];
    [_pieSlice setNeedsDisplay];
}

- (void) arrange {
    CGRect percentLabelFrame = _percentLabel.frame;
    CGRect nameLabelFrame = _nameLabel.frame;
    CGRect pieSliceFrame = _pieSliceBackground.frame;
    
    
    _percentLabel.frame = CGRectMake(_size.width/2 - percentLabelFrame.size.width/2, 0, pieSliceFrame.size.width, pieSliceFrame.size.height);
    _pieSliceBackground.frame = CGRectMake(0, pieSliceFrame.size.height, pieSliceFrame.size.width, pieSliceFrame.size.height);
    _pieSlice.frame = _pieSliceBackground.frame;
    _nameLabel.frame = CGRectMake(_size.width/2 - nameLabelFrame.size.width/2, _pieSliceBackground.frame.origin.y + _pieSliceBackground.frame.size.height, nameLabelFrame.size.width, nameLabelFrame.size.height);
}

- (void) animateToPercent: (float) percent {
    _animationStartTime = [NSDate date];
    _animStartValue = self.percent;
    _animChangeValue = percent - _animStartValue;
    self.percent = percent;
    
    _animationDuration = fabsf(COMPLETE_ANIM_CYCLE_DURATION * _animChangeValue);
    
    [self animateRangeTick:nil];
}

- (void) setSize:(CGSize)size {
    _size = size;
    
    [_pieSliceBackground setSize:_size];
    [_pieSlice setSize:_size];
}

- (void) setPercent:(float)percent {
    _percent = percent;
    
    [_percentLabel setTitle: [NSString stringWithFormat:@"%ld%%", lroundf(_percent*100)]];
    [_pieSlice setPercent:_percent];
}

- (void) setValue:(float)value {
    _value = value;
}

- (void) setName:(NSString *)name {
    _name = name;
    
    [_nameLabel setTitle: [NSString stringWithFormat:@"%@", _name]];
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    const CGFloat *components = CGColorGetComponents(adm.driveDescription.color.CGColor);
    
    UIColor *bgColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:.1];
    UIColor *sliceColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:.3];
    
    PieChartCircle *pieSliceBackground = [[PieChartCircle alloc] initWithFrame:CGRectMake(0, 0, _size.width, _size.height)];
    [pieSliceBackground setSize:_size andColor:bgColor];
    
    PieChartSlice *pieSlice = [[PieChartSlice alloc] initWithFrame:CGRectMake(0, 0, _size.width, _size.height)];
    [pieSlice setSize:_size andColor:sliceColor];
    pieSlice.percent = _percent;
    
    LightFontLabel *percentLabel = [[LightFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [percentLabel setTitle: [NSString stringWithFormat:@"%ld%%", lroundf(_percent*100)]];
    
    LightFontLabel *nameLabel = [[LightFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [nameLabel setTitle: [NSString stringWithFormat:@"%@", _name]];
    
    [self addSubview: pieSliceBackground];
    [self addSubview: pieSlice];
    [self addSubview: percentLabel];
    [self addSubview: nameLabel];
    
    _pieSliceBackground = pieSliceBackground;
    _pieSlice = pieSlice;
    _percentLabel = percentLabel;
    _nameLabel = nameLabel;
    
    [[self subviews] makeObjectsPerformSelector:@selector(setUserInteractionEnabled:)
     withObject:[NSNumber numberWithBool:FALSE]];
}

-(void) animateRangeTick: (NSTimer *) timer {
    
    float timeDifference = [[NSDate date] timeIntervalSinceDate: _animationStartTime];
    
    if(timeDifference >= _animationDuration) {
        [_pieSlice setPercent:_percent];
        [_pieSlice setNeedsDisplay];
        
        return;
    }
    
    float nv = [self getNextStepForCurrentTime:timeDifference fromStart:_animStartValue withChange:_animChangeValue andDuration:_animationDuration];
    
    [_pieSlice setPercent:nv];
    [_pieSlice setNeedsDisplay];
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(animateRangeTick:)
                                   userInfo:nil
                                    repeats:NO];
    
    //NSLog(@"nv: %f", nv);
}

-(float) getNextStepForCurrentTime: (float) time fromStart: (float) start withChange: (float) change andDuration: (float) duration  {
    time /= duration;
    return -change * time*(time-2) + start;
    
    //time /= duration;
	//return change*time*time*time*time*time + start;
}


@end
