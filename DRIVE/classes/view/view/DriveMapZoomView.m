#import "AppDataManager.h"
#import "DriveMapZoomView.h"
#import "RoutePoint.h"

@implementation DriveMapZoomView

@synthesize routePathData = _routePathData;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) drawRect:(CGRect)rect {
    if(_routePathData == nil) {
        return;
    }
    
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *color = adm.driveDescription.color;
    
    RoutePoint *seed = [_routePathData getLinkForAreaMin:_routePathData.topLeftCoordinate andMax:_routePathData.bottomRightCoordinate];
    RoutePoint *currentPt = seed;
    NSLog(@"seed: %@", seed);
    float factor = [_routePathData getDrawFactorForRectangle:rect];
    
    //CGFloat dotRadius = 2.0f;
    
    CGFloat lengths[2];
    lengths[0] = 0;
    lengths[1] = 4;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, 1);
    CGContextSetLineDash(context, 0.0f, lengths, 2);
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    
    CGContextMoveToPoint(context, currentPt.coordinate.longitude * factor, currentPt.coordinate.latitude * factor);
    
    while (currentPt.next) {
        CGContextAddLineToPoint(context, currentPt.next.coordinate.longitude * factor, currentPt.next.coordinate.latitude * factor);
        currentPt = currentPt.next;
    }
    
    while (currentPt.next) {
        CGContextAddLineToPoint(context, currentPt.next.coordinate.longitude * factor, currentPt.next.coordinate.latitude * factor);
        currentPt = currentPt.next;
    }
    
    currentPt = seed;
    
    CGContextMoveToPoint(context, currentPt.coordinate.longitude * factor, currentPt.coordinate.latitude * factor);
    
    while (currentPt.prev) {
        CGContextAddLineToPoint(context, currentPt.prev.coordinate.longitude * factor, currentPt.prev.coordinate.latitude * factor);
        currentPt = currentPt.prev;
    }
    
    CGContextStrokePath(context);
    
    /*CGContextStrokePath(context);
    
    CGRect rectangle = CGRectMake(linePoints[i].x-3,linePoints[i].y-3,80,80);
    CGRect rectangle = CGRectMake(linePoints[i-1].x - dotRadius, linePoints[i-1].y - dotRadius, dotRadius*2, dotRadius*2);
    
    CGContextSetLineWidth(context, 0);
    CGContextSetFillColorWithColor(context, _dotColor.CGColor);
    CGContextFillEllipseInRect(context, rectangle);*/
    
    
}


@end
