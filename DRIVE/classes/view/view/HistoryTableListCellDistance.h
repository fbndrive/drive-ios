#import "RegularFontLabel.h"

@interface HistoryTableListCellDistance : RegularFontLabel

-(void) fillWithData: (DriveData *) data;

@end
