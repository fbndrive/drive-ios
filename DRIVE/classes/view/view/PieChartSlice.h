#import <UIKit/UIKit.h>

@interface PieChartSlice : UIView

@property (nonatomic) float percent;
@property (nonatomic) CGSize size;
@property (nonatomic, strong) UIColor *color;

- (void) setSize:(CGSize)size andColor:(UIColor *) color;

@end
