#import "AppDataManager.h"
#import "BoldLabelBoldValueLabelValueView.h"
#import "Constants.h"

@implementation BoldLabelBoldValueLabelValueView

- (void) applyFormatLabel {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 14];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.labelLabel.font = fontFace;
    self.labelLabel.textColor = fontColor;
}

@end