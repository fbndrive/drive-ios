#import "AppDataManager.h"
#import "Constants.h"
#import "HistoryTableListCellDate.h"

@implementation HistoryTableListCellDate

-(void) applyFormatting {
    self.font = [UIFont fontWithName: Font_regularFace size: 18];
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void) fillWithData: (DriveData *) data {
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName: Database_dateTimezone];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:timeZone];
    [format setDateFormat:@"dd MMM"];
    
    [self setTitle: [format stringFromDate:data.startDate]];
}

@end