#import "BasicChartView.h"

@implementation BasicChartView

float const COMPLETE_ANIM_CYCLE_DURATION = 6.0f;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self drawUI];
    }
    return self;
}

- (void) drawUI {
    
}

@end
