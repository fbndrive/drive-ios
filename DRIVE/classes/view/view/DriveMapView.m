#import "AppDataManager.h"
#import <CoreLocation/CoreLocation.h>
#import "DriveMapView.h"
#import "DottedPath.h"

@interface DriveMapView ()

@property (nonatomic, strong) DottedPath *driveMapPath;

@end


@implementation DriveMapView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self drawUI];
        //[self setupEvents];
    }
    return self;
}

- (void) pushWithLocation: (CLLocation *) location  {
    [_driveMapPath pushWithCoordinates: location.coordinate];
    [_driveMapPath setNeedsDisplay];
}

- (void) drawUI {
    self.backgroundColor = [UIColor clearColor];
    
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIColor *dotColor = [adm driveDescription].color;
    
    _driveMapPath = [[DottedPath alloc] initWithFrame: CGRectMake(0,0,self.frame.size.width, self.frame.size.height)];
    _driveMapPath.backgroundColor = [UIColor clearColor];
    _driveMapPath.dotColor = dotColor;
    _driveMapPath.centerPoint = CGPointMake(75, 125);
    _driveMapPath.scale = 5;
    self.clipsToBounds = NO;
    
    [self addSubview:_driveMapPath];
    
    [_driveMapPath setNeedsDisplay];
}

- (void) setupEvents {
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:singleFingerTap];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    [_driveMapPath pushWithCoordinates:CLLocationCoordinate2DMake(location.x, location.y)];
    [_driveMapPath setNeedsDisplay];
}

@end
