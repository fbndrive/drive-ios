#import "AppDataManager.h"
#import "Constants.h"
#import "LightFontLabel.h"

@implementation LightFontLabel

-(void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.font = [UIFont fontWithName: Font_lightFace size: 16];
    self.textColor = colour;
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}


@end
