#import "AppDataManager.h"
#import "Constants.h"
#import "DriveFloatValueLabelValueView.h"

@implementation DriveFloatValueLabelValueView

- (void) setNumericValue:(float)numericValue {
    [super setNumericValue:floorf(numericValue * 10) / 10];
    
    NSString *sv = [self floatStringOfValue];
    [self setStringValue: sv];
}

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *lightFace = [UIFont fontWithName: Font_lightFace size: 25];
    UIFont *boldFace = [UIFont fontWithName: Font_boldFace size: 25];
    UIColor *fontColor = [adm driveDescription].color;
    UIColor *clearColor = [UIColor clearColor];
    
    [self.mutableAttributedStringValue addAttribute:NSFontAttributeName value:boldFace range:NSMakeRange(0, self.stringValueLength)];
    [self.mutableAttributedStringValue addAttribute:NSFontAttributeName value:lightFace range:NSMakeRange(self.stringValueLength-2, 2)];
    [self.mutableAttributedStringValue addAttribute:NSForegroundColorAttributeName value:fontColor range:NSMakeRange(0,self.stringValueLength)];
    [self.mutableAttributedStringValue addAttribute:NSBackgroundColorAttributeName value:clearColor range:NSMakeRange(0, self.stringValueLength)];
    
    self.valueLabel.attributedText = self.mutableAttributedStringValue;
}

- (void) sizeMe {
    [self.valueLabel sizeToFit];
    [self.labelLabel sizeToFit];
    
    
    self.valueLabel.frame = CGRectMake(self.labelLabel.frame.size.width - 2, 0, self.valueLabel.frame.size.width + 5, self.valueLabel.frame.size.height);
    self.labelLabel.frame = CGRectMake(0, self.valueLabel.frame.size.height - 12, self.labelLabel.frame.size.width, self.labelLabel.frame.size.height);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.valueLabel.frame.size.width + self.labelLabel.frame.size.width, self.valueLabel.frame.size.height + self.labelLabel.frame.size.height);
    
    //NSLog(@"frame width: %f", self.frame.size.width);
}

@end
