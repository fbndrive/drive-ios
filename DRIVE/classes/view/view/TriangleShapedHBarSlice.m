#import "TriangleShapedHBarSlice.h"

@implementation TriangleShapedHBarSlice

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) setSize:(CGSize)size andColor:(UIColor *) color {
    _color = color;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void) setDelta:(float)delta {
    _delta = delta;
}

/*- (void) setFrame:(CGSize)size {
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}*/

- (void)drawRect:(CGRect)rect {
    CGSize size = self.frame.size;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    //CGContextFillEllipseInRect(context, rectangle);
    CGContextMoveToPoint(context, 0, size.height);
    CGContextAddLineToPoint(context, size.width * _delta, size.height - size.height * _delta);
    CGContextAddLineToPoint(context, size.width * _delta, size.height);
    CGContextAddLineToPoint(context, 0, size.height);
    CGContextFillPath(context);
}

@end
