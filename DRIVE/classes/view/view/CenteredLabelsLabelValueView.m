#import "AppDataManager.h"
#import "CenteredLabelsLabelValueView.h"
#import "Constants.h"

@implementation CenteredLabelsLabelValueView

@synthesize labelWidth = _labelWidth;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _labelWidth = 200;
    }
    
    return self;
}

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 40];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
    self.valueLabel.textAlignment = NSTextAlignmentCenter;
}

- (void) applyFormatLabel {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 28];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.labelLabel.font = fontFace;
    self.labelLabel.textColor = fontColor;
    self.labelLabel.textAlignment = NSTextAlignmentCenter;
}

- (void) sizeMe {
    [self.valueLabel sizeToFit];
    [self.labelLabel sizeToFit];
    
    self.labelLabel.frame = CGRectMake(0, 0, _labelWidth, self.labelLabel.frame.size.height);
    self.valueLabel.frame = CGRectMake(0, self.labelLabel.frame.size.height - 8, _labelWidth, self.valueLabel.frame.size.height);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _labelWidth, self.valueLabel.frame.size.height + self.labelLabel.frame.size.height);
    
    //NSLog(@"frame width: %f", self.frame.size.width);
}

@end
