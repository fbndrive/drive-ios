#import "AppDataManager.h"
#import "Constants.h"
#import "CompassView.h"
#import "CompassBackgroundView.h"
#import "CompassHandView.h"
#import "NumberTools.h"

@interface CompassView()

@property (nonatomic) float animationDuration;
@property (nonatomic,strong) CompassHandView *hand;
@property (nonatomic,strong) CompassBackgroundView *background;

// animation helpers
@property (nonatomic, strong) NSDate *animationStartTime;
@property (nonatomic) float animStartValue;
@property (nonatomic) float animChangeValue;

@end

@implementation CompassView

float const FULL_360_DURATION = 3.0f;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _animationDuration = FULL_360_DURATION;
        
        _size = frame.size;
        _directionValue = 0.0f;
        
        [self drawUI];
    }
    return self;
}

- (void) refresh {
    [_background setNeedsDisplay];
}

- (void) animateToDirectionValue: (float) directionValue {
    float directionInRadians = [NumberTools degreesToRadians: directionValue];
    
    _animationStartTime = [NSDate date];
    _animStartValue = _directionValue;
    _animChangeValue = directionInRadians - _animStartValue;
    
    NSLog(@"prior: %f %f %f", _animStartValue, directionInRadians, _animChangeValue);
    
    /*if(fabsf(_animChangeValue) > M_PI) {
        _animStartValue = M_2_PI - directionValue;
        _animChangeValue = _animStartValue - (M_PI - _animChangeValue);
    }
    
    
    [self setDirectionValue:_animStartValue];
    return;*/
    
    _directionValue = directionInRadians;
    
    NSLog(@"after: %f %f %f", _animStartValue, directionInRadians, _animChangeValue);
    
    
    //_animationDuration = fabsf(COMPLETE_ANIM_CYCLE_DURATION * _animChangeValue);
    
    [self animateRangeTick:nil];
}

- (void) setSize:(CGSize)size {
    _size = size;
    
    [_background setSize:_size];
}

- (void) setDirectionValue:(float)directionValue {
    float directionInRadians = [NumberTools degreesToRadians: directionValue];
    
    _directionValue = directionInRadians;
    
    _hand.transform = CGAffineTransformMakeRotation(_directionValue);
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    const CGFloat *components = CGColorGetComponents(adm.driveDescription.color.CGColor);
    
    UIColor *bgColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:.1];
    //UIColor *handColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:.3];
    
    CompassBackgroundView *background = [[CompassBackgroundView alloc] initWithFrame:CGRectMake(0, 0, _size.width, _size.height)];
    [background setSize:_size andColor:bgColor];
    
    CompassHandView *hand = [[CompassHandView alloc] initWithFrame:CGRectMake(_size.height/2, 0, 10, _size.height)];
    [hand setSize:_size andColor:adm.driveDescription.color];
    hand.alpha = .3;
    
    CGPoint oldOrigin = hand.frame.origin;
    hand.layer.anchorPoint = CGPointMake(0, .5);
    CGPoint newOrigin = hand.frame.origin;
    
    CGPoint transition;
    transition.x = newOrigin.x - oldOrigin.x;
    transition.y = newOrigin.y - oldOrigin.y;
    
    hand.center = CGPointMake (hand.center.x - transition.x, hand.center.y - transition.y);
    
    //hand.transform = CGAffineTransformMakeRotation(M_PI_4);
    
    
    [self addSubview: background];
    [self addSubview:hand];
    
    _background = background;
    _hand = hand;
}

-(void) animateRangeTick: (NSTimer *) timer {
    
    float timeDifference = [[NSDate date] timeIntervalSinceDate: _animationStartTime];
    
    if(timeDifference >= _animationDuration) {
        _hand.transform = CGAffineTransformMakeRotation(_directionValue);
        
        
        [_hand setNeedsDisplay];
        return;
    }

    //float nv = [self getNextStepForCurrentTime:timeDifference fromStart:_animStartValue withChange:_animChangeValue andDuration:_animationDuration];
    
    float nv = [self getNextStepForParam:timeDifference/_animationDuration];
    
    _hand.transform = CGAffineTransformMakeRotation(_directionValue - (1-nv)*_animChangeValue);
    [_hand setNeedsDisplay];
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(animateRangeTick:)
                                   userInfo:nil
                                    repeats:NO];
    //NSLog(@"nv: %f %f", _directionValue, _directionValue - (1-nv)*_animChangeValue);
}

- (float) getNextStepForParam: (float) param {
    float p = 0.3f;
    return pow(2,-10*param) * sin((param-p/4)*(2*M_PI)/p) + 1;
}

-(float) getNextStepForCurrentTime: (float) time fromStart: (float) start withChange: (float) change //
                       andDuration: (float) duration  {
    
    time /= duration;
    return -change * time*(time-2) + start;
    
    //time /= duration;
	//return change*time*time*time*time*time + start;
}





@end
