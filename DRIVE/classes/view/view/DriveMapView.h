#import <UIKit/UIKit.h>

@interface DriveMapView : UIView

- (void) pushWithLocation: (CLLocation *) location;

@end
