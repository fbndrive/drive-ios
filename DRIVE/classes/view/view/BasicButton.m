#import "BasicButton.h"
#import "AppDataManager.h"
#import "Constants.h"
#import <Quartzcore/Quartzcore.h>

@implementation BasicButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self applyFormatting];
    }
    
    return self;
}

-(void) animateIn {
    self.enabled = NO;
    self.hidden = NO;
    self.alpha = 0;
    
    [UIView animateWithDuration:.4
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         self.enabled = YES;
                     }];

}

-(void) animateOut {
    self.enabled = NO;
    self.alpha = 1;
    
    [UIView animateWithDuration:.4
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         self.hidden = YES;
                         self.enabled = YES;
                     }];
}

-(void) setTitle:(NSString *)title {
    [self setTitle:[title uppercaseString] forState:UIControlStateNormal];
    [self sizeToFit];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName: Font_boldFace size: 16]};
    CGSize stringsize = [[title uppercaseString] sizeWithAttributes: attributes];
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, stringsize.width + 10, stringsize.height + 10);
}

-(void) refresh {
    [self applyFormatting];
}

-(void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.titleLabel.font = [UIFont fontWithName: Font_boldFace size: 16];
    [self setTitleColor:colour forState:UIControlStateNormal];
    self.backgroundColor = [UIColor clearColor];
    
    self.titleLabel.layer.shadowColor = [colour CGColor];
    self.titleLabel.layer.shadowRadius = 18.0f;
    self.titleLabel.layer.shadowOpacity = .9f;
    self.titleLabel.layer.shadowOffset = CGSizeZero;
    self.titleLabel.layer.masksToBounds = NO;
    
    //self.titleLabel.clipsToBounds = YES;
}

/*
 -(void) animateIn {
 self.clipsToBounds = YES;
 
 //backup of some properties
 CGRect labelFrame = self.titleLabel.frame;
 
 //self.frame.origin.y = buttonFrame.origin.y + buttonFrame.size.height;
 //self.frame.size.height = 1.0f;
 
 self.titleLabel.frame = CGRectMake(labelFrame.origin.x, labelFrame.origin.y + labelFrame.size.height, labelFrame.size.width, labelFrame.size.height);
 self.titleLabel.alpha = .1;
 self.titleLabel.layer.shadowOpacity = 0.0f;
 
 [UIView animateWithDuration:.4
 delay:0
 options: UIViewAnimationCurveEaseOut
 animations:^{
 self.titleLabel.frame = labelFrame;
 self.titleLabel.alpha = 1;
 }
 completion:^(BOOL finished){
 self.clipsToBounds = NO;
 self.titleLabel.layer.shadowOpacity = .9f;
 }];
 
 }
*/

@end
