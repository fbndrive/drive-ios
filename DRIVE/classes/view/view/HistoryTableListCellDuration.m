#import "AppDataManager.h"
#import "Constants.h"
#import "HistoryTableListCellDuration.h"

@implementation HistoryTableListCellDuration

NSString * const HOUR_LABEL = @"''";
NSString * const MINUTE_LABEL = @"'";
NSString * const SECOND_LABEL = @"";

-(void) applyFormatting {
    self.font = [UIFont fontWithName: Font_regularFace size: 18];
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}

- (void) fillWithData: (DriveData *) data {
    int duration = [data.finishDate timeIntervalSinceDate: data.startDate];
    NSString *durationString = [self timeStringOfValue: duration];
    
    [self setTitle: durationString];
}

/*-(void) fillWithData: (DriveData *) dd {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    //NSString *distance = [numberFormatter stringFromNumber:@([adm convertDistanceToUserDistanceUnit:data.distance])];
    NSString *distance = @"test distance";
    
    //NSString *durationString = [self timeStringOfValue:data.duration];
    NSString *durationString = @"test duration";
    
    NSString *distanceUnit = [[adm getUserDistanceUnitLabel] uppercaseString];
    
    NSString *stringValue = [NSString stringWithFormat:@"%@ %@ | %@", distance, distanceUnit, durationString];
    
    NSMutableAttributedString *mutableAttributedStringValue = [[NSMutableAttributedString alloc] initWithString:stringValue];
    NSInteger stringValueLength = [stringValue length];
    
    UIFont *bigfont = [UIFont fontWithName: Font_regularFace size: 24];
    UIFont *smallfont = [UIFont fontWithName: Font_regularFace size: 14];
    
    UIColor *color= adm.driveDescription.color;
    
    NSRange position = NSMakeRange(0, stringValueLength);
    [mutableAttributedStringValue addAttribute:NSFontAttributeName value:bigfont range:position];
    
    position = [stringValue rangeOfString:[[adm getUserDistanceUnitLabel] uppercaseString]];
    [mutableAttributedStringValue addAttribute:NSFontAttributeName value:smallfont range:position];
    
    position = [stringValue rangeOfString:HOUR_LABEL];
    [mutableAttributedStringValue addAttribute:NSFontAttributeName value:smallfont range:position];
    
    position = [stringValue rangeOfString:MINUTE_LABEL];
    [mutableAttributedStringValue addAttribute:NSFontAttributeName value:smallfont range:position];
    
    position = [stringValue rangeOfString:SECOND_LABEL];
    [mutableAttributedStringValue addAttribute:NSFontAttributeName value:smallfont range:position];
    
    [mutableAttributedStringValue addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, stringValueLength)];
    
    self.attributedText = mutableAttributedStringValue;
}

-(void) applyFormatting {
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}*/

- (NSString *) timeStringOfValue: (int) value {
    NSString *timeString;
    NSInteger ti = (NSInteger) value;
    
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    if(hours > 0) {
        timeString = [NSString stringWithFormat:@"%02ld%@%02ld%@%02ld%@", (long)hours, HOUR_LABEL, (long)minutes, MINUTE_LABEL, (long)seconds, SECOND_LABEL];
    } else {
        timeString = [NSString stringWithFormat:@"%02ld%@%02ld%@", (long)minutes, MINUTE_LABEL, (long)seconds, SECOND_LABEL];
    }
    
    return timeString;
}


@end
