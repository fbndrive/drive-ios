#import "AppDataManager.h"
#import "RegularFontLabel.h"
#import "Constants.h"

@implementation RegularFontLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self applyFormatting];
    }
    return self;
}

- (void) refresh {
    [self applyFormatting];
}

- (void)setTitle:(NSString *)text {
    [super setText:[text uppercaseString]];
    
    [self sizeToFit];
    //CGRect labelFrame = self.titleLabel.frame;
    //self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, labelFrame.size.width + 10, labelFrame.size.height + 10);
}

-(void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.font = [UIFont fontWithName: Font_regularFace size: 16];
    self.textColor = colour;
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}

@end
