#import <UIKit/UIKit.h>

@interface ApplicationBackgroundView : UIView

@property (strong, nonatomic) UIImageView *backgroundView;

- (void) refresh;

@end
