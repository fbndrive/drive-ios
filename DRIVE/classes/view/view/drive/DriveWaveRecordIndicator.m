#import "Constants.h"
#import "DriveWaveRecordIndicator.h"
#import "DriveWaveRecordIndicatorDial.h"
#import "DriveWaveRecordIndicatorPie.h"

@interface DriveWaveRecordIndicator ()

@property (nonatomic,strong) DriveWaveRecordIndicatorPie *pie;
@property (nonatomic,strong) DriveWaveRecordIndicatorDial *dial;
@property (nonatomic,strong) UIImageView *icon;

@end


@implementation DriveWaveRecordIndicator

@synthesize progress = _progress;
@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.progress = 1.0f;
        
        _dial = [[DriveWaveRecordIndicatorDial alloc] initWithFrame: CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _dial.alpha = 0.2f;
        [self addSubview: _dial];
        
        _pie = [[DriveWaveRecordIndicatorPie alloc] initWithFrame: CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _pie.alpha = 0.5f;
        [_pie setProgress: 0.0f];
        [self addSubview: _pie];
        
        _icon = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"rec_mic"]];
        _icon.frame = CGRectMake(frame.size.width/2 - _icon.frame.size.width/2, frame.size.height/2 - _icon.frame.size.height/2, _icon.frame.size.width, _icon.frame.size.height);
        [self addSubview: _icon];
    }
    
    return self;
}

- (void) setProgress: (float) progress {
    _progress = progress;
    
    _pie.progress = _progress;
    [_pie setNeedsDisplay];
}

- (void) setColor:(UIColor *)color {
    _color = color;
    
    _dial.color = _color;
    _pie.color = _color;
}

- (void)drawRect:(CGRect)rect {
    return;
    CGContextRef context = UIGraphicsGetCurrentContext();
    /*CGContextSetLineWidth(context, 1.0f);
    CGContextSetStrokeColorWithColor(context,
                                     _color.CGColor);*/
    CGContextSetFillColorWithColor(context,
                                     _color.CGColor);
    
    float radius = rect.size.width / 2.0f;
    
    CGPoint arcCenter = CGPointMake(rect.origin.x + rect.size.width/2, rect.origin.y + rect.size.height/2);
    
    CGContextMoveToPoint(context, arcCenter.x + radius, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius, 0, My_PI * 2.0f, YES);
    
    /*CGContextMoveToPoint(context, arcCenter.x + radius - radiusDescent * 2.0f, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius - radiusDescent * 2.0f, 0, My_PI * 2, YES);
    
    CGContextMoveToPoint(context, arcCenter.x + radius - radiusDescent, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius - radiusDescent, 0.0f, My_PI * 2, YES);
    
    CGContextMoveToPoint(context, arcCenter.x + radius, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius, 0.0f, My_PI * 2, YES);
    
    CGContextMoveToPoint(context, rect.size.width/2.0f, 0);
    CGContextAddLineToPoint(context, rect.size.width/2.0f, rect.size.height);
    CGContextMoveToPoint(context, 0,rect.size.height/2.0f);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height/2.0f);*/
    
    CGContextFillPath(context);
    //CGContextStrokePath(context);
}


@end
