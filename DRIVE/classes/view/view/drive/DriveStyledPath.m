#import "AppDataManager.h"
#import <CoreLocation/CoreLocation.h>
#import "DriveTrackpointData.h"
#import "DriveStyledPath.h"

@interface DriveStyledPath ()

@property (nonatomic, retain) DriveTrackpointCollection *collection;

@end

@implementation DriveStyledPath

@synthesize style = _style;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.style = DrivePathStyleDefault;
    }
    
    return self;
}

- (void) setStyle:(DrivePathStyle) style {
    _style = style;
}

- (void) redrawWithCollection:(DriveTrackpointCollection *) collection {
    _collection = collection;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    switch (_style) {
        case DrivePathStyleDefault:
        default:
            [self drawDefaultLineStyle: rect];
            break;
    }
}

- (void) drawDefaultLineStyle:(CGRect) rect {
    int i;
    int count = (int)[_collection.trackpoints count];
    
    DriveTrackpointData *tp;
    
    if(count < 1) {
        return;
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    //CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineWidth(context, 2);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    
    tp = [_collection.trackpoints objectAtIndex: 0];
    CGContextMoveToPoint (context, tp.mapPosition.x, tp.mapPosition.y);
    
    for (i = 1; i < count; i++) {
        tp = [_collection.trackpoints objectAtIndex: i];
        CGContextAddLineToPoint (context, tp.mapPosition.x, tp.mapPosition.y);
    }
    
    CGContextStrokePath(context);
    
    _collection = nil;
}

- (void) drawDottedLineStyle:(CGRect) rect {
}

- (void) drawArrowedLineStyle:(CGRect) rect {
    
}

/*
 AppDataManager *adm = (AppDataManager *)[AppDataManager appDataManager];
 
 int i;
 int count = (int)[_points count];
 CGPoint pt;
 
 if(count < 1) {
 return;
 }
 
 CGRect dotRect;
 CGFloat dotRadius = 2;
 
 float radius = 4;
 
 CGFloat lengths[2];
 lengths[0] = 0;
 lengths[1] = 4;
 
 CGContextRef context = UIGraphicsGetCurrentContext();
 CGContextSetAllowsAntialiasing(context, YES);
 CGContextSetLineCap(context, kCGLineCapRound);
 CGContextSetLineJoin(context, kCGLineJoinRound);
 CGContextSetLineWidth(context, 2);
 //CGContextSetLineDash(context, 0.0f, lengths, 2);
 //CGContextSetStrokeColorWithColor(context, adm.driveDescription.color.CGColor);
 CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
 CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
 
 pt = [[_points objectAtIndex: 0] CGPointValue];
 
 CGContextMoveToPoint (context, pt.x, pt.y);
 for (i = 0; i < count; i++) {
 pt = [[_points objectAtIndex: i] CGPointValue];
 CGContextAddLineToPoint (context, pt.x, pt.y);
 
 dotRect = CGRectMake(pt.x - radius/2, pt.y - radius/2, radius, radius);
 
 CGContextAddEllipseInRect(context, dotRect);
 CGContextMoveToPoint (context, pt.x, pt.y);
 }
 
 CGContextStrokePath(context);
 CGContextFillPath(context);
 
 
 //CGRect rectangle = CGRectMake(linePoints[i].x-3,linePoints[i].y-3,80,80);
 //CGRect rectangle = CGRectMake(pt.x - dotRadius, pt.y - dotRadius, dotRadius*2, dotRadius*2);
 
 //CGContextSetLineWidth(context, 0);
 //CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
 //CGContextFillEllipseInRect(context, rectangle);
 
 //CGContextStrokePath(context);
 
 _points = nil;
 */

@end
