#import <UIKit/UIKit.h>

@interface DriveWaveRecordIndicatorDial : UIView

@property (nonatomic, strong) UIColor *color;

@end
