#import "Constants.h"
#import "DriveWaveRecordIndicatorDial.h"

@implementation DriveWaveRecordIndicatorDial

@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    
    float radius = rect.size.width / 2.0f;
    
    CGPoint arcCenter = CGPointMake(rect.size.width/2, rect.size.height/2);
    
    CGContextMoveToPoint(context, arcCenter.x + radius, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius, 0, My_PI * 2.0f, YES);
    
    CGContextFillPath(context);
}


@end
