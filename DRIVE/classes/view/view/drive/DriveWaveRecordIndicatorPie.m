#import "Constants.h"
#import "DriveWaveRecordIndicatorPie.h"

@implementation DriveWaveRecordIndicatorPie

@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void) setProgress:(float)progress {
    _progress = progress;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    float progress = fminf(1, fmaxf(0, _progress));
    float startAngle = 2.0f * My_PI - My_PI / 2;
    
    if(progress <= 0.0f) {
        //return;
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    
    float radius = rect.size.width / 2.0f;
    
    CGPoint arcCenter = CGPointMake(rect.size.width/2, rect.size.height/2);
    
    CGContextMoveToPoint(context, arcCenter.x, arcCenter.y);
    CGContextAddLineToPoint(context, arcCenter.x, 0);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius, startAngle, startAngle + progress * 2.0f * My_PI, NO);
    CGContextAddLineToPoint(context, arcCenter.x, arcCenter.y);
    
    CGContextFillPath(context);
    
}


@end
