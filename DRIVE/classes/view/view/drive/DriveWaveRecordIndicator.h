#import <UIKit/UIKit.h>

@interface DriveWaveRecordIndicator : UIView

@property (nonatomic) float progress;
@property (nonatomic, strong) UIColor *color;

@end
