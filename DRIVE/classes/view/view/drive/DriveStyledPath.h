#import "DriveTrackpointCollection.h"
#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    DrivePathStyleDefault = 1,
    DrivePathStyleDotted = 2,
    DrivePathStyleArrowed = 3,
} DrivePathStyle;


@interface DriveStyledPath : UIView

@property (nonatomic) DrivePathStyle style;

- (void) redrawWithCollection:(DriveTrackpointCollection *) collection;

@end
