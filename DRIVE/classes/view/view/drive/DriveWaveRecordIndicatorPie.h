#import <UIKit/UIKit.h>

@interface DriveWaveRecordIndicatorPie : UIView

@property (nonatomic) float progress;
@property (nonatomic, strong) UIColor *color;

@end
