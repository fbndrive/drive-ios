#import "TopEdgeSliderView.h"

@interface TopEdgeSliderView ()

@property (nonatomic) BOOL animating;

@end

@implementation TopEdgeSliderView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _color = [UIColor whiteColor];
    }
    return self;
}


- (void) setPosition:(float)position {
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.frame = CGRectMake((screenFrame.size.width - self.frame.size.width) * position, 0, self.frame.size.width, self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

- (void)drawRect:(CGRect)rect {
    CGSize size = self.frame.size;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    CGContextMoveToPoint(context, 0, 0);
    CGContextFillRect(context, CGRectMake(0, 0, size.width, size.height));
}

@end
