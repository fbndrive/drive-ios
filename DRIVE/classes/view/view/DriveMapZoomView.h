#import "RoutePathData.h"
#import <UIKit/UIKit.h>

@interface DriveMapZoomView : UIView

@property (nonatomic, retain) RoutePathData *routePathData;

@end
