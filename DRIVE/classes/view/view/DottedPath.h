#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface DottedPath : UIView

@property (nonatomic, retain) UIColor *dotColor;

@property (nonatomic) float scale;
@property (nonatomic) CGPoint centerPoint;
// this require rethinking
@property (nonatomic) UIEdgeInsets dotEdgeRect;

- (void) reset;
- (void) pushWithCoordinates: (CLLocationCoordinate2D) coordinate;

@end
