#import "AppDataManager.h"
#import "Constants.h"
#import "DriveData.h"
#import "FileSystemManager.h"
#import "HistoryTableListCell.h"
#import "HistoryTableListCellDate.h"
#import "HistoryTableListCellDuration.h"
#import "HistoryTableListCellDistance.h"
#import "HistoryTableListCellMapBackground.h"

@interface HistoryTableListCell ()

@property (nonatomic) BOOL drew;
@property (strong, nonatomic) HistoryTableListCellDate *dateLabel;
@property (strong, nonatomic) HistoryTableListCellDuration *durationLabel;
@property (strong, nonatomic) HistoryTableListCellDistance *distanceLabel;
@property (strong, nonatomic) HistoryTableListCellMapBackground *mapBackground;

@end

@implementation HistoryTableListCell

int const HISTORY_LIST_ROW_HEIGHT = 60;

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
        _drew = NO;
    }
    
    return self;
}

- (void) drawWithData:(DriveData *) data andRowIndex: (int) index {
    if (_drew == NO) {
        [self drawUI];
    }
    
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIColor *color = adm.driveDescription.color;
    
    _dateLabel.textColor = color;
    _durationLabel.textColor = color;
    _distanceLabel.textColor = color;
    
    [_dateLabel fillWithData:data];
    [_durationLabel fillWithData:data];
    [_distanceLabel fillWithData: data];
    
    UIImage *mapImage = [FileSystemManager getHistoryListMapImageForDriveData: data];
    
    if(_mapBackground) {
        [_mapBackground removeFromSuperview];
        _mapBackground = nil;
    }
    
    if(mapImage != nil) {
        _mapBackground = [[HistoryTableListCellMapBackground alloc] initWithImage: mapImage];
        
        [self.contentView insertSubview:_mapBackground atIndex:0];
    }
    
    if(index %2 == 0) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha: .2];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
    
    [self sizeMe];
}

- (void)sizeMe {
    CGSize size = self.contentView.frame.size;
    
    float y = (size.height - _durationLabel.frame.size.height)/2;
    
    _dateLabel.frame = CGRectMake(Screen_globalMargin, y, _dateLabel.frame.size.width, _dateLabel.frame.size.height);
    _durationLabel.frame = CGRectMake((size.width - _durationLabel.frame.size.width)/2, y, _durationLabel.frame.size.width, _durationLabel.frame.size.height);
    _distanceLabel.frame = CGRectMake(size.width - _distanceLabel.frame.size.width - Screen_globalMargin, y, _distanceLabel.frame.size.width, _distanceLabel.frame.size.height);
}

- (void)drawUI {
    CGSize size = self.contentView.frame.size;
    
    float leftColWidth = size.width * .8;
    
    _dateLabel = [[HistoryTableListCellDate alloc] initWithFrame:CGRectMake(Screen_globalMargin, 0, leftColWidth, 30)];
    
    _durationLabel = [[HistoryTableListCellDuration alloc] initWithFrame:CGRectMake(Screen_globalMargin, 32, leftColWidth, 30)];
    
    _distanceLabel = [[HistoryTableListCellDistance alloc] initWithFrame:CGRectMake(Screen_globalMargin, -4, leftColWidth, 30)];
    
    [self.contentView addSubview: _dateLabel];
    [self.contentView addSubview: _durationLabel];
    [self.contentView addSubview: _distanceLabel];
    
    //self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _drew = YES;    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
