#import "AppDataManager.h"
#import "Constants.h"
#import "MainMenuOpenCloseButton.h"

@implementation MainMenuOpenCloseButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self drawUI];
    }
    return self;
}

- (void) refresh {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIImage *img;
    
    if(adm.driveCharacteristic == DriveCharacteristic_ECO) {
        img = [UIImage imageNamed:@"main-menu-glow-green"];
    } else if(adm.driveCharacteristic == DriveCharacteristic_NORMAL) {
        img = [UIImage imageNamed:@"main-menu-glow-white"];
    } else {
        img = [UIImage imageNamed:@"main-menu-glow-red"];
    }
    
    [self setImage:img forState:UIControlStateNormal];
    self.frame = CGRectMake(0, 0, img.size.width, img.size.height);
}

- (void) drawUI {
    [self refresh];
    
    //self.backgroundColor = [UIColor redColor];
}

@end
