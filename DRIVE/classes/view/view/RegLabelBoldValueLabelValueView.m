#import "AppDataManager.h"
#import "Constants.h"
#import "RegLabelBoldValueLabelValueView.h"

@interface RegLabelBoldValueLabelValueView ()

@property (nonatomic) NSTimer *blinkTimer;
@property (nonatomic) NSInteger blinkCounter;

@end

@implementation RegLabelBoldValueLabelValueView

#pragma mark - User Interface

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 20];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
}

- (void) applyFormatLabel {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_regularFace size: 14];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.labelLabel.font = fontFace;
    self.labelLabel.textColor = fontColor;
}

- (void) applyFormatting {
    [self applyFormatLabel];
    [self applyFormatValue];
}

- (void) sizeMe {
    [self.valueLabel sizeToFit];
    [self.labelLabel sizeToFit];
    
    
    self.valueLabel.frame = CGRectMake(self.labelLabel.frame.size.width - 2, 0, self.valueLabel.frame.size.width + 5, self.valueLabel.frame.size.height);
    self.labelLabel.frame = CGRectMake(0, self.valueLabel.frame.size.height - 10, self.labelLabel.frame.size.width, self.labelLabel.frame.size.height);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.valueLabel.frame.size.width + self.labelLabel.frame.size.width, self.valueLabel.frame.size.height + self.labelLabel.frame.size.height);
    
    //NSLog(@"frame: %@", NSStringFromCGRect(self.labelLabel.frame));
}

- (void) killMe {
    if(_blinkTimer) {
        [_blinkTimer invalidate];
        _blinkTimer = nil;
    }
}

//blink animation methods

-(void) randomBlink: (NSTimer *) timer {
    // end of blink
    if(_blinkCounter >= 8) {
        _blinkCounter = 0;
        [self applyFormatting];
        return;
    }
    
    _blinkCounter++;
    
    if(_blinkCounter % 2 == 0) {
        [self applyFormatting];
    } else {
        int blinkIdx = rand() % self.stringValueLength;
        [self blinkAtIndex:blinkIdx];
    }
    
    if(_blinkTimer) [_blinkTimer invalidate];
    _blinkTimer = [NSTimer scheduledTimerWithTimeInterval:.02
                                     target:self
                                   selector:@selector(randomBlink:)
                                   userInfo:nil
                                    repeats:NO];

}

- (void) blinkAtIndex:(NSInteger) index {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *font = [UIFont fontWithName: Font_boldFace size: 20];
    UIColor *fontColor = [adm driveDescription].color;
    UIColor *clearColor = [UIColor clearColor];
    
    [self.mutableAttributedStringValue addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, self.stringValueLength)];
    [self.mutableAttributedStringValue addAttribute:NSForegroundColorAttributeName value:fontColor range:NSMakeRange(0, self.stringValueLength)];
    [self.mutableAttributedStringValue addAttribute:NSForegroundColorAttributeName value:clearColor range:NSMakeRange(index, 1)];
    
    self.valueLabel.attributedText = self.mutableAttributedStringValue;
}

@end
