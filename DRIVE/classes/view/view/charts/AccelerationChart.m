#import "AccelerationChart.h"
#import "AppDataManager.h"
#import "UIImage+Tint.h"

@interface AccelerationChart ()

@property (nonatomic, retain) RegularFontLabel *accelerationRangeLabel;
@property (nonatomic, retain) BoldFontLabel *accelerationTimeLabel;

@property (nonatomic, retain) UIImageView *dial1View;
@property (nonatomic, retain) UIImageView *dial2View;
@property (nonatomic, retain) UIImageView *dial3View;
@property (nonatomic, retain) UIImageView *dial4View;

@end


@implementation AccelerationChart

float const FASTEST_0_100_ACC = 3.0f;
float const SLOWEST_0_100_ACC = 20.0f;

@synthesize value = _value;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    return self;
}

- (void) setValue:(float)value {
    _value = value;
}

- (void) setRangeLabelText:(NSString *) text {
    [_accelerationRangeLabel setTitle: text];
    
    _accelerationRangeLabel.frame = CGRectMake((_dial4View.frame.size.width - _accelerationRangeLabel.frame.size.width)/2, _dial4View.frame.origin.y + _dial4View.frame.size.height + 10, _accelerationRangeLabel.frame.size.width, _accelerationRangeLabel.frame.size.height);
}

- (void) setTimeLabelFromValue:(float) value {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setGroupingSeparator:@""];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *commaString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat: value]];
    
    [_accelerationTimeLabel setTitle: commaString];
    
    _accelerationTimeLabel.frame = CGRectMake((_dial4View.frame.size.width - _accelerationTimeLabel.frame.size.width)/2, 0, _accelerationTimeLabel.frame.size.width, _accelerationTimeLabel.frame.size.height);
    
}

- (void) setForAnimarionValue:(float) value {
    [self setTimeLabelFromValue:value];
    
    value = fmaxf(FASTEST_0_100_ACC, fminf(SLOWEST_0_100_ACC, value));
    float alphaValue = value/(SLOWEST_0_100_ACC - FASTEST_0_100_ACC);
    
    _dial1View.alpha = fminf(alphaValue, .25f) / .25f;
    _dial2View.alpha = fminf(alphaValue, .5f) / .5f;
    _dial3View.alpha = fminf(alphaValue, .75f) / .75f;
    _dial4View.alpha = fminf(alphaValue, 1.0f);
    
    //NSLog(@"acceleration chart animation step / value: %0.02f", value);
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *color = [adm driveDescription].color;
    
    RegularFontLabel *accelerationRangeLabel = [[RegularFontLabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    accelerationRangeLabel.alpha = .5f;
    [accelerationRangeLabel setTitle: @"0-0"];
    
    BoldFontLabel *accelerationTimeLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [accelerationTimeLabel setTitle: @"0.00"];
    
    UIImage *dial1 = [[UIImage imageNamed:@"acceleration-dial-1"] tintedImageWithColor:color];
    UIImage *dial2 = [[UIImage imageNamed:@"acceleration-dial-2"] tintedImageWithColor:color];
    UIImage *dial3 = [[UIImage imageNamed:@"acceleration-dial-3"] tintedImageWithColor:color];
    UIImage *dial4 = [[UIImage imageNamed:@"acceleration-dial-4"] tintedImageWithColor:color];
    
    UIImageView *dial1View = [[UIImageView alloc] initWithImage:dial1];
    UIImageView *dial2View = [[UIImageView alloc] initWithImage:dial2];
    UIImageView *dial3View = [[UIImageView alloc] initWithImage:dial3];
    UIImageView *dial4View = [[UIImageView alloc] initWithImage:dial4];
    
    float midW = dial4View.frame.size.width;
    float midH = dial4View.frame.size.height;
    float ydrop = 30.0f;
    dial1View.frame = CGRectMake((midW - dial1View.frame.size.width)/2, (midH - dial1View.frame.size.height)/2 + ydrop, dial1View.frame.size.width, dial1View.frame.size.height);
    dial2View.frame = CGRectMake((midW - dial2View.frame.size.width)/2, (midH - dial2View.frame.size.height)/2 + ydrop, dial2View.frame.size.width, dial2View.frame.size.height);
    dial3View.frame = CGRectMake((midW - dial3View.frame.size.width)/2, (midH - dial3View.frame.size.height)/2 + ydrop, dial3View.frame.size.width, dial3View.frame.size.height);
    dial4View.frame = CGRectMake(dial4View.frame.origin.x, dial4View.frame.origin.y + ydrop, dial4View.frame.size.width, dial4View.frame.size.height);
    
    accelerationTimeLabel.frame = CGRectMake((dial4View.frame.size.width - accelerationTimeLabel.frame.size.width)/2, 0, accelerationTimeLabel.frame.size.width, accelerationTimeLabel.frame.size.height);
    accelerationRangeLabel.frame = CGRectMake((dial4View.frame.size.width - accelerationRangeLabel.frame.size.width)/2, dial4View.frame.origin.y + dial4View.frame.size.height + 5, accelerationRangeLabel.frame.size.width, accelerationRangeLabel.frame.size.height);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, dial4View.frame.size.width, accelerationRangeLabel.frame.origin.y + accelerationRangeLabel.frame.size.height);
    
    
    [self addSubview:dial1View];
    [self addSubview:dial2View];
    [self addSubview:dial3View];
    [self addSubview:dial4View];
    
    [self addSubview:accelerationRangeLabel];
    [self addSubview:accelerationTimeLabel];
    
    _dial1View = dial1View;
    _dial2View = dial2View;
    _dial3View = dial3View;
    _dial4View = dial4View;
    
    _accelerationRangeLabel = accelerationRangeLabel;
    _accelerationTimeLabel = accelerationTimeLabel;
}

@end
