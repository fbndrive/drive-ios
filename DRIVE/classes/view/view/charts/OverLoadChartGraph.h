#import <UIKit/UIKit.h>

@interface OverLoadChartGraph : UIView

@property (nonatomic) float maximumOverload;
@property (nonatomic, retain) NSArray *dataSet;
@property (nonatomic, strong) UIColor *color;

- (void) setColor:(UIColor *)color;

@end
