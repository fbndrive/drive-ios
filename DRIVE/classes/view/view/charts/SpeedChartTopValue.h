#import <UIKit/UIKit.h>

@interface SpeedChartTopValue : UIView

@property (nonatomic) float value;
@property (nonatomic, strong) UIColor *color;

- (void) setColor:(UIColor *)color;

@end
