#import "AppDataManager.h"
#import "BoldFontLabel.h"
#import "SpeedChart.h"
#import "SpeedChartAvgValue.h"
#import "SpeedChartDial.h"
#import "SpeedChartTopSpeedLabel.h"
#import "SpeedChartTopValue.h"

@interface SpeedChart ()

@property (nonatomic, strong) BoldFontLabel *avgSpeedLabel;
@property (nonatomic, strong) BoldFontLabel *topSpeedLabel;
@property (nonatomic, strong) BoldFontLabel *avgSpeedValueLabel;
@property (nonatomic, strong) SpeedChartTopSpeedLabel *topSpeedValueLabel;
@property (nonatomic, strong) SpeedChartAvgValue *avgSpeedIndicator;
@property (nonatomic, strong) SpeedChartTopValue *topSpeedIndicator;

@end

@implementation SpeedChart

float const TOP_SPEED = 300.0f;

@synthesize avgSpeed = _avgSpeed;
@synthesize topSpeed = _topSpeed;

- (void) setForAnimarionAvgSpeed:(float)avgSpeed andTopSpeed: (float) topSpeed {
    _avgSpeedIndicator.value = fmaxf(0.01f, fminf((avgSpeed / TOP_SPEED), 1.0f));
    _topSpeedIndicator.value = fmaxf(0.01f, fminf((topSpeed / TOP_SPEED), 1.0f));
    
    [self fillAvgSpeedValueLabelWithValue:(int)avgSpeed];
    [self fillTopSpeedValueLabelWithValue:(int)topSpeed];
    
    [_avgSpeedIndicator setNeedsDisplay];
    [_topSpeedIndicator setNeedsDisplay];
    
    //NSLog(@"speed chart animation step / avg speed: %0.02f / top speed: %0.02f", avgSpeed, topSpeed);
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    return self;
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *color = [adm driveDescription].color;
    
    BoldFontLabel *avgSpeedLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    avgSpeedLabel.alpha = .5f;
    [avgSpeedLabel setTitle:@"AVG"];
    BoldFontLabel *topSpeedLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    topSpeedLabel.alpha = .5f;
    [topSpeedLabel setTitle:@"TOP"];
    
    avgSpeedLabel.frame = CGRectMake(-avgSpeedLabel.frame.size.width - 4, self.frame.size.height - avgSpeedLabel.frame.size.height + 5, avgSpeedLabel.frame.size.width, avgSpeedLabel.frame.size.height);
    topSpeedLabel.frame = CGRectMake((self.frame.size.width - topSpeedLabel.frame.size.width)/2, self.frame.size.height - topSpeedLabel.frame.size.height + 5, topSpeedLabel.frame.size.width, topSpeedLabel.frame.size.height);
    
    BoldFontLabel *avgSpeedValueLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self fillAvgSpeedValueLabelWithValue: 0];
    
    SpeedChartTopSpeedLabel *topSpeedValueLabel = [[SpeedChartTopSpeedLabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self fillTopSpeedValueLabelWithValue: 0];
    
    SpeedChartDial *dial = [[SpeedChartDial alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [dial setColor:color];
    dial.alpha = .1f;
    
    SpeedChartAvgValue *avgSpeedIndicator = [[SpeedChartAvgValue alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [avgSpeedIndicator setColor:color];
    avgSpeedIndicator.alpha = .3f;
    avgSpeedIndicator.value = 0.01f;
    
    SpeedChartTopValue *topSpeedIndicator = [[SpeedChartTopValue alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [topSpeedIndicator setColor:color];
    topSpeedIndicator.alpha = .5f;
    topSpeedIndicator.value = 0.01f;
    
    [self addSubview:dial];
    [self addSubview:avgSpeedIndicator];
    [self addSubview:topSpeedIndicator];
    [self addSubview:avgSpeedLabel];
    [self addSubview:topSpeedLabel];
    [self addSubview:avgSpeedValueLabel];
    [self addSubview:topSpeedValueLabel];
    
    _avgSpeedIndicator = avgSpeedIndicator;
    _topSpeedIndicator = topSpeedIndicator;
    _avgSpeedLabel = avgSpeedLabel;
    _topSpeedLabel = topSpeedLabel;
    _avgSpeedValueLabel = avgSpeedValueLabel;
    _topSpeedValueLabel = topSpeedValueLabel;
}

- (void) fillTopSpeedValueLabelWithValue: (int) value {
    [_topSpeedValueLabel setTitle: @(value).stringValue];
    
    _topSpeedValueLabel.frame = CGRectMake((self.frame.size.width - _topSpeedValueLabel.frame.size.width) / 2, _topSpeedLabel.frame.origin.y - _topSpeedValueLabel.frame.size.height + 5, _topSpeedValueLabel.frame.size.width, _topSpeedValueLabel.frame.size.height);
}

- (void) fillAvgSpeedValueLabelWithValue: (int) value {
    [_avgSpeedValueLabel setTitle: @(value).stringValue];
    
    float circleMiddle = self.frame.size.width / 2;
    float radius = self.frame.size.width / 2 + _avgSpeedValueLabel.frame.size.width;
    float theta = fmaxf(-.9f * M_PI, _avgSpeedIndicator.value);
    
    float xpos = circleMiddle + radius * cosf(theta) - (_avgSpeedValueLabel.frame.size.width / 2);
    float ypos = circleMiddle + radius * sinf(theta) - (_avgSpeedValueLabel.frame.size.height / 2);
    
    _avgSpeedValueLabel.frame = CGRectMake(xpos, ypos, _avgSpeedValueLabel.frame.size.width, _avgSpeedValueLabel.frame.size.height);
}


@end
