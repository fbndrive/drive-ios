#import "AppDataManager.h"
#import "Constants.h"
#import "SpeedChartTopSpeedLabel.h"

@implementation SpeedChartTopSpeedLabel

- (void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.font = [UIFont fontWithName: Font_boldFace size: 30];
    self.textColor = colour;
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.layer.shadowColor = [[adm driveDescription].color CGColor];
    self.layer.shadowRadius = 18.0f;
    self.layer.shadowOpacity = .9f;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.masksToBounds = NO;
}


@end
