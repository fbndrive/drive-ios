#import "SpeedChartDial.h"

@implementation SpeedChartDial

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void)drawRect:(CGRect)rect {
    CGFloat radius = rect.size.width/2;
    CGFloat radiusDecline = 20.0f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, _color.CGColor);
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    
    CGContextMoveToPoint(context, 0, radius);
    CGContextAddArc(context, radius, radius, radius, M_PI, 0, 0);
    CGContextAddLineToPoint(context, 2 * radius - radiusDecline, radius);
    CGContextAddArc(context, radius, radius, radius - radiusDecline, 0, M_PI, 1);
    CGContextAddLineToPoint(context, 0, radius);

    CGContextFillPath(context);
}

@end
