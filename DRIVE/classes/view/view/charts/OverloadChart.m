#import "AppDataManager.h"
#import "BoldFontLabel.h"
#import "CircleShapeNoStroke.h"
#import "OverloadChart.h"
#import "OverLoadChartDial.h"
#import "OverLoadChartGraph.h"
#import "OverloadChartLabel.h"

@interface OverloadChart ()

@property (nonatomic, retain) OverLoadChartGraph *graph;

@end

@implementation OverloadChart

@synthesize dataSet = _dataSet;
@synthesize maximumOverload = _maximumOverload;

- (id)initWithFrame:(CGRect)frame {
    _maximumOverload = 4.0f;
    
    self = [super initWithFrame:frame];
    
    return self;
}

- (void) setDataSet:(NSArray *)dataSet {
    _dataSet = dataSet;
    
    [_graph setDataSet:dataSet];
}

- (void) setForAnimarionMaxOverload:(float) maxOverload {
    _graph.maximumOverload = maxOverload;
    
    [_graph setNeedsDisplay];
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *color = [adm driveDescription].color;
    
    OverLoadChartDial *dialView = [[OverLoadChartDial alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [dialView setColor:color];
    dialView.alpha = .1f;
    
    BoldFontLabel *topLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 26)];
    [topLabel setTitle: NSLocalizedString(@"OverloadChart_breaking", nil)];
    
    BoldFontLabel *rightLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 26)];
    [rightLabel setTitle: NSLocalizedString(@"OverloadChart_right", nil)];
    
    BoldFontLabel *bottomLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 26)];
    [bottomLabel setTitle: NSLocalizedString(@"OverloadChart_acceleration", nil)];
    
    BoldFontLabel *leftLabel = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 26)];
    [leftLabel setTitle: NSLocalizedString(@"OverloadChart_left", nil)];
    
    float margin = 10.0f;
    
    topLabel.frame = CGRectMake((self.frame.size.width - topLabel.frame.size.width) / 2, -topLabel.frame.size.height - margin / 2, topLabel.frame.size.width, topLabel.frame.size.height);
    rightLabel.frame = CGRectMake(self.frame.size.width + margin, (self.frame.size.height - rightLabel.frame.size.height) / 2, rightLabel.frame.size.width, rightLabel.frame.size.height);
    bottomLabel.frame = CGRectMake((self.frame.size.width - bottomLabel.frame.size.width) / 2, self.frame.size.height + margin / 2, bottomLabel.frame.size.width, bottomLabel.frame.size.height);
    leftLabel.frame = CGRectMake(-leftLabel.frame.size.width - margin, (self.frame.size.height - leftLabel.frame.size.height) / 2, leftLabel.frame.size.width, leftLabel.frame.size.height);
    
    OverLoadChartGraph *graph = [[OverLoadChartGraph alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [graph setColor:color];
    
    _graph = graph;
    
    [self addSubview: dialView];
    [self addSubview: topLabel];
    [self addSubview: rightLabel];
    [self addSubview: bottomLabel];
    [self addSubview: leftLabel];
    [self addSubview: graph];
}


/*
- (void) drawCircleShapeForVertex: (CGPoint) vertex {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *color = [adm driveDescription].color;
    float circleRadius = 2.0f;
    
    CircleShapeNoStroke *circle = [[CircleShapeNoStroke alloc] initWithFrame:CGRectMake(vertex.x - circleRadius, vertex.y - circleRadius, circleRadius * 2.0f, circleRadius * 2.0f)];
    circle.fillColor = color;
    
    [self addSubview:circle];
}

- (void) drawLabelForVertex: (CGPoint) vertex {
    OverloadChartLabel *label = [[OverloadChartLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 26)];
    [label setTitle: [NSString stringWithFormat:@"%.01f", vertex.x]];
    
    float labelW = label.frame.size.width;
    float labelH = label.frame.size.height;
    float labelD = sqrtf(powf(labelW, 2.0f) + powf(labelH, 2.0f));
    float labelD_2 = labelD/2;
    
    // three points lies on the same line and we lavarage this
    
    CGPoint vertexCircleCenterPt = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
    float sloap = (vertex.y - vertexCircleCenterPt.y)/(vertex.x - vertexCircleCenterPt.x);
    float x0 = - ((vertex.y - vertexCircleCenterPt.y) * vertexCircleCenterPt.x)/(vertex.x - vertexCircleCenterPt.x) + vertexCircleCenterPt.y;
    
    float alpha = atanf(sloap);
    
    float labelX = (vertex.x > vertexCircleCenterPt.x) ? vertex.x + cosf(alpha) * labelD_2 : vertex.x - cosf(alpha) * labelD_2;
    float labelY = sloap * labelX + x0;
    
    if(isnan(labelY)) {
        labelY = (vertex.y > vertexCircleCenterPt.y) ? vertex.y + labelD_2 : vertex.y - labelD_2;
    }
    
    if(labelY == vertex.y) {
        labelX -= (labelW/2 + 5);
    } else {
        labelX -= labelW/2;
    }
    
    labelY -= labelH/2;
    
    label.frame = CGRectMake(labelX, labelY, labelW, labelH);
    
    [self addSubview:label];
    
    // scribbles
    
    
    //(pt2.x - pt1.x)(y - pt1.y) = (pt2.y - pt1.y)(x - pt1.x);
    //y - pt1.y = [(pt2.y - pt1.y)(x - pt1.x)]/(pt2.x - pt1.x);
    
    //y = [(pt2.y - pt1.y)(x - pt1.x)]/(pt2.x - pt1.x) + pt1.y;
    //y = [(pt2.y - pt1.y) * x - (pt2.y - pt1.y) * pt1.x)]/(pt2.x - pt1.x) + pt1.y;
    
    //y = ((pt2.y - pt1.y) * x)/(pt2.x - pt1.x) - ((pt2.y - pt1.y) * pt1.x))/(pt2.x - pt1.x) + pt1.y;
    
    //LightFontLabel *percentLabel = [[LightFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    //[percentLabel setTitle: [NSString stringWithFormat:@"%ld%%", lroundf(_percent*100)]];
}

- (void) drawLabelForVertex: (CGPoint) vertex {
    BoldFontLabel *label = [[BoldFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 26)];
    [label setTitle: [NSString stringWithFormat:@"%.02f", vertex.x]];
    
    float radiusDecrease = 20.0f;
    CGPoint arcCenter = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
    //NSLog(@"vertex.x vertex.y: %.02f %.02f", vertex.x, vertex.y);
    
    float xDistance = fmaxf(arcCenter.x, vertex.x) - fminf(arcCenter.x, vertex.x);
    float yDistance = fmaxf(arcCenter.y, vertex.y) - fminf(arcCenter.y, vertex.y);
    
    float radius = sqrtf(powf(xDistance, 2.0f) + powf(yDistance, 2.0f));
    
    float theta = acosf((vertex.x - arcCenter.x) / radius);
    
    //x = cx + r*cos(t);
    
    //NSLog(@"xDistance yDistance radius theta: %.02f %.02f %.02f %.02f", xDistance, yDistance, radius, theta);
    //NSLog(@"-------------------");
    
    float labelX = arcCenter.x + (radius + radiusDecrease) * cos(theta);
    float labelY = arcCenter.y + (radius + radiusDecrease) * sin(theta);
    
    label.frame = CGRectMake(labelX, labelY, label.frame.size.width, label.frame.size.height);
    
    //NSLog(@"check 3");
    
    [self addSubview:label];
    
    //LightFontLabel *percentLabel = [[LightFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    //[percentLabel setTitle: [NSString stringWithFormat:@"%ld%%", lroundf(_percent*100)]];
}*/


@end
