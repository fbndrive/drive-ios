#import <UIKit/UIKit.h>

@interface ElevationChartGraph : UIView

@property (nonatomic) float maximumElevation;
@property (nonatomic) float xStep;
@property (nonatomic, retain) NSArray *dataSet;

@property (nonatomic, strong) UIColor *color;

- (void) setColor:(UIColor *)color;

@end
