#import "AppDataManager.h"
#import "BoldFontLabel.h"
#import "ElevationChart.h"
#import "ElevationChartGraph.h"
#import "ElevationChartPointer.h"
#import "ElevationChartRules.h"
#import "ElevationChartVertex.h"

@interface ElevationChart ()

@property (nonatomic, retain) ElevationChartPointer *pointer;
@property (nonatomic, retain) BoldFontLabel *pointerDistanceIndicator;
@property (nonatomic, retain) BoldFontLabel *pointerElevationIndicator;
@property (nonatomic, retain) ElevationChartGraph *graph;

@end

@implementation ElevationChart

@synthesize dataSet = _dataSet;
// unit agnostic
@synthesize maximumElevation = _maximumElevation;

- (void) setDataSet:(NSArray *)dataSet {
    _dataSet = dataSet;
    [_graph setDataSet:dataSet];
    
    [_graph setNeedsDisplay];
    [self snapPointerToClosestVertex:CGPointMake(0, 0)];
    
    if([_dataSet count] < 2) {
        _graph.hidden = YES;
        _pointer.hidden = YES;
        _pointerDistanceIndicator.hidden = YES;
        _pointerElevationIndicator.hidden = YES;
    } else {
        _graph.hidden = NO;
        _pointer.hidden = NO;
        _pointerDistanceIndicator.hidden = NO;
        _pointerElevationIndicator.hidden = NO;
    }
}

- (void) setMaximumElevation:(float)maximumElevation {
    _maximumElevation = maximumElevation;
    _graph.maximumElevation = maximumElevation;
}

- (void) setForAnimationMaximum:(float) maximum {
    _graph.maximumElevation = maximum;
    [_graph setNeedsDisplay];
}

- (id)initWithFrame:(CGRect)frame {
    _maximumElevation = 2000.0f;
    _dataSet = [[NSArray alloc] init];
    
    self = [super initWithFrame:frame];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    panGesture.minimumNumberOfTouches = 1;
    panGesture.maximumNumberOfTouches = 1;
    
    [self addGestureRecognizer: panGesture];
    
    return self;
}

- (void)handlePanGesture:(UIPanGestureRecognizer *) sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        
    }
    
    if(_pointer) {
        CGPoint pt = [sender locationInView:self];
        [self snapPointerToClosestVertex:pt];
    }
    
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *color = [adm driveDescription].color;
    
    BoldFontLabel *pointerDistanceIndicator = [[BoldFontLabel alloc] init];
    
    BoldFontLabel *pointerElevationIndicator = [[BoldFontLabel alloc] init];
    
    ElevationChartRules *rules = [[ElevationChartRules alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    rules.color = color;
    rules.alpha = .5f;
    
    ElevationChartGraph *graph = [[ElevationChartGraph alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    graph.color = color;
    graph.alpha = .1f;
    graph.maximumElevation = _maximumElevation;
    graph.dataSet = _dataSet;
    
    ElevationChartPointer *pointer = [[ElevationChartPointer alloc] initWithFrame:CGRectMake(0, 0, 2, self.frame.size.height)];
    pointer.color = color;
    
    [self addSubview:rules];
    [self addSubview:graph];
    [self addSubview:pointer];
    [self addSubview:pointerElevationIndicator];
    [self addSubview:pointerDistanceIndicator];
    
    _pointer = pointer;
    _pointerDistanceIndicator = pointerDistanceIndicator;
    _pointerElevationIndicator = pointerElevationIndicator;
    _graph = graph;
}

- (void) snapPointerToClosestVertex: (CGPoint) point {
    int i = -1;
    int q = (int)[_dataSet count];
    
    if(q <= 0) {
        return;
    }
    
    float distance, minDistance, xPos;
    float closestX = 0.0f;
    
    minDistance = INFINITY;
    
    ElevationChartVertex *closestPt = (ElevationChartVertex *) [_dataSet objectAtIndex:0];
    
    while (++i < q) {
        xPos = (i * _graph.xStep);
        distance = fabsf(xPos - point.x);
        
        if(distance < minDistance) {
            minDistance = distance;
            closestX = xPos;
            closestPt = (ElevationChartVertex *) [_dataSet objectAtIndex:i];
        }
    }
    
    _pointer.frame = CGRectMake(closestX, _pointer.frame.origin.y, _pointer.frame.size.width, _pointer.frame.size.height);
    [_pointerElevationIndicator setTitle: [NSString stringWithFormat:@"%d %@", (int) closestPt.elevation, @"M" ]];
    [_pointerDistanceIndicator setTitle: [NSString stringWithFormat:@"%d %@", (int) closestPt.distance, @"KM" ]];
    [self adjustPointerLabels];
}

- (void) adjustPointerLabels {
    _pointerDistanceIndicator.frame = CGRectMake(_pointer.frame.origin.x - _pointerDistanceIndicator.frame.size.width / 2, _pointer.frame.origin.y + _pointer.frame.size.height, _pointerDistanceIndicator.frame.size.width, _pointerDistanceIndicator.frame.size.height);
    
    _pointerElevationIndicator.frame = CGRectMake(_pointer.frame.origin.x - _pointerElevationIndicator.frame.size.width / 2, -_pointerElevationIndicator.frame.size.height, _pointerElevationIndicator.frame.size.width, _pointerElevationIndicator.frame.size.height);
}

@end
