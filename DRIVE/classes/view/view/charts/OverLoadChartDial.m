#import "Constants.h"
#import "OverLoadChartDial.h"

@implementation OverLoadChartDial

@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetStrokeColorWithColor(context,
                                     _color.CGColor);
    
    float radius = rect.size.width / 2.0f;
                                     
    float radiusDescent = rect.size.width * .15f;
    CGPoint arcCenter = CGPointMake(rect.origin.x + rect.size.width/2, rect.origin.y + rect.size.height/2);
    
    CGContextMoveToPoint(context, arcCenter.x + radius - radiusDescent * 3.0f, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius - radiusDescent * 3.0f, 0, My_PI * 2.0f, YES);
    
    CGContextMoveToPoint(context, arcCenter.x + radius - radiusDescent * 2.0f, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius - radiusDescent * 2.0f, 0, My_PI * 2, YES);
    
    CGContextMoveToPoint(context, arcCenter.x + radius - radiusDescent, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius - radiusDescent, 0.0f, My_PI * 2, YES);
    
    CGContextMoveToPoint(context, arcCenter.x + radius, arcCenter.y);
    CGContextAddArc(context, arcCenter.x, arcCenter.y, radius, 0.0f, My_PI * 2, YES);
    
    CGContextMoveToPoint(context, rect.size.width/2.0f, 0);
    CGContextAddLineToPoint(context, rect.size.width/2.0f, rect.size.height);
    CGContextMoveToPoint(context, 0,rect.size.height/2.0f);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height/2.0f);
    
    CGContextStrokePath(context);
}

@end
