#import <UIKit/UIKit.h>

@interface SpeedChartDial : UIView

@property (nonatomic, strong) UIColor *color;

- (void) setColor:(UIColor *)color;

@end
