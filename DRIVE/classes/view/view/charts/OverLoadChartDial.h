#import <UIKit/UIKit.h>

@interface OverLoadChartDial : UIView

@property (nonatomic, strong) UIColor *color;

- (void) setColor:(UIColor *)color;

@end
