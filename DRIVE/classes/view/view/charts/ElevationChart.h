#import "BasicChartView.h"

@interface ElevationChart : BasicChartView

@property (nonatomic) float maximumElevation;
@property (nonatomic, retain) NSArray *dataSet;

- (void) setForAnimationMaximum:(float) maximum;

@end
