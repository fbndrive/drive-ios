#import "ElevationChartGraph.h"
#import "ElevationChartVertex.h"

@implementation ElevationChartGraph

@synthesize maximumElevation = _maximumElevation;
@synthesize xStep = _xStep;
@synthesize dataSet = _dataSet;
@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    _dataSet = [[NSArray alloc] init];
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void) setDataSet:(NSArray *)dataSet {
    _dataSet = dataSet;
    
    
    int q = (int)[dataSet count];
    
    if(q > 1) {
        _xStep = self.frame.size.width / (q - 1);
    } else {
        _xStep = self.frame.size.width;
    }
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void)drawRect:(CGRect)rect {
    if([_dataSet count] <= 0) {
        return;
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetStrokeColorWithColor(context,
                                     _color.CGColor);
    CGContextSetFillColorWithColor(context, _color.CGColor);
    
    int i = -1;
    int q = (int)[_dataSet count];
    
    ElevationChartVertex *pt;
    float xPos = 0;
    float yPos;
    float graphH = rect.size.height;
    
    CGContextMoveToPoint(context, rect.origin.x, rect.size.height);
    
    while (++i < q) {
        pt = (ElevationChartVertex *) [_dataSet objectAtIndex:i];
        
        yPos = graphH - fminf((pt.elevation * graphH) / _maximumElevation, graphH);
        
        CGContextAddLineToPoint(context, xPos, yPos);
        //NSLog(@"Elevation Chart Graph i: %d, xpos: %f, elevation: %f", i, xPos, yPos);
        
        xPos += _xStep;
    }
    
    // FIX here we hit Assertion failed: (CGFloatIsValid(x) && CGFloatIsValid(y)), function void CGPathAddLineToPoint(CGMutablePathRef, const CGAffineTransform *, CGFloat, CGFloat), file Paths/CGPath.cc, line 265.
    
    CGContextAddLineToPoint(context, xPos, rect.size.height);
    CGContextAddLineToPoint(context, rect.origin.x, rect.size.height);
    
    CGContextFillPath(context);
}


@end
