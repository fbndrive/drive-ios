#import <Foundation/Foundation.h>

@interface ElevationChartVertex : NSObject

@property (nonatomic) float elevation;
@property (nonatomic) float distance;

+ (ElevationChartVertex *) initWithElevation: (float) elevation andDistance: (float) distance;

@end