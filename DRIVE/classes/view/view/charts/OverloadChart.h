#import "BasicChartView.h"

@interface OverloadChart : BasicChartView

@property (nonatomic, retain) NSArray *dataSet;
@property (nonatomic) float maximumOverload;

- (void) setForAnimarionMaxOverload:(float) maxOverload;

@end
