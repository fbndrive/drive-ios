#import "SpeedChartTopValue.h"

@implementation SpeedChartTopValue

@synthesize value = _value;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void) setValue:(float)value {
    _value = (1 - value) * -M_PI;
}

- (void)drawRect:(CGRect)rect {
    CGFloat radius = rect.size.width/2;
    CGFloat radiusIndent = 10.0f;
    CGFloat radiusDecline = 10.0f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    
    CGContextMoveToPoint(context, radiusIndent, radius);
    CGContextAddArc(context, radius, radius, radius - radiusIndent, M_PI, _value, 0);
    CGContextAddLineToPoint(context, radius + (2 * radius - radiusIndent - radiusDecline) * cos(_value), radius + (2 * radius - radiusIndent - radiusDecline) * sin(_value));
    CGContextAddArc(context, radius, radius, radius - radiusDecline - radiusIndent, _value, M_PI, 1);
    CGContextAddLineToPoint(context, radiusIndent, radius);
    
    CGContextFillPath(context);
}


@end
