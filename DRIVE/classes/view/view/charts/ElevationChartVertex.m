#import "ElevationChartVertex.h"

@implementation ElevationChartVertex

@synthesize elevation = _elevation;
@synthesize distance = _distance;

+ (ElevationChartVertex *) initWithElevation: (float) elevation andDistance: (float) distance {
    ElevationChartVertex *ecv = [[ElevationChartVertex alloc] init];
    
    ecv.elevation = elevation;
    ecv.distance = distance;
    
    return ecv;
}

@end
