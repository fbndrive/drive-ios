#import "OverLoadChartGraph.h"
#import "OverloadChartLabel.h"

@interface OverLoadChartGraph ()

@property (nonatomic, retain) NSMutableArray *labelArray;

@end

@implementation OverLoadChartGraph

@synthesize maximumOverload = _maximumOverload;
@synthesize dataSet = _dataSet;
@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    _maximumOverload = 4.0f;
    _dataSet = [[NSArray alloc] init];
    self.backgroundColor = [UIColor clearColor];
    
    _labelArray = [[NSMutableArray alloc] init];
    
    return self;
}

- (void) setDataSet:(NSArray *)dataSet {
    _dataSet = dataSet;
    
    [self removeLabels];
    [self drawLabels];
    
    [self setNeedsDisplay];
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void)drawLabels {
    int i = -1;
    int q = (int)[_dataSet count];
    
    OverloadChartLabel *label;
    
    while(++i < q) {
        label = [[OverloadChartLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 26)];
        //[label setTitle: [NSString stringWithFormat:@"%.01f", vertex.x]];
        
        [self addSubview:label];
        
        [_labelArray addObject: label];
    }
}

- (void) removeLabels {
    int i = -1;
    int q = (int)[_labelArray count];
    
    UIView *label;
    
    while(++i < q) {
        label = [_labelArray objectAtIndex:i];
        [label removeFromSuperview];
    }
    
    _labelArray = [[NSMutableArray alloc] init];
}

- (void)drawRect:(CGRect)rect {
    int i = -1;
    int q = (int)[_dataSet count];

    if(q <= 0) {
        return;
    }
    
    CGFloat *components = CGColorGetComponents([_color CGColor]);
    UIColor *areaColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:.2];
    UIColor *dotColor = _color;
    
    float maxDistanceFromCenter = rect.size.width * .4f;
    float absDistanceFromCenter = maxDistanceFromCenter; // related to maximum overload
    
    CGPoint centerPoint = CGPointMake(rect.size.width / 2, rect.size.height / 2);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0f);
    //CGContextSetStrokeColorWithColor(context, areaColor.CGColor);
    CGContextSetFillColorWithColor(context, areaColor.CGColor);
    
    BOOL first = NO;
    
    float overloadValue;
    CGPoint graphPt;
    CGPoint firstGraphPt;
    
    OverloadChartLabel *label;
    float labelW, labelH, labelX, labelY;
    
    NSMutableArray *vertextes = [[NSMutableArray alloc] init];
    
    while (++i < q) {
        overloadValue = [[_dataSet objectAtIndex:i] floatValue];
        
        absDistanceFromCenter = fminf((overloadValue * maxDistanceFromCenter) / _maximumOverload, maxDistanceFromCenter);
        
        label = [_labelArray objectAtIndex:i];
        [label setTitle: [NSString stringWithFormat:@"%.01f", overloadValue]];
        
        labelW = label.frame.size.width;
        labelH = label.frame.size.height;
        
        if(i == 0) {
            graphPt = CGPointMake(centerPoint.x, centerPoint.y - absDistanceFromCenter);
            labelX = graphPt.x - labelW / 2;
            labelY = graphPt.y - labelH - 3;
        }
        if(i == 1) {
            graphPt = CGPointMake(centerPoint.x + absDistanceFromCenter, centerPoint.y);
            labelX = graphPt.x + 5;
            labelY = graphPt.y - labelH/2;
        }
        if(i == 2) {
            graphPt = CGPointMake(centerPoint.x, centerPoint.y + absDistanceFromCenter);
            labelX = graphPt.x - labelW/2;
            labelY = graphPt.y + 5;
        }
        if(i == 3) {
            graphPt = CGPointMake(centerPoint.x - absDistanceFromCenter, centerPoint.y);
            labelX = graphPt.x - labelW - 5;
            labelY = graphPt.y - labelH/2;
        }
        
        [vertextes addObject: [NSValue valueWithCGPoint: graphPt]];
        label.frame = CGRectMake(labelX, labelY, labelW, labelH);
        
        if(!first) {
            first = YES;
            firstGraphPt = graphPt;
            
            CGContextMoveToPoint(context, graphPt.x, graphPt.y);
            continue;
        }
        
        CGContextAddLineToPoint(context, graphPt.x, graphPt.y);
    }
    
    CGContextAddLineToPoint(context, firstGraphPt.x, firstGraphPt.y);
    
    CGContextFillPath(context);
    
    // draw dots
    
    CGContextSetFillColorWithColor(context, dotColor.CGColor);
    
    i = -1;
    q = (int)[vertextes count];
    
    CGPoint vertex;
    
    while(++i < q) {
        vertex = [[vertextes objectAtIndex:i] CGPointValue];
        CGContextAddEllipseInRect(context, CGRectMake(vertex.x - 2, vertex.y - 2, 4, 4));
    }
    
    CGContextFillPath(context);
}

@end
