#import "ElevationChartPointer.h"

@implementation ElevationChartPointer

@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetFillColorWithColor(context, _color.CGColor);
    
    // draw rules
    
    CGContextFillRect(context, rect);
    
    NSLog(@"test");
}

@end
