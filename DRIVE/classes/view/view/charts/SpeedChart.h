#import "BasicChartView.h"

@interface SpeedChart : BasicChartView

extern float const MIN_SPEED;
extern float const TOP_SPEED;

@property (nonatomic) float avgSpeed;
@property (nonatomic) float topSpeed;

- (void) setForAnimarionAvgSpeed:(float)avgSpeed andTopSpeed: (float) topSpeed;

@end
