#import <UIKit/UIKit.h>

@interface ElevationChartRules : UIView

@property (nonatomic, strong) UIColor *color;

- (void) setColor:(UIColor *)color;

@end
