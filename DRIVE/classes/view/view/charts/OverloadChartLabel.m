#import "AppDataManager.h"
#import "Constants.h"
#import "OverloadChartLabel.h"

@implementation OverloadChartLabel

- (void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.font = [UIFont fontWithName: Font_boldFace size: 12];
    self.textColor = colour;
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}

@end