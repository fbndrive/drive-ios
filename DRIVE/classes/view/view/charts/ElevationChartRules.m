#import "ElevationChartRules.h"

@implementation ElevationChartRules

@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0f);
    CGContextSetStrokeColorWithColor(context,
                                     _color.CGColor);
    
    // draw rules
    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + 1);
    CGContextAddLineToPoint(context, rect.size.width, rect.origin.y);
    CGContextMoveToPoint(context, rect.origin.x, rect.size.height/2);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height/2);
    CGContextMoveToPoint(context, rect.origin.x, rect.size.height - 1);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height - 1);
    
    CGContextStrokePath(context);
}

@end
