#import "BasicChartView.h"
#import "BoldFontLabel.h"
#import "RegularFontLabel.h"

@interface AccelerationChart : BasicChartView

extern float const FASTEST_0_100_ACC;
extern float const SLOWEST_0_100_ACC;

@property (nonatomic) float value;

- (void) setValue:(float)value;
- (void) setForAnimarionValue:(float) value;

- (void) setRangeLabelText:(NSString *) text;
- (void) setTimeLabelFromValue:(float) value;

@end
