#import "Constants.h"
#import "SpeedChartAvgValue.h"

@interface SpeedChartAvgValue ()

@end

@implementation SpeedChartAvgValue

@synthesize value = _value;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void) setValue:(float)value {
    _value = (1 - value) * -M_PI;
}

- (void)drawRect:(CGRect)rect {
    CGFloat radius = rect.size.width/2;
    CGFloat radiusDecline = 10.0f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    
    CGContextMoveToPoint(context, 0, radius);
    CGContextAddArc(context, radius, radius, radius, M_PI, _value, 0);
    CGContextAddLineToPoint(context, radius + (2 * radius - radiusDecline) * cos(_value), radius + (2 * radius - radiusDecline) * sin(_value));
    CGContextAddArc(context, radius, radius, radius - radiusDecline, _value, M_PI, 1);
    CGContextAddLineToPoint(context, 0, radius);
    
    CGContextFillPath(context);
}

@end
