#import <UIKit/UIKit.h>

@interface ElevationChartPointer : UIView

@property (nonatomic, strong) UIColor *color;

- (void) setColor:(UIColor *)color;

@end
