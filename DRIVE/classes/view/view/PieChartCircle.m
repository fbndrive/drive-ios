#import "PieChartCircle.h"

@implementation PieChartCircle

@synthesize color = _color;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) setSize:(CGSize)size andColor:(UIColor *) color {
    _color = color;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void) setSize:(CGSize)size {
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rectangle = CGRectMake(0,0, self.frame.size.width, self.frame.size.height);
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    CGContextFillEllipseInRect(context, rectangle);
    CGContextStrokePath(context);
}

@end
