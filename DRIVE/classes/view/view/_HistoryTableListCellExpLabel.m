#import "Constants.h"
#import "_HistoryTableListCellExpLabel.h"

@implementation _HistoryTableListCellExpLabel

-(void) applyFormatting {
    self.font = [UIFont fontWithName: Font_regularFace size: 14];
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}

@end
