#import "AppDataManager.h"
#import "ArrowArcClockView.h"
#import "Constants.h"
#import "DottedArc.h"
#import "UIImage+Tint.h"

@interface ArrowArcClockView ()

@property (nonatomic, strong) UIImageView *clockHand;
@property (nonatomic, strong) DottedArc *clockIndicator;
@property (nonatomic, strong) DottedArc *clockShadow;
@property (nonatomic) int range;
@property (nonatomic) float animationDuration;

// animation helpers
@property (nonatomic, strong) NSDate *rotationStartTime;
@property (nonatomic) float startRotation;
@property (nonatomic) float changeInRotation;

// CGFloat radians = atan2f(yourView.transform.b, yourView.transform.a);

@end

@implementation ArrowArcClockView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _animationDuration = 3;
        _range = 0;
        [self drawUI];
    }
    
    return self;
}

- (void) animateToValue: (float) value {
    _rotationStartTime = [NSDate date];
    
    _startRotation = atan2f(_clockHand.transform.b, _clockHand.transform.a);
    _changeInRotation = value - _startRotation;
    
    //float nv = [self getNextStepForCurrentTime:[[NSDate date] timeIntervalSinceDate:_rotationStartTime] fromStart:_startRotation withChange:_changeInRotation andDuration:3];
    
    [self animateRangeTick:nil];
}

- (void) setClockValue: (float) value {
    if(value > _range) {
        value = 2 * _range - value;
    }
    
    _clockIndicator.finalAngle = value;
    _clockHand.transform = CGAffineTransformMakeRotation(value);
    
    [_clockIndicator setNeedsDisplay];
}

- (void)drawUI {
    self.clipsToBounds = NO;
    self.backgroundColor = [UIColor clearColor];
    
    [self drawClockIndicator];
    [self drawClockHand];
    [self drawClockShadow];
    
    CGRect frame = self.frame;
    frame.size.width = _clockShadow.frame.size.width;
    frame.size.height = _clockShadow.frame.size.height;
    self.frame = frame;
    
    [self addSubview:_clockIndicator];
    [self addSubview:_clockShadow];
    [self addSubview:_clockHand];
}

- (void)drawClockHand {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIImage *image = [[UIImage imageNamed:@"clock-hand"] tintedImageWithColor:[adm driveDescription].color];
    
    _clockHand = [[UIImageView alloc]initWithImage:image];
    _clockHand.frame = CGRectMake(0, 80, _clockHand.frame.size.width, _clockHand.frame.size.height);
    
    CGPoint oldOrigin = _clockHand.frame.origin;
    _clockHand.layer.anchorPoint = CGPointMake(0, .5);
    CGPoint newOrigin = _clockHand.frame.origin;
    
    CGPoint transition;
    transition.x = newOrigin.x - oldOrigin.x;
    transition.y = newOrigin.y - oldOrigin.y;
    
    _clockHand.center = CGPointMake (_clockHand.center.x - transition.x, _clockHand.center.y - transition.y);
    
    _clockHand.transform = CGAffineTransformMakeRotation(-M_PI/2);
}

- (void)drawClockIndicator {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIColor *indicatorColor = [adm driveDescription].color;
    
    _clockIndicator = [[DottedArc alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    _clockIndicator.backgroundColor = [UIColor clearColor];
    _clockIndicator.finalAngle = -M_PI/2;
    _clockIndicator.dotColor = indicatorColor;
}

- (void)drawClockShadow {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIColor *shadowColor = [adm driveDescription].colorAlpha;
    
    _clockShadow = [[DottedArc alloc] initWithFrame:CGRectMake(0, 0, 85, 85)];
    _clockShadow.backgroundColor = [UIColor clearColor];
    _clockShadow.finalAngle = 0;
    _clockShadow.dotColor = shadowColor;
}

-(void) animateRangeTick: (NSTimer *) timer {
    
    float timeDifference = [[NSDate date] timeIntervalSinceDate:_rotationStartTime];
    
    if(timeDifference >= _animationDuration) {
        [self setClockValue:_startRotation + _changeInRotation];
        
        return;
    }
    
    float nv = [self getNextStepForCurrentTime:timeDifference fromStart:_startRotation withChange:_changeInRotation andDuration:_animationDuration];
    
    [self setClockValue: nv];
    
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(animateRangeTick:)
                                   userInfo:nil
                                    repeats:NO];
    
    //NSLog(@"nv: %f", nv);
}

-(float) getNextStepForCurrentTime: (float) time fromStart: (float) start withChange: (float) change andDuration: (float) duration  {
    time /= duration;
    return -change * time*(time-2) + start;
    
    //time /= duration;
	//return change*time*time*time*time*time + start;
}

/*
 
 Math.easeOutQuad = function (t, b, c, d) {
    t /= d;
    return -c * t*(t-2) + b;
 };
 
 */

@end
