#import <UIKit/UIKit.h>

@interface TriangleShapedHBarTriangle : UIView

@property (nonatomic) CGSize size;
@property (nonatomic, strong) UIColor *color;

- (void) setSize:(CGSize)size andColor:(UIColor *) color;

@end
