#import "AppDataManager.h"
#import "Constants.h"
#import "LightFontLabel.h"
#import "TriangleShapedHBarChart.h"
#import "TriangleShapedHBarSlice.h"
#import "TriangleShapedHBarTriangle.h"
#import "UIImage+Tint.h"

@interface TriangleShapedHBarChart ()

@property (nonatomic) float animationDuration;

@property (nonatomic,strong) LightFontLabel *minValueLabel;
@property (nonatomic,strong) LightFontLabel *midValueLabel;
@property (nonatomic,strong) LightFontLabel *maxValueLabel;
@property (nonatomic,strong) UIImageView *minValueIcon;
@property (nonatomic,strong) UIImageView *midValueIcon;
@property (nonatomic,strong) UIImageView *maxValueIcon;
@property (nonatomic,strong) TriangleShapedHBarSlice *barSlice;
@property (nonatomic,strong) TriangleShapedHBarTriangle *barBackground;

// animation helpers
@property (nonatomic, strong) NSDate *animationStartTime;
@property (nonatomic) float animStartValue;
@property (nonatomic) float animChangeValue;

@end

@implementation TriangleShapedHBarChart

@synthesize size = _size;
@synthesize minimum = _minimum;
@synthesize maximum = _maximum;
@synthesize value = _value;

- (id)initWithFrame:(CGRect)frame {
    _animationDuration = COMPLETE_ANIM_CYCLE_DURATION;
    _size = frame.size;
    _minimum = 0.0f;
    _maximum = 10.0f;
    _value = 0.0f;
    
    self = [super initWithFrame:frame];
    
    return self;
}

- (void) refresh {
    [_barBackground setNeedsDisplay];
    [_barSlice setNeedsDisplay];
}

- (void) arrange {
    //CGRect percentLabelFrame = _percentLabel.frame;
    //CGRect nameLabelFrame = _nameLabel.frame;
    
    _minValueIcon.frame = CGRectMake(0, 0, _minValueIcon.frame.size.width, _minValueIcon.frame.size.height);
    _midValueIcon.frame = CGRectMake(_size.width/2 - _midValueIcon.frame.size.width/2, 0, _midValueIcon.frame.size.width, _midValueIcon.frame.size.height);
    _maxValueIcon.frame = CGRectMake(_size.width - _maxValueIcon.frame.size.width, 0, _maxValueIcon.frame.size.width, _maxValueIcon.frame.size.height);
    
    _barBackground.frame = CGRectMake(0, _minValueIcon.frame.origin.y + _minValueIcon.frame.size.height + 10, _size.width, _size.height);
    _barSlice.frame = CGRectMake(0, _minValueIcon.frame.origin.y + _minValueIcon.frame.size.height + 10, _size.width, _size.height);
    
    CGRect barBackgroundFrame = _barBackground.frame;
    
    _minValueLabel.frame = CGRectMake(0, barBackgroundFrame.origin.y + barBackgroundFrame.size.height, _minValueLabel.frame.size.width, _minValueLabel.frame.size.height);
    _midValueLabel.frame = CGRectMake(barBackgroundFrame.size.width/2 - _midValueLabel.frame.size.width, barBackgroundFrame.origin.y + barBackgroundFrame.size.height, _midValueLabel.frame.size.width, _midValueLabel.frame.size.height);
    _maxValueLabel.frame = CGRectMake(barBackgroundFrame.size.width - _maxValueLabel.frame.size.width, barBackgroundFrame.origin.y + barBackgroundFrame.size.height, _maxValueLabel.frame.size.width, _maxValueLabel.frame.size.height);
}


- (void) animateToValue: (float) value {
    _animationStartTime = [NSDate date];
    _animStartValue = _value;
    _animChangeValue = value - _animStartValue;
    _value = value;
    
    _animationDuration = fabsf(COMPLETE_ANIM_CYCLE_DURATION * (_animChangeValue/(_maximum - _minimum)));
    
    [self animateRangeTick:nil];
}

- (void) setMinimum:(float) minimum andMaximum:(float) maximum {
    _minimum = minimum;
    _maximum = maximum;
    
    [self setValue:_value];
}

- (void) setSize:(CGSize)size {
    _size = size;
    
    [_barBackground setSize:_size];
    [_barSlice setDelta:[self getSliceDeltaForValue:_value]];
}

- (void) setValue:(float)value {
    _value = value;
    
    if(_value < _minimum) _value = _minimum;
    if(_value > _maximum) _value = _maximum;
    
    [self updateLabelsValues];
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    TriangleShapedHBarTriangle *barBackground = [[TriangleShapedHBarTriangle alloc] initWithFrame:CGRectMake(0, 0, _size.width, _size.height)];
    [barBackground setSize:_size andColor:colour];
    barBackground.alpha = .1f;
    
    TriangleShapedHBarSlice *barSlice = [[TriangleShapedHBarSlice alloc] initWithFrame:CGRectMake(0, 0, _size.width, _size.height)];
    [barSlice setSize:_size andColor:colour];
    barSlice.alpha = .3f;
    
    LightFontLabel *minValueLabel = [[LightFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    LightFontLabel *midValueLabel = [[LightFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    LightFontLabel *maxValueLabel = [[LightFontLabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [self updateLabelsValues];
    
    UIImage *minValueIconImage = [[UIImage imageNamed:@"bike-icon"] tintedImageWithColor:colour];
    UIImageView *minValueIcon = [[UIImageView alloc] initWithImage:minValueIconImage];
    minValueIcon.alpha = .5;
    
    UIImage *midValueIconImage = [[UIImage imageNamed:@"f1-icon"] tintedImageWithColor:colour];
    UIImageView *midValueIcon = [[UIImageView alloc] initWithImage:midValueIconImage];
    midValueIcon.alpha = .5;
    
    UIImage *maxValueIconImage = [[UIImage imageNamed:@"jet-icon"] tintedImageWithColor:colour];
    UIImageView *maxValueIcon = [[UIImageView alloc] initWithImage:maxValueIconImage];
    maxValueIcon.alpha = .5;
    
    [self addSubview: barBackground];
    [self addSubview: barSlice];
    [self addSubview: minValueLabel];
    [self addSubview: midValueLabel];
    [self addSubview: maxValueLabel];
    [self addSubview: minValueIcon];
    [self addSubview: midValueIcon];
    [self addSubview: maxValueIcon];
    
    _barBackground = barBackground;
    _barSlice = barSlice;
    _minValueLabel = minValueLabel;
    _midValueLabel = midValueLabel;
    _maxValueLabel = maxValueLabel;
    _minValueIcon = minValueIcon;
    _midValueIcon = midValueIcon;
    _maxValueIcon = maxValueIcon;
}

- (void) updateLabelsValues {
    [_minValueLabel setTitle: [NSString stringWithFormat:@"%ld", lroundf(_minimum)]];
    [_midValueLabel setTitle: [NSString stringWithFormat:@"%ld", lroundf(_minimum + (_maximum - _minimum) / 2)]];
    [_maxValueLabel setTitle: [NSString stringWithFormat:@"%ld", lroundf(_maximum)]];
}

-(void) animateRangeTick: (NSTimer *) timer {
    
    float timeDifference = [[NSDate date] timeIntervalSinceDate: _animationStartTime];
    
    if(timeDifference >= _animationDuration) {
        [_barSlice setDelta:[self getSliceDeltaForValue:_value]];
        [_barSlice setNeedsDisplay];
        
        return;
    }
    
    float nv = [self getNextStepForCurrentTime:timeDifference fromStart:_animStartValue withChange:_animChangeValue andDuration:_animationDuration];
    
    [_barSlice setDelta:[self getSliceDeltaForValue:nv]];
    [_barSlice setNeedsDisplay];
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(animateRangeTick:)
                                   userInfo:nil
                                    repeats:NO];
    
    //NSLog(@"nv: %f", nv);
}

-(float) getNextStepForCurrentTime: (float) time fromStart: (float) start withChange: (float) change andDuration: (float) duration  {
    time /= duration;
    return -change * time*(time-2) + start;
    
    //time /= duration;
	//return change*time*time*time*time*time + start;
}

-(float) getSliceDeltaForValue: (float) value {
    return (value - _minimum)/(_maximum - _minimum);
}

@end
