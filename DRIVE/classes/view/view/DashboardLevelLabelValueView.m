#import "AppDataManager.h"
#import "Constants.h"
#import "DashboardLevelLabelValueView.h"

@interface DashboardLevelLabelValueView ()

@end

@implementation DashboardLevelLabelValueView

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 78];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
}

- (void) applyFormatLabel {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 12];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.labelLabel.font = fontFace;
    self.labelLabel.textColor = fontColor;
}

- (void) sizeMe {
    [self.valueLabel sizeToFit];
    [self.labelLabel sizeToFit];
    
    self.valueLabel.frame = CGRectMake(0, self.labelLabel.frame.size.height - 25, self.valueLabel.frame.size.width + 5, self.valueLabel.frame.size.height);
    self.labelLabel.frame = CGRectMake(0, 0, self.labelLabel.frame.size.width, self.labelLabel.frame.size.height);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.valueLabel.frame.size.width, self.valueLabel.frame.size.height + self.labelLabel.frame.size.height);
}

- (void) drawUI {
    [super drawUI];
    
    self.valueLabel.layer.shadowRadius = 10.0f;
    self.valueLabel.layer.shadowOpacity = .6f;
    
    self.labelLabel.layer.shadowRadius = 10.0f;
    self.labelLabel.layer.shadowOpacity = .6f;
}


/*
- (void) setValueLabel:(NSInteger) value withLabel: (NSString *) label withStyle: (NSString *) style {
    NSString *formattedValueString = [self convertValueToString:value];
    
    if([style isEqualToString:Style_labeledValueBoldLabelGlow]) {
        //setting up value label
        NSMutableAttributedString *valueAttString = [[NSMutableAttributedString alloc] initWithString:formattedValueString];
        NSInteger valueStringLength = [formattedValueString length];
        
        UIFont *fontSmall = [UIFont fontWithName: @"Oswald-Bold" size: 12];
        UIFont *fontBig = [UIFont fontWithName: @"Oswald-Bold" size: 78];
        UIColor *fontColor = [Constants Color_perfromanceValue];
        UIColor *clearColor = [UIColor clearColor];
        UIColor *bgColor = [UIColor clearColor];
        
        [valueAttString addAttribute:NSFontAttributeName value:fontBig range:NSMakeRange(0, valueStringLength)];
        [valueAttString addAttribute:NSForegroundColorAttributeName value:fontColor range:NSMakeRange(0, valueStringLength)];
        [valueAttString addAttribute:NSBackgroundColorAttributeName value:bgColor range:NSMakeRange(0, valueStringLength)];
        
        _valueLabel.attributedText = valueAttString;
        
        NSMutableAttributedString *labelAttString = [[NSMutableAttributedString alloc] initWithString:[label uppercaseString]];
        NSInteger labelStringLength = [label length];
        
        [labelAttString addAttribute:NSFontAttributeName value:fontSmall range:NSMakeRange(0, labelStringLength)];
        [labelAttString addAttribute:NSForegroundColorAttributeName value:fontColor range:NSMakeRange(0, labelStringLength)];
        [labelAttString addAttribute:NSBackgroundColorAttributeName value:bgColor range:NSMakeRange(0, labelStringLength)];
        
        _labelLabel.attributedText = labelAttString;
        
        CGSize maximumSize = CGSizeMake(296, FLT_MAX);
        CGSize expectedValueSize = [formattedValueString sizeWithFont:font constrainedToSize:maximumSize lineBreakMode: _valueLabel.lineBreakMode];
        CGSize expectedLabelSize = [label sizeWithFont:font constrainedToSize:maximumSize lineBreakMode: _labelLabel.lineBreakMode];
        
        _valueLabel.frame = CGRectMake(expectedLabelSize.width, 0, expectedValueSize.width + 5, expectedValueSize.height);
        _labelLabel.frame = CGRectMake(0, expectedValueSize.height - 12, expectedLabelSize.width + 5, expectedLabelSize.height);
             
        [_valueLabel sizeToFit];
        [_labelLabel sizeToFit];
        
        _valueLabel.frame = CGRectMake(0, _labelLabel.frame.size.height - 25, _valueLabel.frame.size.width + 5, _valueLabel.frame.size.height);
        _labelLabel.frame = CGRectMake(0, 0, _labelLabel.frame.size.width, _labelLabel.frame.size.height);
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, _valueLabel.frame.size.width + _labelLabel.frame.size.width, _valueLabel.frame.size.height + _labelLabel.frame.size.height);
    }
}
*/

@end
