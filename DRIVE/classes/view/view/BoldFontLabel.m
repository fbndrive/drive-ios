#import "AppDataManager.h"
#import "BoldFontLabel.h"
#import "Constants.h"

@implementation BoldFontLabel

- (void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.font = [UIFont fontWithName: Font_boldFace size: 16];
    self.textColor = colour;
    self.backgroundColor = [UIColor clearColor];
    
    self.numberOfLines = 1;
    self.lineBreakMode = NSLineBreakByWordWrapping;
}

@end
