#import "BasicLabelValueView.h"
#import "AppDataManager.h"

@implementation BasicLabelValueView

@synthesize numericValue = _numericValue;
@synthesize stringValue = _stringValue;
@synthesize mutableAttributedStringValue = _mutableAttributedStringValue;
@synthesize stringValueLength = _stringValueLength;
@synthesize label = _label;
@synthesize mutableAttributedStringLabel = _mutableAttributedStringLabel;
@synthesize labelLength = _labelLength;

@synthesize labelLabel = _labelLabel;
@synthesize valueLabel = _valueLabel;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self drawUI];
    }
    
    return self;
}

- (void) displayValue: (NSString *) value {
    if(value) {
        self.valueLabel.text = value;
    } else {
        self.valueLabel.text = self.stringValue;
    }
}

- (void) displayLabel: (NSString *) label {
    if(label) {
        self.labelLabel.text = label;
    } else {
        self.labelLabel.text = self.label;
    }
}

- (void) displayWithLabel: (NSString *)displayLabel andValue: (NSString *) displayValue {
    [self displayLabel:displayLabel];
    [self displayValue:displayValue];
}

- (void) applyFormatting {
    
}

- (void) applyFormattingWithLabel: (NSString *)displayLabel andValue: (NSString *) displayValue {
    [self displayWithLabel:displayLabel andValue:displayValue];
    [self applyFormatting];
}

#pragma mark - User Interface

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    _valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    _labelLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    
    _valueLabel.layer.shadowColor = [[adm driveDescription].color CGColor];
    _valueLabel.layer.shadowRadius = 18.0f;
    _valueLabel.layer.shadowOpacity = .9f;
    _valueLabel.layer.shadowOffset = CGSizeZero;
    _valueLabel.layer.masksToBounds = NO;
    _valueLabel.numberOfLines = 1;
    _valueLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    _labelLabel.layer.shadowColor = [[adm driveDescription].color CGColor];
    _labelLabel.layer.shadowRadius = 18.0f;
    _labelLabel.layer.shadowOpacity = .9f;
    _labelLabel.layer.shadowOffset = CGSizeZero;
    _labelLabel.layer.masksToBounds = NO;
    _labelLabel.numberOfLines = 1;
    _labelLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [self addSubview:_valueLabel];
    [self addSubview:_labelLabel];
}

- (void) animateIn {
    
}

- (void) animateOut {
    
}

#pragma mark - Getters / Setters

- (void) setNumericValue:(float)numericValue {
    _numericValue = numericValue;
    
    NSString *sv = [self intStringOfValue];
    [self setStringValue: sv];
}

- (float) numericValue {
    return _numericValue;
}

- (void) setStringValue:(NSString *)stringValue {
    _stringValue = stringValue;
    
    _mutableAttributedStringValue = [[NSMutableAttributedString alloc] initWithString:stringValue];
    _stringValueLength = [stringValue length];
}

- (NSString *) stringValue {
    return _stringValue;
}

- (void) setLabel:(NSString *)label {
    _label = [label uppercaseString];
    
    _mutableAttributedStringLabel = [[NSMutableAttributedString alloc] initWithString:label];
    _labelLength = [label length];
}

- (NSString *) label {
    return _label;
}

- (void) setNumValue:(float) numValue withLabel: (NSString *) label {
    [self setNumericValue:numValue];
    [self setLabel:label];
}

- (NSString *) intStringOfValue {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *commaString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger: (int) _numericValue]];
    
    return commaString;
}

- (NSString *) floatStringOfValue {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setGroupingSeparator:@" "];
    [numberFormatter setMinimumFractionDigits:1];
    [numberFormatter setMaximumFractionDigits:1];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *commaString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat: _numericValue]];
    
    //return [commaString stringByReplacingOccurrencesOfString:@"," withString:@" "];
    return commaString;
}

@end
