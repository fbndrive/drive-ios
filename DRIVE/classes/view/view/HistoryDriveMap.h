#import "DriveCustomMapView.h"

@interface HistoryDriveMap : DriveCustomMapView

+ (HistoryDriveMap *) getMapForFrame:(CGRect)frame andPath: (NSArray *) path;

@end
