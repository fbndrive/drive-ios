#import "AppDataManager.h"
#import "Constants.h"
#import "GlowButton.h"

@implementation GlowButton

-(void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.titleLabel.font = [UIFont fontWithName: Font_boldFace size: 16];
    [self setTitleColor:colour forState:UIControlStateNormal];
    self.backgroundColor = [UIColor clearColor];
    
    self.titleLabel.layer.shadowColor = [colour CGColor];
    self.layer.shadowRadius = 14.0f;
    self.layer.shadowOpacity = .6f;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.masksToBounds = NO;
    
    //self.titleLabel.clipsToBounds = YES;
}

@end
