#import <UIKit/UIKit.h>

@interface CompassView : UIView

extern float const FULL_360_DURATION;

@property (nonatomic) CGSize size;
@property (nonatomic) float directionValue;

- (void) animateToDirectionValue: (float) directionValue;

@end
