#import "BasicChartView.h"

@interface TriangleShapedHBarChart : BasicChartView

@property (nonatomic) CGSize size;
@property (nonatomic) float minimum;
@property (nonatomic) float maximum;
@property (nonatomic) float value;

- (void) refresh;
- (void) arrange;
- (void) setMinimum:(float) minimum andMaximum:(float) maximum;
- (void) animateToValue: (float) value;

@end