#import "HistoryTableListCellMapBackground.h"

@implementation HistoryTableListCellMapBackground

- (id)initWithImage:(UIImage *) image {
    self = [super initWithFrame: CGRectMake(0, 0, image.size.width, image.size.height)];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
        imageView.alpha = .3;
        
        [self addSubview: imageView];
    }
    
    return self;
}

- (void) highlightMe {
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha: .2];
}

@end
