#import <UIKit/UIKit.h>

@interface HistoryTableListCellMapBackground : UIView

- (id)initWithImage:(UIImage *) image;
- (void) highlightMe;

@end
