#import "DriveData.h"
#import "RegularFontLabel.h"

@interface HistoryTableListCellDuration : RegularFontLabel

-(void) fillWithData: (DriveData *) data;

@end