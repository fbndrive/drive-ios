#import "Constants.h"
#import "HistoryDriveMap.h"
#import "RMAnnotation.h"
#import "RMMapboxSource.h"

@interface HistoryDriveMap ()


@end

@implementation HistoryDriveMap

+ (HistoryDriveMap *) getMapForFrame:(CGRect)frame andPath: (NSArray *) path {
    RMMapboxSource *tileSrc = [[RMMapboxSource alloc] initWithMapID: API_MapBox_projectID];
    
    HistoryDriveMap *hdm = [[HistoryDriveMap alloc] initWithFrame:frame andTilesource: tileSrc];
    
    RMAnnotation *annotation = [[RMAnnotation alloc] initWithMapView: hdm
                                                         coordinate: hdm.centerCoordinate
                                                           andTitle: nil];
    
    [hdm addAnnotation: annotation];

    
    return hdm;
}

@end
