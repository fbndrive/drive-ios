#import <UIKit/UIKit.h>

@interface DottedArc : UIView

@property (nonatomic) float finalAngle;
@property (strong, nonatomic) UIColor *dotColor;

@end
