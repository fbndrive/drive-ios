#import "AppDataManager.h"
#import "CenteredLabelsLabelValueView.h"
#import "CenteredSmallLabelBigValueLabelValue.h"
#import "CenteredSmallLabelHugeValueLabelValue.h"
#import "Constants.h"
#import "DriveSummaryCountView.h"
#import "ExperienceComponentData.h"

@interface DriveSummaryCountView ()

@property (strong, nonatomic) CenteredLabelsLabelValueView *gainedExpCounter;
@property (strong, nonatomic) CenteredSmallLabelHugeValueLabelValue *particularExperienceCounter;
@property (strong, nonatomic) CenteredSmallLabelBigValueLabelValue *totalExperienceCounter;

@property (nonatomic) float animationDuration;

// animation helpers
@property (nonatomic, strong) NSDate *countAnimStartTime;
@property (nonatomic) float startCount;
@property (nonatomic) float changeCountBy;

@property (nonatomic, strong) NSMutableArray *experienceArray;

@end


@implementation DriveSummaryCountView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _animationDuration = 1.0f;
        
        [self drawUI];
    }
    return self;
}

- (void) dispatchAnimationComplete {
    NSNotification *animCompleteNotification = [[NSNotification alloc] initWithName:Animation_complete object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:animCompleteNotification];
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CenteredLabelsLabelValueView *gainedExpCounter = [[CenteredLabelsLabelValueView alloc] init];
    gainedExpCounter.labelWidth = screenRect.size.width;
    [gainedExpCounter setNumValue:0.0f withLabel:@"exp"];
    [gainedExpCounter applyFormattingWithLabel:nil andValue:nil];
    [gainedExpCounter sizeMe];
    
    CenteredSmallLabelHugeValueLabelValue *particularExperienceCounter = [[CenteredSmallLabelHugeValueLabelValue alloc] init];
    particularExperienceCounter.labelWidth = screenRect.size.width;
    [particularExperienceCounter setNumValue:0.0f withLabel:@"test"];
    [particularExperienceCounter applyFormattingWithLabel:nil andValue:nil];
    [particularExperienceCounter sizeMe];
    particularExperienceCounter.alpha = 0;
    
    CenteredSmallLabelBigValueLabelValue *totalExperienceCounter = [[CenteredSmallLabelBigValueLabelValue alloc] init];
    totalExperienceCounter.labelWidth = screenRect.size.width;
    [totalExperienceCounter setNumValue:adm.totalExperience withLabel:@"total experience"];
    [totalExperienceCounter applyFormattingWithLabel:nil andValue:nil];
    [totalExperienceCounter sizeMe];
    totalExperienceCounter.hidden = YES;
    totalExperienceCounter.alpha = 0;
    
    particularExperienceCounter.frame = CGRectMake(0, 128, particularExperienceCounter.frame.size.height, particularExperienceCounter.frame.size.height);
    totalExperienceCounter.frame = CGRectMake(0, 0, totalExperienceCounter.frame.size.height, totalExperienceCounter.frame.size.height);
    
    [self addSubview:gainedExpCounter];
    [self addSubview:particularExperienceCounter];
    [self addSubview:totalExperienceCounter];
    
    _gainedExpCounter = gainedExpCounter;
    _particularExperienceCounter = particularExperienceCounter;
    _totalExperienceCounter = totalExperienceCounter;
}

// phase 1 of the whole animation, individual exp points in

- (void) proceedWithExpereinceArray: (NSMutableArray *) experienceArray {
    NSLog(@"test count %lu", (unsigned long)[experienceArray count]);
    _experienceArray = experienceArray;
    
    [self fadeInOutParticularExperience];
}

- (void) fadeInOutParticularExperience {
    float talpha;
    ExperienceComponentData *ecd;
    CGRect frame;
    
    if(_particularExperienceCounter.alpha == 0) {
        if([_experienceArray count] <= 0) {
            //finish
            _particularExperienceCounter.hidden = YES;
            
            [self proceedWithTotalExperience];
            
            return;
        }
        
        talpha = 1;
        frame = CGRectMake(0, 128, _particularExperienceCounter.frame.size.height, _particularExperienceCounter.frame.size.height);
        
        ecd = [_experienceArray lastObject];
        [_experienceArray removeLastObject];

        [_particularExperienceCounter setNumValue:ecd.points withLabel:ecd.description];
        [_particularExperienceCounter displayWithLabel:nil andValue:nil];
        
        _particularExperienceCounter.hidden = NO;
    }
    
    if(_particularExperienceCounter.alpha == 1) {
        talpha = 0;
        
        frame = CGRectMake(0, 138, _particularExperienceCounter.frame.size.height, _particularExperienceCounter.frame.size.height);
    }
    
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _particularExperienceCounter.alpha = talpha;
                         _particularExperienceCounter.frame = frame;
                     }
                     completion:^(BOOL finished){
                         if(talpha == 1) {
                             [self triggerExperienceGainedData:ecd];
                         }
                         if(talpha == 0) {
                             [self fadeInOutParticularExperience];
                         }
                     }];
}

- (void) triggerExperienceGainedData: (ExperienceComponentData *) ecd {
    [_particularExperienceCounter setNumValue:ecd.points withLabel:ecd.description];
    
    [self countFromStartValue:_gainedExpCounter.numericValue andChangeBy:ecd.points];
}

- (void) countFromStartValue: (float) startValue andChangeBy: (float) changeBy {
    _countAnimStartTime = [NSDate date];
    
    _startCount = startValue;
    _changeCountBy = changeBy;
    
    [self countTick:nil];
}

- (void) updateGainedAndParticularWithNewValue: (float) newValue {
    int nv = (int) newValue;
    
    [_gainedExpCounter setNumericValue:nv];
    [_particularExperienceCounter setNumericValue:_startCount + _changeCountBy - nv];
    
    [_gainedExpCounter displayValue:nil];
    [_particularExperienceCounter displayValue:nil];
}

-(void) countTick: (NSTimer *) timer {
    
    float timeDifference = [_countAnimStartTime timeIntervalSinceNow] * -1;
    
    if(timeDifference >= _animationDuration) {
        [self updateGainedAndParticularWithNewValue:_startCount + _changeCountBy];
        
        [self fadeInOutParticularExperience];
        
        return;
    }
    
    float nv = [self getNextStepForCurrentTime:timeDifference fromStart:_startCount withChange:_changeCountBy andDuration:_animationDuration];
    
    [self updateGainedAndParticularWithNewValue: nv];
    
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(countTick:)
                                   userInfo:nil
                                    repeats:NO];
    
    //NSLog(@"nv: %f", nv);
}

// phase 2 of the whole animation, topping up total experience

- (void) proceedWithTotalExperience {
    [self moveGainedExpDown];
}

- (void) moveGainedExpDown {
    CGRect frame = CGRectMake(_gainedExpCounter.frame.origin.x, 128, _totalExperienceCounter.frame.size.width, _totalExperienceCounter.frame.size.height);
    
    [UIView animateWithDuration:.5
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _gainedExpCounter.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [self fadeInTotalExperience];
                     }];
}


- (void) fadeInTotalExperience {
    _totalExperienceCounter.hidden = NO;
    
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _totalExperienceCounter.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         [self triggerTotalExp];
                     }];
}

- (void) fadeOutExpGained {
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         _gainedExpCounter.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         _gainedExpCounter.hidden = YES;
                         [self moveUpTotalExperience];
                     }];
}

- (void) moveUpTotalExperience {
    [self dispatchAnimationComplete];
    
    return;
    
    CGRect frame = CGRectMake(_totalExperienceCounter.frame.origin.x, _gainedExpCounter.frame.origin.y, _totalExperienceCounter.frame.size.width, _totalExperienceCounter.frame.size.height);
    
    [UIView animateWithDuration:.5
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _totalExperienceCounter.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [self dispatchAnimationComplete];
                     }];
}

- (void) triggerTotalExp {
    [self countTotalExpFromStartValue:_totalExperienceCounter.numericValue andChangeBy:_gainedExpCounter.numericValue];
}

- (void) countTotalExpFromStartValue: (float) startValue andChangeBy: (float) changeBy {
    _countAnimStartTime = [NSDate date];
    
    _startCount = startValue;
    _changeCountBy = changeBy;
    
    [self countTotalExpTick:nil];
}

- (void) updateTotalAndGainedWithNewValue: (float) newValue {
    int nv = (int) newValue;
    
    [_totalExperienceCounter setNumericValue:nv];
    [_gainedExpCounter setNumericValue:_startCount + _changeCountBy - nv];
    
    [_gainedExpCounter displayValue:nil];
    [_totalExperienceCounter displayValue:nil];
}

-(void) countTotalExpTick: (NSTimer *) timer {
    
    float timeDifference = [_countAnimStartTime timeIntervalSinceNow] * -1;
    
    if(timeDifference >= _animationDuration) {
        [self updateTotalAndGainedWithNewValue:_startCount + _changeCountBy];
        
        [self fadeOutExpGained];
        
        return;
    }
    
    float nv = [self getNextStepForCurrentTime:timeDifference fromStart:_startCount withChange:_changeCountBy andDuration:_animationDuration];
    
    [self updateTotalAndGainedWithNewValue: nv];
    
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(countTotalExpTick:)
                                   userInfo:nil
                                    repeats:NO];
    
    //NSLog(@"nv: %f", nv);
}


// equation

-(float) getNextStepForCurrentTime: (float) time fromStart: (float) start withChange: (float) change andDuration: (float) duration  {
    return change * time / duration + start;
    
    //time /= duration;
    //return -change * time*(time-2) + start;
    
    //time /= duration;
	//return change*time*time*time*time*time + start;
}


@end
