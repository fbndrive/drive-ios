#import "CompassHandView.h"

@implementation CompassHandView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) setSize:(CGSize)size andColor:(UIColor *) color {
    _color = color;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void) setSize:(CGSize)size {
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void)drawRect:(CGRect)rect {
    CGSize size = self.frame.size;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 1.0);
    
    
    CGContextSetFillColorWithColor(context, _color.CGColor);
    CGContextSetStrokeColorWithColor(context, _color.CGColor);
    
    CGContextMoveToPoint(context, 0, size.height);
    
    CGContextAddLineToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, 3, 15);
    CGContextAddLineToPoint(context, 0, 15);
    
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, 1, size.height);
    
    CGContextAddLineToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, 3, 15);
    CGContextAddLineToPoint(context, 0, 15);
    
    
    CGContextFillPath(context);
    
}

@end
