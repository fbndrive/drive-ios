#import "Constants.h"
#import "DriveDetailsDateLabel.h"

@implementation DriveDetailsDateLabel

- (void) fillWithDate: (NSDate *) date {
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:Database_dateTimezone];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:timeZone];
    [format setDateFormat:@"dd MMM YYYY"];
    
    [self setTitle: [format stringFromDate:date]];
}

@end
