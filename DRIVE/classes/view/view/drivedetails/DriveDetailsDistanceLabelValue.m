#import "AppDataManager.h"
#import "Constants.h"
#import "DriveDetailsDistanceLabelValue.h"

@implementation DriveDetailsDistanceLabelValue

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 25];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
}

- (void) applyFormatLabel {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_regularFace size: 14];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.labelLabel.font = fontFace;
    self.labelLabel.textColor = fontColor;
}


- (void) setNumericValue:(float)numericValue {
    [super setNumericValue:floorf(numericValue * 10) / 10];
    
    NSString *sv = [self floatStringOfValue];
    [self setStringValue: sv];
}

- (void) drawUI {
    [super drawUI];
    
    self.valueLabel.layer.shadowRadius = 0.0f;
    self.labelLabel.layer.shadowRadius = 0.0f;
}


@end
