#import <UIKit/UIKit.h>

@interface DayNightIndicatorView : UIView

- (void) indicateDayTime: (BOOL) isDay;

@end
