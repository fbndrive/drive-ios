#import "AppDataManager.h"
#import "Constants.h"
#import "DriveDetailsExperienceLabelValue.h"

@implementation DriveDetailsExperienceLabelValue

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 25];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
}

- (void) applyFormatLabel {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_regularFace size: 14];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.labelLabel.font = fontFace;
    self.labelLabel.textColor = fontColor;
}

- (void) drawUI {
    [super drawUI];
    
    self.valueLabel.layer.shadowRadius = 0.0f;
    self.labelLabel.layer.shadowRadius = 0.0f;
}


@end
