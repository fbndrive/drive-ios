#import "AppDataManager.h"
#import "Constants.h"
#import "DriveDetailsDurationLabelValue.h"
#import "UIImage+Tint.h"

@interface DriveDetailsDurationLabelValue ()

@property (nonatomic, retain) UIImageView *icon;

@end


@implementation DriveDetailsDurationLabelValue

- (void) setNumericValue:(float)numericValue {
    [super setNumericValue:numericValue];
    
    NSString *sv = [self timeStringOfValue];
    [self setStringValue: sv];
}

- (void) applyFormatValue {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIFont *fontFace = [UIFont fontWithName: Font_boldFace size: 25];
    UIColor *fontColor = [adm driveDescription].color;
    
    self.valueLabel.font = fontFace;
    self.valueLabel.textColor = fontColor;
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    [super drawUI];
    
    self.valueLabel.layer.shadowRadius = 0.0f;
    self.labelLabel.layer.shadowRadius = 0.0f;
    
    UIImage *iconImg = [[UIImage imageNamed:@"timer-icon"] tintedImageWithColor:[adm driveDescription].color];
    UIImageView *icon = [[UIImageView alloc] initWithImage:iconImg];
    
    [self addSubview:icon];
    
    _icon = icon;
}

- (void) sizeMe {
    [self.valueLabel sizeToFit];
    [self.labelLabel sizeToFit];
    
    self.valueLabel.frame = CGRectMake(self.icon.frame.size.width + 2, 0, self.valueLabel.frame.size.width + 5, self.valueLabel.frame.size.height);
    self.icon.frame = CGRectMake(0, self.valueLabel.frame.size.height - 10, self.icon.frame.size.width, self.icon.frame.size.height);
    self.labelLabel.frame = CGRectMake(0, 0, 1, 1);
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.valueLabel.frame.size.width + self.icon.frame.size.width, self.valueLabel.frame.size.height + self.icon.frame.size.height);
    
    //NSLog(@"frame: %@", NSStringFromCGRect(self.labelLabel.frame));
}

- (NSString *) timeStringOfValue {
    NSInteger ti = (NSInteger) self.numericValue;
    
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld.%02ld", (long)hours, (long)minutes, (long)seconds];
}

@end
