#import "DriveDetailsMainAnimationStep.h"
#import "NumberTools.h"

@implementation DriveDetailsMainAnimationStep

- (NSArray*) animationStepArray {
    
    return [NSArray arrayWithObject:self];
}

- (CPAnimationStepBlock) animationStep:(BOOL)animated {
    NSLog(@"time: %d", [NumberTools randomNumberFromRange:NSMakeRange(0, 500)]);
    
    return self.step;
}

@end
