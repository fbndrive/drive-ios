#import "RegularFontLabel.h"

@interface DriveDetailsDateLabel : RegularFontLabel

- (void) fillWithDate: (NSDate *) date;

@end
