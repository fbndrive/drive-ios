#import "AppDataManager.h"
#import "DayNightIndicatorView.h"
#import "UIImage+Tint.h"

@interface DayNightIndicatorView ()

@property (nonatomic, retain) UIImageView *sun;
@property (nonatomic, retain) UIImageView *moon;

@end

@implementation DayNightIndicatorView

- (void) indicateDayTime: (BOOL) isDay {
    _sun.alpha = 1.0f;
    _moon.alpha = 1.0f;
    
    if(isDay == YES) {
        _moon.alpha = .2f;
    } else {
        _sun.alpha = .2f;
    }
}

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self drawUI];
        [self indicateDayTime:YES];
    }
    
    return self;
}

- (void) drawUI {
    AppDataManager *adm = [AppDataManager appDataManager];
    
    UIImage *sunImg = [[UIImage imageNamed:@"sun-icon"] tintedImageWithColor: [adm driveDescription].color];
    UIImage *moonImg = [[UIImage imageNamed:@"moon-icon"] tintedImageWithColor: [adm driveDescription].color];
    
    UIImageView *sun = [[UIImageView alloc] initWithImage:sunImg];
    UIImageView *moon = [[UIImageView alloc] initWithImage:moonImg];
    
    moon.frame = CGRectMake(sun.frame.origin.x + sun.frame.size.width + 10, 0, moon.frame.size.width, moon.frame.size.height);
    
    [self addSubview:sun];
    [self addSubview:moon];
    _sun = sun;
    _moon = moon;
}

@end
