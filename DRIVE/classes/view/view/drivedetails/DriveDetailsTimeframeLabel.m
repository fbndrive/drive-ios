#import "Constants.h"
#import "DriveDetailsTimeframeLabel.h"

@implementation DriveDetailsTimeframeLabel

- (void) fillFromDate: (NSDate *) from toDate: (NSDate *) to {
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:Database_dateTimezone];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:timeZone];
    format.dateStyle = NSDateFormatterNoStyle;
    format.timeStyle = NSDateFormatterShortStyle;
    
    
    NSString *timeframeString = [NSString stringWithFormat:@"%@ - %@", [format stringFromDate: from], [format stringFromDate: to]];
    
    [self setTitle: timeframeString];
}

@end
