#import "RegularFontLabel.h"

@interface DriveDetailsTimeframeLabel : RegularFontLabel

- (void) fillFromDate: (NSDate *) from toDate: (NSDate *) to;

@end
