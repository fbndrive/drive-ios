#import <UIKit/UIKit.h>

@interface TriangleShapedHBarSlice : UIView

@property (nonatomic) CGSize size;
@property (nonatomic) float delta;
@property (nonatomic, strong) UIColor *color;

- (void) setSize:(CGSize)size andColor:(UIColor *) color;

@end
