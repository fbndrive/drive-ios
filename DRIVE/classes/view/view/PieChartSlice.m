#import "PieChartSlice.h"

@implementation PieChartSlice

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _percent = 0.0f;
    }
    return self;
}

- (void) setSize:(CGSize)size andColor:(UIColor *) color {
    _color = color;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void) setPercent:(float)percent {
    _percent = percent;
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void) setSize:(CGSize)size {
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void)drawRect:(CGRect)rect {
    if(_percent <=0) {
        return;
    }
    
    CGFloat radius = self.frame.size.width/2;
    CGPoint center = CGPointMake(radius, radius);
    float endAngle = -M_PI_2 + (4 * M_PI_2) * _percent;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,
                                   _color.CGColor);
    //CGContextFillEllipseInRect(context, rectangle);
    CGContextMoveToPoint(context, center.x, center.y);
    CGContextAddLineToPoint(context, center.x, 0);
    CGContextAddArc(context, center.x, center.y, radius, -M_PI_2, endAngle, 0);
    CGContextAddLineToPoint(context, center.x, center.y);
    CGContextFillPath(context);
}

@end
