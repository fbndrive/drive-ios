#import <UIKit/UIKit.h>

@interface BasicLabelValueView : UIView

@property (nonatomic) float numericValue;
@property (strong, nonatomic) NSString *stringValue;
@property (strong, nonatomic) NSMutableAttributedString *mutableAttributedStringValue;
@property (nonatomic) NSInteger stringValueLength;
@property (strong, nonatomic) NSString *label;
@property (strong, nonatomic) NSMutableAttributedString *mutableAttributedStringLabel;
@property (nonatomic) NSInteger labelLength;

@property (strong, nonatomic) UILabel *valueLabel;
@property (strong, nonatomic) UILabel *labelLabel;


- (void) drawUI;
- (void) animateIn;
- (void) animateOut;

- (void) displayValue: (NSString *) value;
- (void) displayLabel: (NSString *) label;
- (void) displayWithLabel: (NSString *)displayLabel andValue: (NSString *) displayValue;

- (void) applyFormatting;
- (void) applyFormattingWithLabel: (NSString *)displayLabel andValue: (NSString *) displayValue;

- (void) setNumericValue:(float)numericValue;
- (float) numericValue;
- (void) setStringValue:(NSString *)stringValue;
- (NSString *) stringValue;
- (void) setLabel:(NSString *)label;
- (NSString *) label;

- (void) setNumValue:(float) numValue withLabel: (NSString *) label;

- (NSString *) intStringOfValue;
- (NSString *) floatStringOfValue;

@end
