#import <UIKit/UIKit.h>

@interface ArrowArcClockView : UIView

- (void) animateToValue: (float) value;

@end
