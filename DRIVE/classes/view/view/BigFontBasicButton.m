#import "AppDataManager.h"
#import "BigFontBasicButton.h"
#import "Constants.h"

@implementation BigFontBasicButton

-(void) applyFormatting {
    AppDataManager *adm = [AppDataManager appDataManager];
    UIColor *colour = [adm driveDescription].color;
    
    self.titleLabel.font = [UIFont fontWithName: Font_boldFace size: 24];
    [self setTitleColor:colour forState:UIControlStateNormal];
    self.backgroundColor = [UIColor clearColor];
    
    self.titleLabel.layer.shadowColor = [colour CGColor];
    self.titleLabel.layer.shadowRadius = 18.0f;
    self.titleLabel.layer.shadowOpacity = .9f;
    self.titleLabel.layer.shadowOffset = CGSizeZero;
    self.titleLabel.layer.masksToBounds = NO;
}

@end
