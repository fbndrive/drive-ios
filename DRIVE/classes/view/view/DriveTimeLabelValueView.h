#import "RegLabelBoldValueLabelValueView.h"

@interface DriveTimeLabelValueView : RegLabelBoldValueLabelValueView

@property (strong, nonatomic) NSDate *startDate;

- (NSString *) timeStringOfValue;

@end
