#import <UIKit/UIKit.h>

@interface RegularFontLabel : UILabel

- (void) refresh;
- (void)setTitle:(NSString *)text;

@end
