#import "MainMenuItemList.h"
#import "MainMenuOpenCloseButton.h"
#import <UIKit/UIKit.h>

@interface MainMenuView : UIView

@property (nonatomic) BOOL isOpened;

@property (strong, nonatomic) MainMenuOpenCloseButton *openCloseButton;
@property (strong, nonatomic) MainMenuItemList *itemList;

- (void) refresh;
- (void)toggleState;

@end
