#import "AppDataManager.h"
#import "CompassBackgroundView.h"

@implementation CompassBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) setSize:(CGSize)size andColor:(UIColor *) color {
    _color = color;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void) setColor:(UIColor *)color {
    _color = color;
}

- (void) setSize:(CGSize)size {
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void)drawRect:(CGRect)rect {
    CGSize size = self.frame.size;
    float x = 0;
    float y = 0;
    float x2 = 0;
    float y2 = 0;
    float r = size.width/2;
    float t = -M_PI_2;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 1.0);
    
    
    //CGContextSetFillColorWithColor(context,
    //                               _color.CGColor);
    CGContextSetStrokeColorWithColor(context, _color.CGColor);
    
    while (t < M_PI_2) {        
        x = r * cos(t) + r;
        y = r * sin(t) + r;
        x2 = r * cos(t + M_PI) + r;
        y2 = r * sin(t + M_PI) + r;
        
        CGContextMoveToPoint(context, x, y);
        CGContextAddLineToPoint(context, x2, y2);
        
        t += M_PI / 8;
    }
    
    
    
    
    //CGContextTranslateCTM(context, size.width/2, size.height);
    //CGContextRotateCTM(context, M_PI_2/2);
    //CGContextMoveToPoint(context, size.width/2, size.height);
    //CGContextAddLineToPoint(context, 0, 0);
    //CGContextAddLineToPoint(context, size.width, size.height);
    //CGContextAddLineToPoint(context, 0, size.height);
    //CGContextFillPath(context);
    CGContextStrokePath(context);
}

@end
