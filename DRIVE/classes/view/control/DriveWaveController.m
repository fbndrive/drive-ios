#import "ASIFormDataRequest.h"
#import "Constants.h"
#import "DriveWaveController.h"

@interface DriveWaveController ()

@property (strong, nonatomic) ASIFormDataRequest *request;

@end

@implementation DriveWaveController

@synthesize recorder = _recorder;
@synthesize player = _player;
@synthesize recording = _recording;

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    _recording = NO;
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    //_player = [[AVAudioPlayer alloc] initWithContentsOfURL:_recorder.url error:nil];
    //[_player setDelegate: self];
    //[_player play];
    
    [self postWaveWithWavePath: _recorder.url];
}

- (void) startWaveRecording {
    _recording = YES;
    
    [self prepareRecording];
    [self startRecording];
}

- (void) stopWaveRecording {
    [_recorder stop];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:NO error:nil];
}

- (void) prepareRecording {
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    _recorder.delegate = self;
    _recorder.meteringEnabled = YES;
    [_recorder prepareToRecord];
}

- (void) startRecording {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    
    [_recorder record];
}

- (void) postWaveWithWavePath: (NSURL *) path {
    if(path == nil) {
        return;
    }
    
    NSURL *url = [NSURL URLWithString:@"http://serwer1425546.home.pl/wave/upload.php"];
    
    _request = [ASIFormDataRequest requestWithURL:url];
    [_request setDelegate:self];
    
    [_request setPostValue:@"123" forKey:@"userid"];
    [_request setPostValue:@"0" forKey:@"date"];
    [_request setPostValue:@"12" forKey:@"latitude"];
    [_request setPostValue:@"14" forKey:@"longitude"];
    [_request setPostValue:@"1" forKey:@"type"];
    [_request setFile:path.relativePath forKey:@"wavefile"];
    
    /*[_request setFile:path.absoluteString withFileName:path.lastPathComponent andContentType:@"audio/mpeg"
              forKey:@"wavefile"];
    */
    
    [_request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    
    NSLog(@"raw: %@", responseString);
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    
    NSLog(@"error: %@", error.debugDescription);
}

- (void)requestStarted:(ASIHTTPRequest *)request {
    
}

@end
