#import "AccelerationChart.h"
#import "AppDataManager.h"
#import "BasicButton.h"
#import "CompassView.h"
#import "Constants.h"
#import "CPAnimationSequence.h"
#import "DayNightIndicatorView.h"
#import "DriveCustomMapView.h"
#import "DriveData.h"
#import "DriveDetailsMainAnimationStep.h"
#import "DriveDetailsDateLabel.h"
#import "DriveDetailsDistanceLabelValue.h"
#import "DriveDetailsDurationLabelValue.h"
#import "DriveDetailsExperienceLabelValue.h"
#import "DriveDetailsHeaderLabel.h"
#import "DriveDetailsTimeframeLabel.h"
#import "DriveStyledPath.h"
#import "DBTransactionManager.h"
#import "ElevationChart.h"
#import "ElevationChartVertex.h"
#import "FileSystemManager.h"
#import "HistoryDriveViewController.h"
#import "HistoryDriveMap.h"
#import "HistoryTableListCell.h"
#import "MapBox.h"
#import "NumberTools.h"
#import "OverloadChart.h"
#import "RegularFontLabel.h"
#import "SpeedChart.h"
#import "TopEdgeSliderView.h"

#define kDriveMapViewHeight   250.0f

@interface HistoryDriveViewController ()

@property (nonatomic, retain) BasicButton *backButton;
@property (nonatomic, retain) BasicButton *closeMapButton;
@property (nonatomic, retain) TopEdgeSliderView *topEdgeSlider;
@property (nonatomic, retain) UIView *headerContainer;
@property (nonatomic, retain) DriveDetailsDurationLabelValue *headerDriveDurationLabelValue;
@property (nonatomic, retain) DriveDetailsDistanceLabelValue *headerDriveDistanceLabelValue;
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIView *scrollViewContainer;

@property (nonatomic, retain) RMMapView *interactiveDriveMap;
@property (nonatomic, retain) DriveStyledPath *interactiveDriveMapPath;

@property (nonatomic, retain) RMStaticMapView *staticDriveMap;
@property (nonatomic, retain) DriveStyledPath *staticDriveMapPath;

@property (nonatomic, retain) UIButton *driveMapButton;
@property (nonatomic, retain) DriveDetailsDateLabel *driveDateLabel;
@property (nonatomic, retain) DriveDetailsTimeframeLabel *driveTimeframeLabel;
@property (nonatomic, retain) DriveDetailsDurationLabelValue *driveDurationLabelValue;
@property (nonatomic, retain) DriveDetailsDistanceLabelValue *driveDistanceLabelValue;
@property (nonatomic, retain) DayNightIndicatorView *dayNightIndicator;
@property (nonatomic, retain) DriveDetailsExperienceLabelValue *driveExperienceLabelValue;
@property (nonatomic, retain) DriveDetailsHeaderLabel *speedHeaderLabel;
@property (nonatomic, retain) SpeedChart *speedChart;
@property (nonatomic, retain) DriveDetailsHeaderLabel *accHeaderLabel;
@property (nonatomic, retain) AccelerationChart *acc0_100Chart;
@property (nonatomic, retain) AccelerationChart *acc80_120Chart;
@property (nonatomic, retain) AccelerationChart *acc100_0Chart;
@property (nonatomic, retain) DriveDetailsHeaderLabel *elevationHeaderLabel;
@property (nonatomic, retain) ElevationChart *elevationChart;
@property (nonatomic, retain) DriveDetailsHeaderLabel *overloadsHeaderLabel;
@property (nonatomic, retain) OverloadChart *overloadChart;
@property (nonatomic, retain) DriveDetailsHeaderLabel *directionHeaderLabel;
@property (nonatomic, retain) CompassView *directionChart;

@property (nonatomic, retain) DriveTrackpointCollection *staticPathData;
@property (nonatomic, retain) DriveTrackpointCollection *interactivePathData;

@property (nonatomic, retain) DriveData *driveData;
@property (nonatomic, retain) DriveData *mapSnapshotDriveData;
@property (nonatomic) BOOL drivePathDrew;

@property (strong, nonatomic) CAGradientLayer *scrollMaskLayer;

@property (nonatomic) int historyDataIndex;
@property (nonatomic) int historyDataCount;

@property (nonatomic) float initialSVY;
@property (nonatomic) float initialSVHeight;
@property (nonatomic) float initialYOfHeaderSVElements;
@property (nonatomic) BOOL yFixDemandOfHeaderSVElements;

@property (nonatomic) float animationDuration;

// animation helpers
@property (nonatomic, strong) NSDate *dataIndexChangedAnimationStartTime;
@property (nonatomic) float dataIndexChangedAnimationStartValue;
@property (nonatomic) float dataIndexChangedAnimationChange;
@property (nonatomic) BOOL dataIndexChangedAnimationInProgress;
@property (nonatomic) BOOL dataIndexChangedAnimation50PercIn;

@property (nonatomic) float speedChartAvgSpeedAnimationStartValue;
@property (nonatomic) float speedChartTopSpeedAnimationStartValue;

@property (nonatomic) float accChart0_100AnimationStartValue;
@property (nonatomic) float accChart80_120AnimationStartValue;
@property (nonatomic) float accChart100_0AnimationStartValue;

@property (nonatomic) float elevationChartAnimationValid;
@property (nonatomic) float elevationChartAnimationStartValue;

@property (nonatomic) float overloadChartAnimationValid;
@property (nonatomic) float overloadChartAnimationStartValue;

// TEMP

@property (nonatomic, strong) UIImageView *testImgView;

@end

@implementation HistoryDriveViewController

@synthesize historyData = _historyData;

- (void)viewDidLoad {

    self.appDataManager = [AppDataManager appDataManager];
    _drivePathDrew = NO;
    _initialYOfHeaderSVElements = 0;
    _yFixDemandOfHeaderSVElements = NO;
    _animationDuration = .5f;
    _dataIndexChangedAnimationInProgress = NO;
    _dataIndexChangedAnimation50PercIn = NO;
    
    _testImgView = nil;
    
    _mapSnapshotDriveData = nil;
    
    _staticDriveMap = nil;
    _interactivePathData = nil;
    
    [super viewDidLoad];
    
    [self addGestureRecognizers];
    
    if(_historyDataCount > 1) {
        float position = (float)_historyDataIndex/(_historyDataCount-1);
        [_topEdgeSlider setPosition:position];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self applyDataIndex:_historyDataIndex];
}

- (void) applyDataIndex:(int) index {
    if(index < 0) return;
    if(index > _historyDataCount - 1) return;
    if(_dataIndexChangedAnimationInProgress == YES) return;
    
    _historyDataIndex = index;
    
    if(_historyDataCount > 1) {
        float position = (float)_historyDataIndex/(_historyDataCount-1);
        [_topEdgeSlider setPosition:position];
    }
    
    _driveData = (DriveData *) [_historyData objectAtIndex:_historyDataIndex];
    
    [self triggerIndexChangedAnimation];
    
    [self removeMap];
    [self removeMapSnapshot];
    _staticPathData = nil;    _interactivePathData = nil;
    
    [self addMapSnapshot];
    
    
        
    //[_speedChart setAvgSpeed:[NumberTools randomNumberFromRange:NSMakeRange(0, 350)] andTopSpeed:[NumberTools randomNumberFromRange:NSMakeRange(0, 350)]];
}

- (void) staticMapLoaded {
    NSLog(@"static map loaded");
}

- (void)beforeMapZoom:(RMMapView *)map byUser:(BOOL)wasUserAction {
    _interactiveDriveMapPath.hidden = YES;
}

- (void)afterMapZoom:(RMMapView *)map byUser:(BOOL)wasUserAction {
    _interactiveDriveMapPath.hidden = NO;
    
    [self reloadInteractiveMapPathData];
    [_interactiveDriveMapPath redrawWithCollection: _interactivePathData];
}

- (void)beforeMapMove:(RMMapView *)map byUser:(BOOL)wasUserAction {
    _interactiveDriveMapPath.hidden = YES;
}

- (void)afterMapMove:(RMMapView *)map byUser:(BOOL)wasUserAction {
    _interactiveDriveMapPath.hidden = NO;
    
    [self reloadInteractiveMapPathData];
    [_interactiveDriveMapPath redrawWithCollection: _interactivePathData];
}

- (void)backButtonClick: (id) target {
    [self switchToHistoryViewController];
}

- (void)driveMapButtonClick: (id) target {
    [self showInteractiveMap];
}

- (void)closeMapButtonClick: (id) target {
    [self hideInteraciveMap];
}

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        //NSLog(@"tap recognized from: %@", [sender.vie);
    }
}

- (void)handleSwipeGesture:(UISwipeGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        if(sender.direction == UISwipeGestureRecognizerDirectionRight) {
            [self applyDataIndex:_historyDataIndex - 1];
        }
        
        if(sender.direction == UISwipeGestureRecognizerDirectionLeft) {
            [self applyDataIndex:_historyDataIndex + 1];
        }
    }   
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView.contentOffset.y > 294) {
        _headerContainer.hidden = NO;
        [self maskScrollViewWithHeight:_scrollView.frame.size.height - 50];
    } else {
        _headerContainer.hidden = YES;
        [self maskScrollViewWithHeight:_scrollView.frame.size.height];
    }
}

- (void)singleTapOnMap:(RMMapView *)map at:(CGPoint)point {
    NSLog(@"singletap");
    
    if(map == _staticDriveMap) {
        [self driveMapButtonClick:nil];
    }
}

- (void)doubleTapOnMap:(RMMapView *)map at:(CGPoint)point {
    NSLog(@"doubletap");
}

- (void) setHistoryData:(NSArray *)historyData {
    _historyData = historyData;
    _historyDataCount = (int)[_historyData count];
}

- (void) drawUI {
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    float xpos = Screen_globalMargin;
    float ypos = 0.0f; // y position in current view
    
    TopEdgeSliderView *topEdgeSlider = [[TopEdgeSliderView alloc] initWithFrame:CGRectMake(0, 0, 80, 3)];
    topEdgeSlider.color = [self.appDataManager driveDescription].color;
    
    ypos = 20;
    BasicButton *backButton = [[BasicButton alloc] initWithFrame:CGRectMake(5, 5, screenFrame.size.width, 40)];
    [backButton setTitle: NSLocalizedString(@"HistoryVC_backtolist", nil)];
    [backButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    BasicButton *closeMapButton = [[BasicButton alloc] initWithFrame:CGRectMake(5, 5, screenFrame.size.width, 40)];
    [closeMapButton setTitle: NSLocalizedString(@"HistoryVC_closemap", nil)];
    [closeMapButton addTarget:self action:@selector(closeMapButtonClick:) forControlEvents:UIControlEventTouchDown];
    closeMapButton.frame = CGRectMake( screenFrame.size.width - closeMapButton.frame.size.width - Screen_globalMargin, 5, closeMapButton.frame.size.width, closeMapButton.frame.size.height);
    
    // header of the
    ypos = 30;
    UIView *headerContainer = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, screenFrame.size.width, 100)];
    headerContainer.hidden = YES;
    DriveDetailsDurationLabelValue *headerDriveDurationLabelValue = [[DriveDetailsDurationLabelValue alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [headerDriveDurationLabelValue setNumericValue: 0.0f];
    [headerDriveDurationLabelValue applyFormattingWithLabel:nil andValue:nil];
    [headerDriveDurationLabelValue sizeMe];
    headerDriveDurationLabelValue.frame = CGRectMake(Screen_globalMargin, ypos, headerDriveDurationLabelValue.frame.size.width, headerDriveDurationLabelValue.frame.size.height);
    DriveDetailsDistanceLabelValue *headerDriveDistanceLabelValue = [[DriveDetailsDistanceLabelValue alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [headerDriveDistanceLabelValue setNumValue:0.0f withLabel: self.appDataManager.getUserDistanceUnitLabel];
    [headerDriveDistanceLabelValue applyFormattingWithLabel:nil andValue:nil];
    [headerDriveDistanceLabelValue sizeMe];
    headerDriveDistanceLabelValue.frame = CGRectMake(screenFrame.size.width - headerDriveDistanceLabelValue.frame.size.width - Screen_globalMargin + 5, ypos, headerDriveDistanceLabelValue.frame.size.width, headerDriveDistanceLabelValue.frame.size.height);
    
    // ScrollView and it's container
    
    ypos = 60;
    UIView *scrollViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, ypos, screenFrame.size.width, screenFrame.size.height - ypos)];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, screenFrame.size.height - ypos)];
    scrollView.delegate = self;
    _initialSVY = scrollView.frame.origin.y;
    _initialSVHeight = scrollView.frame.size.height;
    
    //scrollView.backgroundColor = [UIColor whiteColor];
    
    // Drive Map
    
    ypos = 0; // restet y pos but now it's value within the scroll view
    
    //DriveCustomMapView *driveMap = [self createDriveMap];
    
    // other drive components
    // we make space here for map snapshot
    
    ypos += kDriveMapViewHeight + 10;
    
    DriveDetailsDateLabel *driveDateLabel = [[DriveDetailsDateLabel alloc] init];
    [driveDateLabel fillWithDate:[NSDate new]];
    driveDateLabel.alpha = .5f;
    driveDateLabel.frame = CGRectMake(xpos, ypos, driveDateLabel.frame.size.width, driveDateLabel.frame.size.height);
    
    DriveDetailsTimeframeLabel *driveTimeframeLabel = [[DriveDetailsTimeframeLabel alloc] init];
    [driveTimeframeLabel fillFromDate:[NSDate new] toDate:[NSDate new]];
    driveTimeframeLabel.alpha = .5f;
    driveTimeframeLabel.frame = CGRectMake(scrollView.frame.size.width - driveTimeframeLabel.frame.size.width - Screen_globalMargin, driveDateLabel.frame.origin.y, driveTimeframeLabel.frame.size.width, driveTimeframeLabel.frame.size.height);
    
    ypos += driveDateLabel.frame.size.height + 10;
    
    DriveDetailsDurationLabelValue *driveDurationLabelValue = [[DriveDetailsDurationLabelValue alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [driveDurationLabelValue setNumericValue: 0.0f];
    [driveDurationLabelValue applyFormattingWithLabel:nil andValue:nil];
    [driveDurationLabelValue sizeMe];
    driveDurationLabelValue.frame = CGRectMake(Screen_globalMargin, ypos, driveDurationLabelValue.frame.size.width, driveDurationLabelValue.frame.size.height);
    _initialYOfHeaderSVElements = ypos;
    
    DriveDetailsDistanceLabelValue *driveDistanceLabelValue = [[DriveDetailsDistanceLabelValue alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [driveDistanceLabelValue setNumValue: 0.0f withLabel: self.appDataManager.getUserDistanceUnitLabel];
    [driveDistanceLabelValue applyFormattingWithLabel:nil andValue:nil];
    [driveDistanceLabelValue sizeMe];
    driveDistanceLabelValue.frame = CGRectMake(scrollView.frame.size.width - driveDistanceLabelValue.frame.size.width - Screen_globalMargin + 5, ypos, driveDistanceLabelValue.frame.size.width, driveDistanceLabelValue.frame.size.height);
    
    ypos += driveDistanceLabelValue.frame.size.height - 10;
    
    DayNightIndicatorView *dayNightIndicator = [[DayNightIndicatorView alloc] initWithFrame:CGRectMake((driveDurationLabelValue.frame.size.width - driveDurationLabelValue.frame.origin.x)/2 + 10, ypos + 10, 40, 30)];
    [dayNightIndicator indicateDayTime:NO];
    
    DriveDetailsExperienceLabelValue *driveExperienceLabelValue = [[DriveDetailsExperienceLabelValue alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [driveExperienceLabelValue setNumValue: 0.0f withLabel:@"exp"];
    [driveExperienceLabelValue applyFormattingWithLabel:nil andValue:nil];
    [driveExperienceLabelValue sizeMe];
    driveExperienceLabelValue.frame = CGRectMake(scrollView.frame.size.width - driveExperienceLabelValue.frame.size.width - Screen_globalMargin + 5, ypos, driveExperienceLabelValue.frame.size.width, driveExperienceLabelValue.frame.size.height);
    
    
    ypos += driveExperienceLabelValue.frame.size.height;
    DriveDetailsHeaderLabel *speedHeaderLabel = [[DriveDetailsHeaderLabel alloc] initWithFrame:CGRectMake(xpos, ypos, 30, 30)];
    speedHeaderLabel.alpha = .5f;
    [speedHeaderLabel setTitle:@"SPEED"];
    
    ypos += speedHeaderLabel.frame.size.height + 30;
    SpeedChart *speedChart = [[SpeedChart alloc] initWithFrame:CGRectMake((screenFrame.size.width - 200)/2, ypos, 200, 100)];
    
    ypos += speedChart.frame.size.height + 30;
    DriveDetailsHeaderLabel *accHeaderLabel = [[DriveDetailsHeaderLabel alloc] initWithFrame:CGRectMake(xpos, ypos, 30, 30)];
    accHeaderLabel.alpha = .5f;
    [accHeaderLabel setTitle:@"ACCELERATION"];
    
    ypos += accHeaderLabel.frame.size.height + 20;
    AccelerationChart *acc0_100Chart = [[AccelerationChart alloc] initWithFrame:CGRectMake((screenFrame.size.width - (3*68 + 2*10)) / 2, ypos, 68, 68)];
    [acc0_100Chart setRangeLabelText: @"0-100"];
    AccelerationChart *acc80_120Chart = [[AccelerationChart alloc] initWithFrame:CGRectMake(acc0_100Chart.frame.origin.x + 78, ypos, 68, 68)];
    [acc80_120Chart setRangeLabelText: @"80-120"];
    AccelerationChart *acc100_0Chart = [[AccelerationChart alloc] initWithFrame:CGRectMake(acc80_120Chart.frame.origin.x + 78, ypos, 68, 68)];
    [acc100_0Chart setRangeLabelText: @"100-0"];
    
    ypos += acc0_100Chart.frame.size.height + 30;
    DriveDetailsHeaderLabel *elevationHeaderLabel = [[DriveDetailsHeaderLabel alloc] initWithFrame:CGRectMake(xpos, ypos, 30, 30)];
    elevationHeaderLabel.alpha = .5f;
    [elevationHeaderLabel setTitle:@"ELEVATION"];
    
    ypos += elevationHeaderLabel.frame.size.height + 30;
    ElevationChart *elevationChart = [[ElevationChart alloc] initWithFrame:CGRectMake((screenFrame.size.width - 250) / 2, ypos, 250, 100)];
    
    ypos += elevationChart.frame.size.height + 30;
    DriveDetailsHeaderLabel *overloadsHeaderLabel = [[DriveDetailsHeaderLabel alloc] initWithFrame:CGRectMake(xpos, ypos, 30, 30)];
    overloadsHeaderLabel.alpha = .5f;
    [overloadsHeaderLabel setTitle:@"OVERLOADS"];
    
    ypos += overloadsHeaderLabel.frame.size.height + 30;
    OverloadChart *overloadChart = [[OverloadChart alloc] initWithFrame:CGRectMake((screenFrame.size.width - 150) / 2, ypos, 150, 150)];
    
    ypos += overloadChart.frame.size.height + 30;
    DriveDetailsHeaderLabel *directionHeaderLabel = [[DriveDetailsHeaderLabel alloc] initWithFrame:CGRectMake(xpos, ypos, 30, 30)];
    directionHeaderLabel.alpha = .5f;
    [directionHeaderLabel setTitle:@"DIRECTION"];
    
    ypos += directionHeaderLabel.frame.size.height + 30;
    CompassView *directionChart = [[CompassView alloc] initWithFrame:CGRectMake((screenFrame.size.width - 150) / 2, ypos, 150, 150)];
    directionChart.directionValue = M_PI + M_PI_2 + M_PI/2.1f;
    [directionChart animateToDirectionValue:M_PI];
    
    ypos += directionChart.frame.size.height + 30;
    
    scrollView.contentSize = CGSizeMake(screenFrame.size.width, ypos);
    
    [self.view addSubview:topEdgeSlider];
    [self.view addSubview:backButton];
    [self.view addSubview:scrollViewContainer];
    [scrollViewContainer addSubview:scrollView];
    [self.view addSubview:headerContainer];
    [headerContainer addSubview:headerDriveDurationLabelValue];
    [headerContainer addSubview:headerDriveDistanceLabelValue];
    
    //[scrollView addSubview:driveMap];
    [scrollView addSubview:driveDateLabel];
    [scrollView addSubview:driveDurationLabelValue];
    [scrollView addSubview:driveTimeframeLabel];
    [scrollView addSubview:driveDistanceLabelValue];
    [scrollView addSubview:dayNightIndicator];
    [scrollView addSubview:driveExperienceLabelValue];
    [scrollView addSubview:speedHeaderLabel];
    [scrollView addSubview:speedChart];
    [scrollView addSubview:accHeaderLabel];
    [scrollView addSubview:acc0_100Chart];
    [scrollView addSubview:acc80_120Chart];
    [scrollView addSubview:acc100_0Chart];
    [scrollView addSubview:elevationHeaderLabel];
    [scrollView addSubview:elevationChart];
    [scrollView addSubview:overloadsHeaderLabel];
    [scrollView addSubview:overloadChart];
    [scrollView addSubview:directionHeaderLabel];
    [scrollView addSubview:directionChart];
    
    _topEdgeSlider = topEdgeSlider;
    _backButton = backButton;
    _closeMapButton = closeMapButton;
    _headerContainer = headerContainer;
    
    _headerDriveDurationLabelValue = headerDriveDurationLabelValue;
    _headerDriveDistanceLabelValue = headerDriveDistanceLabelValue;
    _scrollViewContainer = scrollViewContainer;
    _scrollView = scrollView;
    
    //_driveMap = driveMap;
    _driveDateLabel = driveDateLabel;
    _driveDurationLabelValue = driveDurationLabelValue;
    _driveTimeframeLabel = driveTimeframeLabel;
    _driveDistanceLabelValue = driveDistanceLabelValue;
    _dayNightIndicator = dayNightIndicator;
    _driveExperienceLabelValue = driveExperienceLabelValue;
    _speedHeaderLabel = speedHeaderLabel;
    _speedChart = speedChart;
    _accHeaderLabel = accHeaderLabel;
    _acc0_100Chart = acc0_100Chart;
    _acc80_120Chart = acc80_120Chart;
    _acc100_0Chart = acc100_0Chart;
    _elevationHeaderLabel = elevationHeaderLabel;
    _elevationChart = elevationChart;
    _overloadsHeaderLabel = overloadsHeaderLabel;
    _overloadChart = overloadChart;
    _directionHeaderLabel = directionHeaderLabel;
    _directionChart = directionChart;
    
    [self maskScrollViewWithHeight:_scrollView.frame.size.height];
}

- (void) reloadInteractiveMapPathData {
    int interval = (int)((1 - (_interactiveDriveMap.zoom - _interactiveDriveMap.minZoom) / (_interactiveDriveMap.maxZoom - _interactiveDriveMap.minZoom)) * 15) + 1;
    
    //interval = 1;
    
    RMSphericalTrapezium mapBBox = [_interactiveDriveMap latitudeLongitudeBoundingBox];
    
    _interactivePathData = [DBTransactionManager getDriveTrackpointByRegion: mapBBox interval:interval andDriveId: _driveData.driveId andTranslateToMap: _interactiveDriveMap];
}

# pragma mark - Maps creation

/*- (DriveCustomMapView *) createDriveMap {
    RMMapboxSource *driveMapSrc = [[RMMapboxSource alloc] initWithMapID: @"xyyp.11781ddc"];
    
    DriveCustomMapView *driveMap = [[DriveCustomMapView alloc] initWithFrame:CGRectMake(0, 0, kDriveMapViewWidth, kDriveMapViewHeight) andTilesource:driveMapSrc centerCoordinate: CLLocationCoordinate2DMake(50.206, 19.164) zoomLevel: 11.0f maxZoomLevel: 17.0f minZoomLevel: 7.0f backgroundImage: [UIImage imageNamed:@"clear-bg"]];
    
    //RMMapView *driveMap = [[RMMapView alloc] initWithFrame:CGRectMake(0, 0, scrollView.frame.size.width, 250) andTilesource: driveMapSrc];
    
    driveMap.backgroundColor = [UIColor clearColor];
    driveMap.delegate = self;
    driveMap.draggingEnabled = NO;
    driveMap.clusteringEnabled = YES;
    
    //DriveCustomMapView *driveMap = [DriveCustomMapView alloc] initWithFrame:<#(CGRect)#> mapID:<#(NSString *)#> zoomWithLatitudeLongitudeBoundsSouthWest:<#(CLLocationCoordinate2D)#> northEast:<#(CLLocationCoordinate2D)#> completionHandler:^(UIImage *) {
 <#code#>
 }
    
    return driveMap;
}*/

- (void) createTempDriveMapForDriveData:(DriveData *) driveData {
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    
    _mapSnapshotDriveData = driveData;
    
    NSLog(@"SW lat: %f long: %f", driveData.southWest.latitude, driveData.southWest.longitude);
    NSLog(@"NE lat: %f long: %f", driveData.northEast.latitude, driveData.northEast.longitude);
    
    DriveCustomMapView *tmpDriveMap = [[DriveCustomMapView alloc] initWithFrame: CGRectMake(0, 0, screenFrame.size.width, kDriveMapViewHeight) mapID: API_MapBox_projectID zoomWithLatitudeLongitudeBoundsSouthWest:driveData.southWest northEast: driveData.northEast
                                                           completionHandler: ^(void) {
                                                               //[self createMapImagesFromMapSnapshop: mapSnapshot];
                                                           }];
    UIView *bgview = [[UIView alloc] init];
    bgview.backgroundColor = [UIColor clearColor];
        
    tmpDriveMap.backgroundColor = [UIColor clearColor];
    tmpDriveMap.backgroundView = bgview;
    tmpDriveMap.opaque = NO;
}

- (void) createStaticDriveMapForDriveData:(DriveData *) driveData {
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    
    //NSLog(@"SW lat: %f long: %f", driveData.southWest.latitude, driveData.southWest.longitude);
    //NSLog(@"NE lat: %f long: %f", driveData.northEast.latitude, driveData.northEast.longitude);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        RMStaticMapView *staticDriveMap = [[DriveCustomMapView alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, kDriveMapViewHeight) mapID:API_MapBox_projectID];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self styleStaticDriveMap: staticDriveMap];
            [self addMapSnapshot];
        });
    });
}

- (void) styleStaticDriveMap:(RMStaticMapView *) map {
    UIView *bgview = [[UIView alloc] init];
    bgview.backgroundColor = [UIColor clearColor];
    
    map.backgroundColor = [UIColor clearColor];
    map.backgroundView = bgview;
     
     _staticDriveMap = map;
     
     DriveStyledPath *staticDriveMapPath = [[DriveStyledPath alloc] initWithFrame: _staticDriveMap.frame];
     _staticDriveMapPath = staticDriveMapPath;
     _staticDriveMapPath.userInteractionEnabled = NO;
}

- (void) createInteractiveDriveMapForDriveData:(DriveData *) driveData {
    CGRect mapFrame = [[UIScreen mainScreen] bounds];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        RMMapboxSource *mapSrc = [[RMMapboxSource alloc] initWithMapID: API_MapBox_projectID];
        RMMapView *interactiveDriveMap = [[RMMapView alloc] initWithFrame:mapFrame andTilesource: mapSrc];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self styleInteractiveDriveMap: interactiveDriveMap];
            [self showInteractiveMap];
        });
    });
    
}

- (void) styleInteractiveDriveMap:(RMMapView *) map {
    UIView *bgview = [[UIView alloc] init];
    bgview.backgroundColor = [UIColor clearColor];
    
    map.minZoom = 2.0f;
    map.maxZoom = 16.0f;
    map.backgroundColor = [UIColor clearColor];
    map.backgroundView = bgview;
    map.delegate = self;
    
    _interactiveDriveMap = map;
    
    DriveStyledPath *interactiveDriveMapPath = [[DriveStyledPath alloc] initWithFrame: _interactiveDriveMap.frame];
    _interactiveDriveMapPath = interactiveDriveMapPath;
    _interactiveDriveMapPath.userInteractionEnabled = NO;
}

- (void) createMapImagesFromMapSnapshop: (UIImage *) snapshot {
    if(_driveData.driveId != _mapSnapshotDriveData.driveId) {
        return;
    }
    
    float historyListRowHeight = (float) HISTORY_LIST_ROW_HEIGHT;
    float scale = snapshot.scale;
    
    CGRect thumbRect = CGRectMake(0, (snapshot.size.height - historyListRowHeight)/2 * scale, snapshot.size.width * scale, historyListRowHeight * scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([snapshot CGImage], thumbRect);
    UIImage *mapListThumb = [UIImage imageWithCGImage: imageRef scale: scale orientation: snapshot.imageOrientation];
    
    BOOL mapSnapshotCreated = [FileSystemManager saveHistoryDriveMapImage: snapshot forDriveData: _driveData];
    BOOL listThumbCreated = [FileSystemManager saveHistoryListMapImage: mapListThumb forDriveData: _driveData];
    
    
    if(mapSnapshotCreated) {
        [self addMapSnapshot];
    }
    
    if(listThumbCreated) {
    }
    
}

- (void) addMapSnapshot {
    if(_staticDriveMap == nil) {
        [self createStaticDriveMapForDriveData: _driveData];
        return;
    }
    
    [self.scrollView addSubview: _staticDriveMap];
    
    CLLocationCoordinate2D sw = CLLocationCoordinate2DMake(_driveData.southWest.latitude, _driveData.southWest.longitude);
    CLLocationCoordinate2D ne = CLLocationCoordinate2DMake(_driveData.northEast.latitude, _driveData.northEast.longitude);
    
    //expand the area
    
    float expandFactor = 0.004f;
    
    ne.latitude += expandFactor;
    ne.longitude += expandFactor;
    sw.latitude -= expandFactor;
    sw.longitude -= expandFactor;
    
    [_staticDriveMap zoomWithLatitudeLongitudeBoundsSouthWest: sw northEast: ne animated:NO];
    
    if(_staticPathData == nil) {
        _staticPathData = [DBTransactionManager getDriveTrackpointByRegion: [_staticDriveMap latitudeLongitudeBoundingBox] interval:3 andDriveId: _driveData.driveId andTranslateToMap: _staticDriveMap];
        
        [_staticDriveMapPath redrawWithCollection: _staticPathData];
    }
    
    [self.scrollView addSubview: _staticDriveMapPath];
    
    UIButton *driveMapButton = [[UIButton alloc] initWithFrame: _staticDriveMap.frame];
    [driveMapButton setBackgroundColor:[UIColor clearColor]];
    [driveMapButton addTarget:self action:@selector(driveMapButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    _driveMapButton = driveMapButton;
    
    [self.scrollView addSubview: _driveMapButton];
}

- (void) showInteractiveMap {
    if(_interactiveDriveMap == nil) {
        [self createInteractiveDriveMapForDriveData: _driveData];
        return;
    }
    
    [_topEdgeSlider removeFromSuperview];
    [_backButton removeFromSuperview];
    [_headerContainer removeFromSuperview];
    [_scrollViewContainer removeFromSuperview];
    
    [self.view addSubview: _interactiveDriveMap];
    [self.view addSubview: _interactiveDriveMapPath];
    [self.view addSubview: _closeMapButton];
    
    CLLocationCoordinate2D sw = CLLocationCoordinate2DMake(_driveData.southWest.latitude, _driveData.southWest.longitude);
    CLLocationCoordinate2D ne = CLLocationCoordinate2DMake(_driveData.northEast.latitude, _driveData.northEast.longitude);
                                                           
    //expand the area
    
    float expandFactor = 0.004f;
    
    ne.latitude += expandFactor;
    ne.longitude += expandFactor;
    sw.latitude -= expandFactor;
    sw.longitude -= expandFactor;
    
    [_interactiveDriveMap zoomWithLatitudeLongitudeBoundsSouthWest: sw northEast: ne animated: NO];
    
    [self reloadInteractiveMapPathData];
    [_interactiveDriveMapPath redrawWithCollection: _interactivePathData];
    
    [self clearGestureRecognizers];
}

- (void) hideInteraciveMap {
    [_interactiveDriveMap removeFromSuperview];
    [_interactiveDriveMapPath removeFromSuperview];
    [_closeMapButton removeFromSuperview];
    
    [self.view addSubview: _topEdgeSlider];
    [self.view addSubview: _backButton];
    [self.view addSubview: _scrollViewContainer];
    [self.view addSubview: _headerContainer];
    
    [self addGestureRecognizers];
}

- (void) removeMap {
    if(_interactiveDriveMap) {
        [_interactiveDriveMap removeFromSuperview];
        _interactiveDriveMap.delegate = nil;
        _interactiveDriveMap = nil;
    }
    
    if(_interactiveDriveMapPath) {
        [_interactiveDriveMapPath removeFromSuperview];
        _interactiveDriveMapPath = nil;
    }
}


- (void) removeMapSnapshot {
    if(_staticDriveMap) {
        [_staticDriveMap removeFromSuperview];
        _staticDriveMap.delegate = nil;
        _staticDriveMap = nil;
    }
    
    if(_staticDriveMapPath) {
        [_staticDriveMapPath removeFromSuperview];
        _staticDriveMapPath = nil;
    }
    
    if(_driveMapButton) {
        [_driveMapButton removeFromSuperview];
        _driveMapButton = nil;
    }
}

- (void) maskScrollViewWithHeight: (float) height {
    _scrollMaskLayer = [CAGradientLayer layer];
    
    CGColorRef outerColor = [UIColor clearColor].CGColor;
    CGColorRef innerColor = [UIColor whiteColor].CGColor;
    
    _scrollMaskLayer.colors = [NSArray arrayWithObjects:(__bridge id)outerColor, (__bridge id)innerColor, (__bridge id)innerColor, (__bridge id)outerColor, nil];
    _scrollMaskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.02], [NSNumber numberWithFloat:0.98], [NSNumber numberWithFloat:1.0], nil];
    
    _scrollMaskLayer.frame = CGRectMake(0, _scrollView.frame.size.height - height, _scrollView.frame.size.width, height);
    
    _scrollViewContainer.layer.mask = _scrollMaskLayer;
}

- (void) addGestureRecognizers {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
    UISwipeGestureRecognizer *leftSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    leftSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    UISwipeGestureRecognizer *rightSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    rightSwipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:leftSwipeGesture];
    [self.view addGestureRecognizer:rightSwipeGesture];
}

- (void) clearGestureRecognizers {
    for(UIGestureRecognizer *gr in self.view.gestureRecognizers) {
        [self.view removeGestureRecognizer: gr];
    }
}


#pragma mark - Navigation

- (void) switchToHistoryViewController {
    UINavigationController *nc = self.navigationController;
    
    //HistoryDriveViewController *hdvc = [[HistoryDriveViewController alloc] init];
    //hdvc.routeData = _selectedDriveData;
    
    [nc popViewControllerAnimated:NO];
}

# pragma mark - Animation

- (void) triggerIndexChangedAnimation {
    _dataIndexChangedAnimationStartTime = [NSDate date];
    
    _dataIndexChangedAnimationStartValue = 0;
    _dataIndexChangedAnimationChange = 1;
    
    //date and time of a drive
    
    [_driveDateLabel fillWithDate: _driveData.startDate];
    _driveDateLabel.frame = CGRectMake(_driveDateLabel.frame.origin.x, _driveDateLabel.frame.origin.y, _driveDateLabel.frame.size.width, _driveDateLabel.frame.size.height);
    
    [_driveTimeframeLabel fillFromDate:_driveData.startDate toDate:_driveData.finishDate];
    _driveTimeframeLabel.frame = CGRectMake(_scrollView.frame.size.width - _driveTimeframeLabel.frame.size.width - Screen_globalMargin, _driveDateLabel.frame.origin.y, _driveTimeframeLabel.frame.size.width, _driveTimeframeLabel.frame.size.height);
    
    // speed chart
    
    CGPoint speedChartPosition = [self.scrollView convertPoint:_speedChart.frame.origin toView:self.view];
    
    //float avgSpeed = [NumberTools randomNumberFromRange:NSMakeRange(0, 350)];
    //float topSpeed = [NumberTools randomNumberFromRange:NSMakeRange(0, 350)];
    
    NSNumber *avgSpeedNumber = [_driveData.attributes valueForKey: DriveDataAttributeAvgSpeed];
    int avgSpeed = (int)[self.appDataManager convertSpeedToUserUnit: [avgSpeedNumber floatValue]];
    
    NSNumber *topSpeedNumber = [_driveData.attributes valueForKey: DriveDataAttributeTopSpeed];
    int topSpeed = (int)[self.appDataManager convertSpeedToUserUnit: [topSpeedNumber floatValue]];
    
    if(speedChartPosition.y < 0 || speedChartPosition.y > 550) {
        _speedChart.avgSpeed = avgSpeed;
        _speedChart.topSpeed = topSpeed;
        
        _speedChartAvgSpeedAnimationStartValue = _speedChart.avgSpeed;
        _speedChartTopSpeedAnimationStartValue = _speedChart.topSpeed;
        
        [_speedChart setForAnimarionAvgSpeed:_speedChart.avgSpeed andTopSpeed:_speedChart.topSpeed];
    } else {
        _speedChartAvgSpeedAnimationStartValue = _speedChart.avgSpeed;
        _speedChartTopSpeedAnimationStartValue = _speedChart.topSpeed;
        
        _speedChart.avgSpeed = avgSpeed;
        _speedChart.topSpeed = topSpeed;
    }
    
    // acceleration chart
    
    CGPoint accChartPosition = [self.scrollView convertPoint: _acc0_100Chart.frame.origin toView:self.view];
    
    float acc0_100 = [_driveData getBestAccelerationByKey: DriveDataAttributeAcc0_100];
    float acc80_120 = [_driveData getBestAccelerationByKey: DriveDataAttributeAcc80_120];
    float acc100_0 = [NumberTools randomNumberFromRange:NSMakeRange(400, 1999)] / 100.0f;
    
    if(accChartPosition.y < -_acc0_100Chart.frame.size.height || accChartPosition.y > 550) {
        _acc0_100Chart.value = acc0_100;
        _acc80_120Chart.value = acc80_120;
        _acc100_0Chart.value = acc100_0;
        
        _accChart0_100AnimationStartValue = _acc0_100Chart.value;
        _accChart80_120AnimationStartValue = _acc80_120Chart.value;
        _accChart100_0AnimationStartValue = _acc100_0Chart.value;
        
        [_acc0_100Chart setForAnimarionValue: _acc0_100Chart.value];
        [_acc80_120Chart setForAnimarionValue: _acc80_120Chart.value];
        [_acc100_0Chart setForAnimarionValue: _acc100_0Chart.value];
    } else {
        _accChart0_100AnimationStartValue = _acc0_100Chart.value;
        _accChart80_120AnimationStartValue = _acc80_120Chart.value;
        _accChart100_0AnimationStartValue = _acc100_0Chart.value;
        
        _acc0_100Chart.value = acc0_100;
        _acc80_120Chart.value = acc80_120;
        _acc100_0Chart.value = acc100_0;
    }
    
    // elevation chart
    
    CGPoint elevationChartPosition = [self.scrollView convertPoint: _elevationChart.frame.origin toView:self.view];
    
    if(elevationChartPosition.y < -_elevationChart.frame.size.height || elevationChartPosition.y > 550) {
        _elevationChartAnimationValid = NO;
        _elevationChart.maximumElevation = [_driveData getHighestElevation] + 30.0f;
        _elevationChart.dataSet = [_driveData getElevationDataSet];
    } else {
        _elevationChartAnimationValid = YES;
    }
    
    // overload chart
    
    CGPoint overloadChartPosition = [self.scrollView convertPoint: _overloadChart.frame.origin toView:self.view];
    
    if(overloadChartPosition.y < -_overloadChart.frame.size.height || overloadChartPosition.y > 550) {
        _overloadChartAnimationValid = NO;
        _overloadChart.dataSet = [self getTestOverloadDataSet];
    } else {
        _overloadChartAnimationValid = YES;
    }
    
    // direction chart
    
    float compassValue = [self getTestCompassData];
    [_directionChart animateToDirectionValue: compassValue];
    
    _dataIndexChangedAnimation50PercIn = NO;
    _dataIndexChangedAnimationInProgress = YES;
    
    [self animationTick:nil];
}

-(void) animationTick: (NSTimer *) timer {
    float timeDifference = [[NSDate date] timeIntervalSinceDate: _dataIndexChangedAnimationStartTime];
    
    if(timeDifference >= _animationDuration) {
        _dataIndexChangedAnimationInProgress = NO;
        [self animationStep: _dataIndexChangedAnimationStartValue + _dataIndexChangedAnimationChange];
        
        return;
    }
    
    float nv = [self getNextStepForCurrentTime:timeDifference fromStart: _dataIndexChangedAnimationStartValue withChange: _dataIndexChangedAnimationChange andDuration:_animationDuration];
    
    [self animationStep: nv];
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(animationTick:)
                                   userInfo:nil
                                    repeats:NO];
}

-(void) animationStep: (float) animationProgress {
    CGPoint dayNightIndicatorPosition = [self.scrollView convertPoint:_dayNightIndicator.frame.origin toView:self.view];
    
    // speed chart update
    
    float stepVAvgSpeed = _speedChartAvgSpeedAnimationStartValue + animationProgress * (_speedChart.avgSpeed - _speedChartAvgSpeedAnimationStartValue);
    
    float stepVTopSpeed = _speedChartTopSpeedAnimationStartValue + animationProgress * (_speedChart.topSpeed - _speedChartTopSpeedAnimationStartValue);
    
    if(stepVAvgSpeed != _speedChart.avgSpeed || stepVTopSpeed != _speedChart.topSpeed) {
        [_speedChart setForAnimarionAvgSpeed:stepVAvgSpeed andTopSpeed:stepVTopSpeed];
    } else {
        [_speedChart setForAnimarionAvgSpeed: _speedChart.avgSpeed andTopSpeed: _speedChart.topSpeed];
    }
    
    // acceleration chart update
    
    float stepAcc0_100 = _accChart0_100AnimationStartValue + animationProgress * (_acc0_100Chart.value - _accChart0_100AnimationStartValue);
    
    if(stepAcc0_100 != _acc0_100Chart.value) {
        [_acc0_100Chart setForAnimarionValue: stepAcc0_100];
    }
    
    float stepAcc80_120 = _accChart80_120AnimationStartValue + animationProgress * (_acc80_120Chart.value - _accChart80_120AnimationStartValue);
    
    if(stepAcc80_120 != _acc80_120Chart.value) {
        [_acc80_120Chart setForAnimarionValue: stepAcc80_120];
    }
    
    float stepAcc100_0 = _accChart100_0AnimationStartValue + animationProgress * (_acc100_0Chart.value - _accChart100_0AnimationStartValue);
    
    if(stepAcc100_0 != _acc100_0Chart.value) {
        [_acc100_0Chart setForAnimarionValue: stepAcc100_0];
    }
    
    // .5 in animation handle
    
    if(animationProgress < .5f) {
        _headerDriveDistanceLabelValue.alpha = _headerDriveDurationLabelValue.alpha = _driveDistanceLabelValue.alpha = _driveDurationLabelValue.alpha = (.5f - animationProgress) * 2;
        
        if (dayNightIndicatorPosition.y < 0) {
            _driveExperienceLabelValue.alpha = _dayNightIndicator.alpha = 1;
        } else {
            _driveExperienceLabelValue.alpha = _dayNightIndicator.alpha = (.5f - animationProgress) * 2;
        }
        
        if(_elevationChartAnimationValid == YES) {
            [_elevationChart setForAnimationMaximum: (_elevationChart.maximumElevation * 5) * (animationProgress * 2)];
        }
        
        if(_overloadChartAnimationValid == YES) {
            [_overloadChart setForAnimarionMaxOverload: _overloadChart.maximumOverload * (.5f - animationProgress) * 2];
        }
    } else {
        if(_dataIndexChangedAnimation50PercIn == NO) {
            _dataIndexChangedAnimation50PercIn = YES;
            
            [self animation50PercIn];
        }
        
        _headerDriveDistanceLabelValue.alpha = _headerDriveDurationLabelValue.alpha = _driveDistanceLabelValue.alpha = _driveDurationLabelValue.alpha = (animationProgress - .5f) * 2.0f;
        
        if (dayNightIndicatorPosition.y < 0.0f) {
            _driveExperienceLabelValue.alpha = _dayNightIndicator.alpha = 1.0f;
        } else {
            _driveExperienceLabelValue.alpha = _dayNightIndicator.alpha = (animationProgress - .5f) * 2.0f;
        }
        
        if(_elevationChartAnimationValid == YES) {
            [_elevationChart setForAnimationMaximum: (_elevationChart.maximumElevation * 5.0f) - (_elevationChart.maximumElevation * 4) * ((animationProgress - .5f) * 2.0f)];
        }
        
        if(_overloadChartAnimationValid == YES) {
            [_overloadChart setForAnimarionMaxOverload: _overloadChart.maximumOverload * ((animationProgress - .5f) * 2.0f)];
        }
    }
}

-(void) animation50PercIn {
    //int duration = [NumberTools randomNumberFromRange:NSMakeRange(15000, 80000)];
    //int distance = [NumberTools randomNumberFromRange:NSMakeRange(10, 1000)];
    //int experience = [NumberTools randomNumberFromRange:NSMakeRange(10000, 750000)];
    
    int duration = [_driveData.finishDate timeIntervalSinceDate: _driveData.startDate];
    float distance = [self.appDataManager convertDistanceToUserUnit: _driveData.distance];
    int experience = _driveData.experience;
    
    [_driveDurationLabelValue setNumericValue: duration];
    [_driveDurationLabelValue applyFormattingWithLabel:nil andValue:nil];
    [_driveDurationLabelValue sizeMe];
    
    [_headerDriveDurationLabelValue setNumericValue: duration];
    [_headerDriveDurationLabelValue applyFormattingWithLabel:nil andValue:nil];
    [_headerDriveDurationLabelValue sizeMe];
    
    [_driveDistanceLabelValue setNumValue:distance withLabel: self.appDataManager.getUserDistanceUnitLabel];
    [_driveDistanceLabelValue applyFormattingWithLabel:nil andValue:nil];
    [_driveDistanceLabelValue sizeMe];
    _driveDistanceLabelValue.frame = CGRectMake(_scrollView.frame.size.width - _driveDistanceLabelValue.frame.size.width - Screen_globalMargin + 5, _driveDistanceLabelValue.frame.origin.y, _driveDistanceLabelValue.frame.size.width, _driveDistanceLabelValue.frame.size.height);
    
    [_headerDriveDistanceLabelValue setNumValue:distance withLabel: self.appDataManager.getUserDistanceUnitLabel];
    [_headerDriveDistanceLabelValue applyFormattingWithLabel:nil andValue:nil];
    [_headerDriveDistanceLabelValue sizeMe];
    _headerDriveDistanceLabelValue.frame = CGRectMake(_scrollView.frame.size.width - _headerDriveDistanceLabelValue.frame.size.width - Screen_globalMargin + 5, _headerDriveDistanceLabelValue.frame.origin.y, _headerDriveDistanceLabelValue.frame.size.width, _headerDriveDistanceLabelValue.frame.size.height);
    
    [_driveExperienceLabelValue setNumValue:experience withLabel: @"EXP"];
    [_driveExperienceLabelValue applyFormattingWithLabel:nil andValue:nil];
    [_driveExperienceLabelValue sizeMe];
    _driveExperienceLabelValue.frame = CGRectMake(_scrollView.frame.size.width - _driveExperienceLabelValue.frame.size.width - Screen_globalMargin + 5, _driveExperienceLabelValue.frame.origin.y, _driveExperienceLabelValue.frame.size.width, _driveExperienceLabelValue.frame.size.height);
    
    BOOL day = ([NumberTools randomNumberFromRange:NSMakeRange(0, 2)] == 0);
    [_dayNightIndicator indicateDayTime: day];
    
    if(_elevationChartAnimationValid == YES) {
        _elevationChart.maximumElevation = [_driveData getHighestElevation] + 30.0f;
        _elevationChart.dataSet = [_driveData getElevationDataSet];
    }
    
    if(_overloadChartAnimationValid == YES) {
        _overloadChart.dataSet = [self getTestOverloadDataSet];
    }

}

-(float) getNextStepForCurrentTime: (float) time fromStart: (float) start withChange: (float) change andDuration: (float) duration  {
    time /= duration;
    return -change * time*(time-2) + start;
    
    //time /= duration;
    //return change*time*time*time*time*time + start;
}

# pragma mark - TEMP

- (NSArray *) getTestElevationDataSet {
    NSMutableArray *dataSet = [[NSMutableArray alloc] init];
    
    int i = -1;
    int q = [NumberTools randomNumberFromRange:NSMakeRange(3, 10)];
    
    while (++i < q) {
        [dataSet addObject: [ElevationChartVertex initWithElevation:[NumberTools randomNumberFromRange:NSMakeRange(500, 2000)] andDistance:i * 20]];
    }
    
    return [dataSet copy];
}

- (NSArray *) getTestOverloadDataSet {
    float rnd1 = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    float rnd2 = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    float rnd3 = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    float rnd4 = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    
    NSArray *dataSet = [NSArray arrayWithObjects:[NSNumber numberWithFloat:rnd1],
                        [NSNumber numberWithFloat:rnd2],
                        [NSNumber numberWithFloat:rnd3],
                        [NSNumber numberWithFloat:rnd4],
                        nil];
    
    /*float rnd = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    CGPoint vertex1 = CGPointMake(0.0f, rnd);
    
    rnd = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    CGPoint vertex2 = CGPointMake(rnd, 0.0f);
    
    rnd = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    CGPoint vertex3 = CGPointMake(0.0f, -rnd);
    
    rnd = ((float)[NumberTools randomNumberFromRange:NSMakeRange(0, 400)]) / 100;
    CGPoint vertex4 = CGPointMake(-rnd, 0.0f);
    
    NSArray *dataSet = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:vertex1],
                        [NSValue valueWithCGPoint:vertex2],
                        [NSValue valueWithCGPoint:vertex3],
                        [NSValue valueWithCGPoint:vertex4],
                        nil];
    
    //[graph setDataSet: dataSet];
    
     -----------
    NSMutableArray *dataSet = [[NSMutableArray alloc] init];
    
    int i = -1;
    int q = 4;
    
    float x, y;
    
    //[NumberTools randomNumberFromRange:NSMakeRange(3, 10)];
    
    while (++i < q) {
        x = [NumberTools randomNumberFromRange:NSMakeRange(0, 800)] / 100 - 4;
        y = [NumberTools randomNumberFromRange:NSMakeRange(0, 800)] / 100 - 4;
        [dataSet addObject: [NSValue valueWithCGPoint: CGPointMake(x, y)]];
    }*/
    
    return [dataSet copy];
}

- (float) getTestCompassData {
    float rnd = ((float)[NumberTools randomNumberFromRange:NSMakeRange(1000, 36000)]) / 100;
    
    NSLog(@"random compass %f", rnd);
    
    return rnd;
}

@end
