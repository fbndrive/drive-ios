#import "BasicButton.h"
#import "BoldLabelBoldValueLabelValueView.h"
#import "Constants.h"
#import "HistoryDriveViewController.h"
#import "HistoryViewController.h"
#import "HistoryTableListCell.h"
#import "NumberTools.h"

@interface HistoryViewController ()

@property (nonatomic) int historyDataCount;
@property (nonatomic) int selectedDataIndex;
@property (strong, nonatomic) NSArray *historyData;
@property (strong, nonatomic) BasicButton *backButton;
@property (strong, nonatomic) BoldLabelBoldValueLabelValueView *noOfDrives;
@property (strong, nonatomic) UITableView *historyTable;

@property (strong, nonatomic) CAGradientLayer *maskLayer;

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    self.appDataManager = [AppDataManager appDataManager];
    [super viewDidLoad];
    
    _historyData = [self.appDataManager getHistoryOfDrives];
    _historyDataCount = (int)[_historyData count];
    
    [_historyTable reloadData];
}

- (void) setAnimationInProps {
    self.view.alpha = 0;
    
    _historyTable.frame = CGRectMake(_historyTable.frame.origin.x, _historyTable.frame.origin.y + Screen_globalMargin, _historyTable.frame.size.width, _historyTable.frame.size.height);
}

- (void) animationIn {
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.view.alpha = 1;
                         _historyTable.frame = CGRectMake(_historyTable.frame.origin.x, _historyTable.frame.origin.y - Screen_globalMargin, _historyTable.frame.size.width, _historyTable.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         [self animationInDone];
                     }];
}

- (void) animationOut {
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.view.alpha = 0;
                         _historyTable.frame = CGRectMake(_historyTable.frame.origin.x, _historyTable.frame.origin.y + 150, _historyTable.frame.size.width, _historyTable.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         [self animationOutDone];
                     }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _historyDataCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"HistoryTableListCell";
    
    HistoryTableListCell *cell = (HistoryTableListCell *)[_historyTable dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    DriveData *dd = [_historyData objectAtIndex:indexPath.row];
    [cell drawWithData:dd andRowIndex: (int) indexPath.row];
    
    //cell.textLabel.text = [NSString stringWithFormat:@"Track %ld", (long)indexPath.row];
    //cell.textLabel.font = [cell.textLabel.font fontWithSize:16];
    
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%i Games", friend.gameCount];
    //cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedDataIndex = (int)[indexPath indexAtPosition:1];
    [self switchToHistoryDriveViewController];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    _maskLayer.position = CGPointMake(0, scrollView.contentOffset.y);
    [CATransaction commit];
}

- (void)backButtonClick: (id) target {

}

- (void) getData {
    
}

- (void) drawUI {
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    BasicButton *backButton = [[BasicButton alloc] initWithFrame:CGRectMake(Screen_globalMargin, Screen_globalMargin, screenFrame.size.width, 40)];
    [backButton setTitle: NSLocalizedString(@"HistoryVC_backtomenu", nil)];
    [backButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    BoldLabelBoldValueLabelValueView *noOfDrives = [[BoldLabelBoldValueLabelValueView alloc] initWithFrame:CGRectMake(0, 25, 10, 10)];
    [noOfDrives setNumValue:_historyDataCount withLabel:@"DRIVES by DATE"];
    [noOfDrives applyFormattingWithLabel:nil andValue:nil];
    [noOfDrives sizeMe];
    
    CGRect noOfDrivesFrame = noOfDrives.frame;
    noOfDrivesFrame.origin.x = screenFrame.size.width - noOfDrives.frame.size.width - Screen_globalMargin;
    noOfDrives.frame = noOfDrivesFrame;
    
    int ypos = 80;
    UITableView *historyTable = [[UITableView alloc] initWithFrame:CGRectMake(0, ypos, screenFrame.size.width, screenFrame.size.height - ypos - 20)];
    
    [historyTable registerClass:[HistoryTableListCell class] forCellReuseIdentifier:@"HistoryTableListCell"];
    historyTable.dataSource = self;
    historyTable.delegate = self;
    historyTable.rowHeight = HISTORY_LIST_ROW_HEIGHT;
    historyTable.backgroundView = nil;
    historyTable.backgroundColor = [UIColor clearColor];
    historyTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    //historyTable.pagingEnabled = YES;
    
    [self.view addSubview:backButton];
    [self.view addSubview:noOfDrives];
    [self.view addSubview:historyTable];
    
    _backButton = backButton;
    _noOfDrives = noOfDrives;
    _historyTable = historyTable;
    
    _maskLayer = [CAGradientLayer layer];
        
    CGColorRef outerColor = [UIColor clearColor].CGColor;
    CGColorRef innerColor = [UIColor whiteColor].CGColor;
        
    _maskLayer.colors = [NSArray arrayWithObjects:(__bridge id)outerColor, (__bridge id)innerColor, (__bridge id)innerColor, (__bridge id)outerColor, nil];
    _maskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.02], [NSNumber numberWithFloat:0.98], [NSNumber numberWithFloat:1.0], nil];
        
    _maskLayer.bounds = CGRectMake(0, 0, _historyTable.frame.size.width, _historyTable.frame.size.height);
    _maskLayer.anchorPoint = CGPointZero;
        
    _historyTable.layer.mask = _maskLayer;
}

#pragma mark - Navigation

- (void) switchToHistoryDriveViewController {
    UINavigationController *nc = self.navigationController;
    
    HistoryDriveViewController *hdvc = [[HistoryDriveViewController alloc] init];
    hdvc.historyData = _historyData;
    [hdvc applyDataIndex:_selectedDataIndex];
    
    [nc pushViewController:hdvc animated:NO];
}

@end
