#import "AppDataManager.h"
#import <UIKit/UIKit.h>

@interface BasicViewController : UIViewController

@property (strong, nonatomic) AppDataManager *appDataManager;

- (void) drawUI;
- (void) setAnimationInProps;
- (void) animationIn;
- (void) animationInDone;
- (void) animationOut;
- (void) animationOutDone;

@end
