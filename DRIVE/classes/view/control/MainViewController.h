#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <AVAudioPlayerDelegate>

- (void) showSnaphotBlink;

@end
