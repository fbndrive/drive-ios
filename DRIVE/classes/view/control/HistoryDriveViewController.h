#import "BasicViewController.h"
#import "RMMapViewDelegate.h"

@interface HistoryDriveViewController : BasicViewController <RMMapViewDelegate, UIScrollViewDelegate>

@property (nonatomic,strong) NSArray *historyData;

- (void) applyDataIndex:(int) index;

@end
