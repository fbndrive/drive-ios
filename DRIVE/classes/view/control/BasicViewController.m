#import "BasicViewController.h"

@interface BasicViewController ()

@end

@implementation BasicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self drawUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setAnimationInProps];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self animationIn];
}

- (void) drawUI {
    
}

- (void) setAnimationInProps {
    self.view.alpha = 0;
}

- (void) animationIn {
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.view.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         [self animationInDone];
                     }];
}

- (void) animationInDone {
    
}

- (void) animationOut {
    [UIView animateWithDuration:.25
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [self animationOutDone];
                     }];
}

- (void) animationOutDone {
    
}

@end
