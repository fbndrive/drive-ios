#import "AppDataManager.h"
#import "ArrowArcClockView.h"
#import "BoldLabelBoldValueLabelValueView.h"
#import "Constants.h"
#import "DashboardLevelLabelValueView.h"
#import "DriveDashboardViewController.h"
#import "DriveViewController.h"
#import "GlowButton.h"
#import "RegLabelBoldValueLabelValueView.h"
#import "UIImage+Tint.h"

@interface DriveDashboardViewController ()

@property (nonatomic,strong) ArrowArcClockView *clock;
@property (nonatomic,strong) RegLabelBoldValueLabelValueView *distanceDriven;
@property (nonatomic,strong) RegLabelBoldValueLabelValueView *expGained;
@property (nonatomic,strong) BoldLabelBoldValueLabelValueView *nextLevelTarget;
@property (nonatomic,strong) DashboardLevelLabelValueView *currentLevel;
@property (nonatomic,strong) GlowButton *driveButton;
@property (nonatomic,strong) NSTimer *blinkTimer;

@end

@implementation DriveDashboardViewController

- (void)viewDidLoad {
    self.appDataManager = [AppDataManager appDataManager];
    
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self updateUIValues];
    
    NSNotification *dashboardEnteredNotification = [[NSNotification alloc] initWithName:DriveNavi_dashboardEntered object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:dashboardEnteredNotification];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if(_blinkTimer) {
        _blinkTimer = nil;
        [_blinkTimer invalidate];
    }
}

- (void)driveButtonClick: (id) target {
    [self animationOut];
}

#pragma mark - User Interface

- (void) drawUI {
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    
    ArrowArcClockView *clock = [[ArrowArcClockView alloc] initWithFrame:CGRectMake(190, 36, 400, 600)];
    //[clock animateToValue:M_PI/3];
    
    RegLabelBoldValueLabelValueView *distanceDriven = [[RegLabelBoldValueLabelValueView alloc] initWithFrame:CGRectMake(65, 288, 10, 10)];
    [distanceDriven setNumValue:0.0f withLabel:self.appDataManager.getUserDistanceUnitLabel];
    //[kmDriven applyFormattingWithLabel:nil andValue:nil];
    //[kmDriven sizeMe];
    
    RegLabelBoldValueLabelValueView *expGained = [[RegLabelBoldValueLabelValueView alloc] initWithFrame:CGRectMake(181, 288, 10, 10)];
    [expGained setNumValue:0.0f withLabel:@"exp"];
    //[expGained applyFormattingWithLabel:nil andValue:nil];
    //[expGained sizeMe];
    
    BoldLabelBoldValueLabelValueView *nextLevelTarget = [[BoldLabelBoldValueLabelValueView alloc] initWithFrame:CGRectMake(50, 128, 10, 10)];
    [nextLevelTarget setNumValue:0.0f withLabel:@"next level"];
    //[nextLevelTarget applyFormattingWithLabel:nil andValue:nil];
    //[nextLevelTarget sizeMe];
    
    //CGRect nextLevelTargetFrame = nextLevelTarget.frame;
    //nextLevelTargetFrame.origin.x = clock.frame.origin.x + clock.frame.size.width - nextLevelTarget.frame.size.width + 6;
    //nextLevelTarget.frame = nextLevelTargetFrame;
    
    //NSLog(@"frame: %f, %f", clock.frame.origin.x, nextLevelTarget.frame.size.width);
    
    DashboardLevelLabelValueView *currentLevel = [[DashboardLevelLabelValueView alloc] initWithFrame:CGRectMake(67, 150, 10, 10)];
    [currentLevel setNumValue:0.0f withLabel:@"level"];
    //[currentLevel applyFormattingWithLabel:nil andValue:nil];
    //[currentLevel sizeMe];
    
    UIImage *driveButtonImage = [[UIImage imageNamed:@"drive-cta-white"] tintedImageWithColor:[self.appDataManager driveDescription].color];
    CGRect driveButtonFrame = CGRectMake((screenFrame.size.width - driveButtonImage.size.width) / 2, roundf(screenFrame.size.height - driveButtonImage.size.height - screenFrame.size.height*0.112f), driveButtonImage.size.width, driveButtonImage.size.height);
    
    GlowButton *driveButton = [[GlowButton alloc] initWithFrame:driveButtonFrame];
    [driveButton setBackgroundImage:driveButtonImage forState:UIControlStateNormal];
    [driveButton setBackgroundColor:[UIColor clearColor]];
    
    _clock = clock;
    _distanceDriven = distanceDriven;
    _expGained = expGained;
    _currentLevel = currentLevel;
    _nextLevelTarget = nextLevelTarget;
    _driveButton = driveButton;
    
    [self.view addSubview: clock];
    [self.view addSubview: distanceDriven];
    [self.view addSubview: expGained];
    [self.view addSubview: currentLevel];
    [self.view addSubview: nextLevelTarget];
    [self.view addSubview: driveButton];
    
    [driveButton addTarget:self action:@selector(driveButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    return;
    
    // design guide
    
    UIImageView *guideImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DF-Dash-D1"]];
    [self.view insertSubview:guideImage atIndex:0];
    guideImage.alpha = .5;
}

- (void) updateUIValues {
    [_distanceDriven setNumericValue:[self.appDataManager convertDistanceToUserUnit: self.appDataManager.totalDistance]];
    [_distanceDriven applyFormattingWithLabel:nil andValue:nil];
    [_distanceDriven sizeMe];
    
    [_expGained setNumericValue:self.appDataManager.totalExperience];
    [_expGained applyFormattingWithLabel:nil andValue:nil];
    [_expGained sizeMe];
    
    [_currentLevel setNumericValue:self.appDataManager.levelData.level];
    [_currentLevel applyFormattingWithLabel:nil andValue:nil];
    [_currentLevel sizeMe];
    
    int nextLvlRequiredPoints = self.appDataManager.levelData.nextTreshold - self.appDataManager.totalExperience;
    
    [_nextLevelTarget setNumValue:nextLvlRequiredPoints withLabel:@"next level"];
    [_nextLevelTarget applyFormattingWithLabel:nil andValue:nil];
    [_nextLevelTarget sizeMe];
    
    CGRect nextLevelTargetFrame = _nextLevelTarget.frame;
    nextLevelTargetFrame.origin.x = _clock.frame.origin.x + _clock.frame.size.width - _nextLevelTarget.frame.size.width + 6;
    _nextLevelTarget.frame = nextLevelTargetFrame;
}

#pragma mark - Animation In/Out

- (void) animationIn {
    [super animationIn];
}

- (void) animationInDone {
    [self clockAndLevelIn];
}

- (void) animationOut {
    [super animationOut];
}

- (void) animationOutDone {
    [self switchToDriveViewController];
}

- (void) clockAndLevelIn {
    int expAboveCurrentThreshold = self.appDataManager.totalExperience - self.appDataManager.levelData.currentThreshold;
    int currentLevelBand = self.appDataManager.levelData.nextTreshold - self.appDataManager.levelData.currentThreshold;
    float clockHandAngle = M_PI_2 - (float)expAboveCurrentThreshold/(float)currentLevelBand*M_PI_2;
    
    [_clock animateToValue:clockHandAngle];
    
    [self blinkValues:nil];
}

#pragma mark - Animation Blink

- (void) blinkValues : (NSTimer *) timer {
    int rnd = rand() % 3;
    
    if(rnd == 0) {
        [_distanceDriven randomBlink:nil];
    } else if (rnd == 1) {
        [_expGained randomBlink:nil];
    } else {
        [_nextLevelTarget randomBlink:nil];
    }
    
    _blinkTimer = [NSTimer scheduledTimerWithTimeInterval:5
                                 target:self
                               selector:@selector(blinkValues:)
                               userInfo:nil
                                repeats:NO];
}

#pragma mark - Navigation

- (void) switchToDriveViewController {
    UINavigationController *nc = self.navigationController;
    
    DriveViewController *dvc = [[DriveViewController alloc] init];
    
    [nc pushViewController:dvc animated:NO];
}

@end
