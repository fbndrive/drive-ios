#import "SnaphotBlinkViewController.h"

@interface SnaphotBlinkViewController ()

@end

@implementation SnaphotBlinkViewController

- (void) drawUI {
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void) animationIn {
    self.view.alpha = 0;
    
    [UIView animateWithDuration:.05
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.view.alpha = .5;
                     }
                     completion:^(BOOL finished){
                         [self animationInDone];
                     }];
}

- (void) animationInDone {
    [self animationOut];
}

- (void) animationOut {
    
    [UIView animateWithDuration:.1
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [self animationOutDone];
                     }];
}

- (void) animationOutDone {
    
}

@end
