#import "AppDelegate.h"
#import "AppDataManager.h"
#import "BigFontBasicButton.h"
#import "Constants.h"
#import <CoreLocation/CoreLocation.h>
#import "DBTransactionManager.h"
#import "DriveData.h"
#import "DriveStyledPath.h"
#import "DriveViewController.h"
#import "DriveExpLabelValueView.h"
#import "DriveFloatValueLabelValueView.h"
#import "DriveRouteController.h"
#import "DriveWaveController.h"
#import "DriveSummaryViewController.h"
#import "DriveTimeLabelValueView.h"
#import "DriveTrackpointCollection.h"
#import "DriveWaveRecordIndicator.h"
#import "GlowButton.h"
#import "MainViewController.h"
#import "NoTilesMapSource.h"
#import "RMMapView.h"
#import "RMMapboxSource.h"
#import "RouteSpecialStageData.h"
#import "SoundTools.h"
#import "SoundManager.h"
#import "UIImage+Tint.h"

@interface DriveViewController ()

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) DriveExpLabelValueView *driveExp;
@property (nonatomic,strong) DriveFloatValueLabelValueView *driveDist;
@property (nonatomic,strong) DriveTimeLabelValueView *driveTime;
@property (nonatomic,strong) DriveFloatValueLabelValueView *driveSpeed;

@property (nonatomic,strong) DriveWaveRecordIndicator *waveRecordIndicator;

@property (nonatomic,strong) GlowButton *stopButton;
@property (nonatomic,strong) BigFontBasicButton *confirmStopButton;
@property (nonatomic,strong) BigFontBasicButton *declineStopButton;

@property (nonatomic,strong) RMMapView *driveMap;
@property (nonatomic,strong) DriveStyledPath *driveStyledPath;

@property (nonatomic, strong) DriveRouteController *driveRouteController;
@property (nonatomic, strong) DriveWaveController *driveWaveController;
@property (nonatomic) BOOL driveInitialised;

@property (nonatomic) int recordWaveProcessState;

@property (nonatomic) float recordWaveProcessAnimationInDuration;
@property (nonatomic) float recordWaveProcessAnimationInStartValue;
@property (nonatomic, strong) NSDate *recordWaveProcessAnimationInStartTime;

@property (nonatomic) float recordWaveProcessAnimationOutDuration;
@property (nonatomic) float recordWaveProcessAnimationOutStartValue;
@property (nonatomic, strong) NSDate *recordWaveProcessAnimationOutStartTime;

@property (nonatomic) float recordWaveProcessRecDuration;
@property (nonatomic) float recordWaveProcessRecStartValue;
@property (nonatomic, strong) NSDate *recordWaveProcessRecStartTime;

@property (nonatomic, strong) RouteSpecialStageData *specialStageData;
@property (nonatomic,strong) NSTimer *interfaceUpdateTimer;

@end

@implementation DriveViewController

- (void)viewDidLoad {
    self.appDataManager = [AppDataManager appDataManager];
    
    _driveRouteController = [[DriveRouteController alloc] init];
    [_driveRouteController start];
    
    _driveWaveController = [[DriveWaveController alloc] init];
    
    _recordWaveProcessState = DriveRecordWaveProcessStateDefault;
    _recordWaveProcessAnimationInDuration = .7f;
    _recordWaveProcessAnimationOutDuration = .3f;
    _recordWaveProcessRecDuration = 6.0f;
    
    [super viewDidLoad];
    
    [self setupLocationManager];
    [self addRouteDataObservers];
    
    //disables auto-lock on the device
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    /*UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:tapGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [self.view addGestureRecognizer:pinchGesture];*/
    
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    panGesture.minimumNumberOfTouches = 2;
    panGesture.maximumNumberOfTouches = 2;
    
    [self.view addGestureRecognizer: panGesture];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _specialStageData = nil;
    _driveInitialised = NO;
    
    NSNotification *driveEnteredNotification = [[NSNotification alloc] initWithName:DriveNavi_driveEntered object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:driveEnteredNotification];
    
    [self playWelcomeSound];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if(_interfaceUpdateTimer) {
        [_interfaceUpdateTimer invalidate];
        _interfaceUpdateTimer = nil;
    }
    
    [self killLocationManager];
    [self removeRouteDataObservers];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
    // TODO
    // remove gesture recognizers
}

- (void) addRouteDataObservers {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(oneKmThresholdHit:) name:Distance_oneKmThresholdHit object:nil];
    [center addObserver:self selector:@selector(fiveKmThresholdHit:) name:Distance_fiveKmThresholdHit object:nil];
    [center addObserver:self selector:@selector(tenKmThresholdHit:) name:Distance_tenKmThresholdHit object:nil];
}

- (void) removeRouteDataObservers {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self name:Distance_oneKmThresholdHit object:nil];
    [center removeObserver:self name:Distance_fiveKmThresholdHit object:nil];
    [center removeObserver:self name:Distance_tenKmThresholdHit object:nil];
}


- (void) setupLocationManager {
    AppDelegate *myApplication = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _locationManager = myApplication.locationManager;
    _locationManager.delegate = self;
    
    
}

- (void) killLocationManager {
    _locationManager.delegate = nil;
    _locationManager = nil;
}

- (void) takeSnapshot {
    // TODO snapshots!
    return;
    
    AppDelegate *myApplication = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    RouteSnapshotData *rsd = [[RouteSnapshotData alloc] init];
    rsd.userId = self.appDataManager.userData.userId;
    //rsd.speed = [_driveRouteController getCurrentSpeed];
    //rsd.coordinate = [_driveRouteController currentLocation].coordinate;
    
    BOOL result = [DBTransactionManager saveSnapshot:rsd];
    
    if(result) {
        [((MainViewController *) myApplication.window.rootViewController) showSnaphotBlink];
    }
}

- (void) takeSpecialStage {
    [_driveRouteController setupSpecialStage];
}

- (void) detectIfSnapshotPresentInCurrentLocation: (CLLocation *) location {
    NSArray *snapshots = self.appDataManager.narrowedSnapshotArray;
    RouteSnapshotData *rsd;
    CLLocation *l;
    
    float distance = 0.0f;
    //float prevDistance = 0.0f;
    float closestDistance = -1.0f;
    RouteSnapshotData *closestSnapshot;
    
    int i = -1;
    int q = (int)[snapshots count];
    
    while(++i < q) {
        rsd = [snapshots objectAtIndex:i];
        l = [[CLLocation alloc]initWithLatitude:rsd.coordinate.latitude longitude:rsd.coordinate.longitude];
        
        distance = [location distanceFromLocation:l];
        
        if(closestDistance < 0.0f || distance <= closestDistance) {
            closestSnapshot = rsd;
            closestDistance = distance;
        }
    }
    
    NSLog(@"Closest snapshot in %f meters", closestDistance);
}

#pragma mark - Event Handlers

- (void)doubleTapOnMap:(RMMapView *)map at:(CGPoint)point {
    
}

- (void)oneKmThresholdHit:(NSNotification *) notification {
    [self.appDataManager narrowSnaphotsToLocation: _driveRouteController.currentLocation];
}

- (void)fiveKmThresholdHit:(NSNotification *) notification {
    //NSLog(@"Distance threshold hit: 5km");
}


- (void)tenKmThresholdHit:(NSNotification *) notification {
    //NSLog(@"Distance threshold hit: 10km");
}

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        [self takeSpecialStage];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self animationInOfWaveRecordProcess];
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if(_recordWaveProcessState == DriveRecordWaveProcessStateAnimationIn) {
        [self animationOutOfWaveRecordProcess];
    }
}

-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    //[self animationOutOfWaveRecordProcess];
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateRecognized) {
        [self playRouteSummarySound];
        [self takeSnapshot];
    }
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateChanged) {
        //NSLog(@"test %@", NSStringFromCGPoint([sender velocityInView: self.view]));
        CGPoint vpt = [sender velocityInView: self.view];
        
        [_driveRouteController adjustDebugAcc: vpt.y];
    }
}

- (void) stopButtonClick: (id) target {
    _confirmStopButton.hidden = NO;
    _declineStopButton.hidden = NO;
    
    [_stopButton animateOut];
    [_confirmStopButton animateIn];
    [_declineStopButton animateIn];
}

- (void) confirmStopButtonClick: (id) target {
    [_driveRouteController stop];
    
    [self animationOut];
}

- (void) declineStopButtonClick: (id) target {
    [_stopButton animateIn];
    [_confirmStopButton animateOut];
    [_declineStopButton animateOut];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if(_driveInitialised == NO) {
        _driveInitialised = YES;
        return;
    }
    
    CLLocation *currentLocation = [locations lastObject];
    
    if(currentLocation) {
        [_driveRouteController addLocation: currentLocation];
        
        //[_driveMap setCenterCoordinate:currentLocation.coordinate animated:NO];
        [self redrawPath];
        
        //[self detectIfSnapshotPresentInCurrentLocation:currentLocation];
    }
}


#pragma mark - User Interface

- (void) drawUI {
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    DriveExpLabelValueView *driveExp = [[DriveExpLabelValueView alloc] initWithFrame:CGRectMake(65, 30, 10, 10)];
    [driveExp setNumValue: 0 withLabel:@"exp"];
    [driveExp applyFormattingWithLabel:nil andValue:nil];
    [driveExp sizeMe];
    
    DriveFloatValueLabelValueView *driveDistance = [[DriveFloatValueLabelValueView alloc] initWithFrame:CGRectMake(65, 125, 10, 10)];
    [driveDistance setNumValue: 0 withLabel:[NSString stringWithFormat:@"dist (%@)", [self.appDataManager getUserDistanceUnitLabel]]];
    [driveDistance applyFormattingWithLabel:nil andValue:nil];
    [driveDistance sizeMe];
    
    DriveTimeLabelValueView *driveTime = [[DriveTimeLabelValueView alloc] initWithFrame:CGRectMake(65, 204, 10, 10)];
    [driveTime setNumValue: 0 withLabel:@"time"];
    [driveTime applyFormattingWithLabel:nil andValue:nil];
    [driveTime sizeMe];
    
    DriveFloatValueLabelValueView *driveSpeed = [[DriveFloatValueLabelValueView alloc] initWithFrame:CGRectMake(65, 281, 10, 10)];
    [driveSpeed setNumValue:0.0f withLabel:@"speed (km/h)"];
    [driveSpeed applyFormattingWithLabel:nil andValue:nil];
    [driveSpeed sizeMe];
    
    float indicatorWidth = 150;
    DriveWaveRecordIndicator *waveRecordIndicator = [[DriveWaveRecordIndicator alloc] initWithFrame: CGRectMake(screenFrame.size.width/2 - indicatorWidth/2, screenFrame.size.height/2 - indicatorWidth/2, indicatorWidth, indicatorWidth)];
    waveRecordIndicator.color = [self.appDataManager driveDescription].color;
    waveRecordIndicator.progress = 0.0f;
    waveRecordIndicator.alpha = 0.0f;
    
    UIImage *stopButtonImage = [[UIImage imageNamed:@"stop-cta"] tintedImageWithColor:[self.appDataManager driveDescription].color];
    CGRect stopButtonFrame = CGRectMake((screenFrame.size.width - stopButtonImage.size.width) / 2, roundf(screenFrame.size.height - stopButtonImage.size.height - screenFrame.size.height*0.112f), stopButtonImage.size.width, stopButtonImage.size.height);
    GlowButton *stopButton = [[GlowButton alloc] initWithFrame:stopButtonFrame];
    [stopButton setBackgroundImage:stopButtonImage forState:UIControlStateNormal];
    [stopButton setBackgroundColor:[UIColor clearColor]];
    
    BigFontBasicButton *confirmStopButton = [[BigFontBasicButton alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, screenFrame.size.height)];
    [confirmStopButton setTitle:@"yes"];
    confirmStopButton.hidden = YES;
    confirmStopButton.frame = CGRectMake(screenFrame.size.width/4 - confirmStopButton.frame.size.width/2, stopButtonFrame.origin.y + confirmStopButton.frame.size.height/2, confirmStopButton.frame.size.width, confirmStopButton.frame.size.height);
    [confirmStopButton addTarget:self action:@selector(confirmStopButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    BigFontBasicButton *declineStopButton = [[BigFontBasicButton alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, screenFrame.size.height)];
    [declineStopButton setTitle:@"no"];
    declineStopButton.hidden = YES;
    declineStopButton.frame = CGRectMake(3*screenFrame.size.width/4 - declineStopButton.frame.size.width/2, stopButtonFrame.origin.y + declineStopButton.frame.size.height/2, declineStopButton.frame.size.width, declineStopButton.frame.size.height);
    [declineStopButton addTarget:self action:@selector(declineStopButtonClick:) forControlEvents:UIControlEventTouchDown];

    
    //DriveMapView *driveMap = [[DriveMapView alloc] initWithFrame:CGRectMake(5, 40, screenFrame.size.width - 10, stopButtonFrame.origin.y - 60)];
    //DriveMapView *driveMap = [[DriveMapView alloc] initWithFrame:CGRectMake(5, 40, 150, 250)];
    
    // MAP SOURCE
    //RMMapboxSource *driveMapSrc = [[RMMapboxSource alloc] initWithMapID: @"xyyp.11781ddc"];
    //RMDBMapSource *driveDBMapSrc = [[RMDBMapSource alloc] initWithPath: [DBTransactionManager getDBPath]];
    
    //RMMapView *driveMap = [[RMMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250) andTilesource:driveDBMapSrc centerCoordinate: CLLocationCoordinate2DMake(50.206, 19.164) zoomLevel: 12.0f maxZoomLevel: 17.0f minZoomLevel: 7.0f backgroundImage: [UIImage imageNamed:@"clear-bg"]];
    
    NoTilesMapSource *offlineSource = [[NoTilesMapSource alloc] initWithTileSetResource:@"offline-maps" ofType:@"db"];
    RMMapView *driveMap = [[RMMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 500) andTilesource:offlineSource];
    
    //RMMapView *driveMap = [[RMMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250) andTilesource:driveMapSrc];
    
    UIView *clearView = [[UIView alloc] init];
    clearView.backgroundColor = [UIColor clearColor];
    
    driveMap.minZoom = 1;
    driveMap.maxZoom = 15;
    driveMap.zoom = 14.0f;
    driveMap.backgroundColor = [UIColor clearColor];
    driveMap.backgroundView = clearView;
    driveMap.delegate = self;
    //driveMap.draggingEnabled = NO;
    driveMap.clusteringEnabled = YES;
    [driveMap setUserTrackingMode: RMUserTrackingModeFollow];
    
    DriveStyledPath *driveStyledPath = [[DriveStyledPath alloc] initWithFrame: driveMap.frame];
    
    driveExp.userInteractionEnabled = NO;
    driveDistance.userInteractionEnabled = NO;
    driveTime.userInteractionEnabled = NO;
    driveSpeed.userInteractionEnabled = NO;
    driveMap.userInteractionEnabled = NO;
    driveStyledPath.userInteractionEnabled = NO;
    waveRecordIndicator.userInteractionEnabled = NO;
    
    _driveExp = driveExp;
    _driveDist = driveDistance;
    _driveTime = driveTime;
    _driveSpeed = driveSpeed;
    _waveRecordIndicator = waveRecordIndicator;
    _stopButton = stopButton;
    _confirmStopButton = confirmStopButton;
    _declineStopButton = declineStopButton;
    _driveMap = driveMap;
    _driveStyledPath = driveStyledPath;
    
    [self.view addSubview: driveMap];
    [self.view addSubview: driveStyledPath];
    [self.view addSubview: driveExp];
    [self.view addSubview: driveDistance];
    [self.view addSubview: driveTime];
    [self.view addSubview: driveSpeed];
    [self.view addSubview: waveRecordIndicator];
    [self.view addSubview: stopButton];
    [self.view addSubview: confirmStopButton];
    [self.view addSubview: declineStopButton];
    [self.view addSubview: stopButton];
    
    [self alignLabelsToRightEdge];
    [stopButton addTarget:self action:@selector(stopButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    return;
    // design guide
    
    UIImageView *guideImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DF-Drive-D1"]];
    [self.view insertSubview:guideImage atIndex:0];
    guideImage.alpha = .3;
}


- (void) animationInOfWaveRecordProcess {
    _recordWaveProcessState = DriveRecordWaveProcessStateAnimationIn;
    [self animationInOfRecordWaveProcessTick: nil];
}

- (void) recordingWaveRecordProcess {
    _recordWaveProcessState = DriveRecordWaveProcessStateRecording;
    
    [self startOfRecordWaveProcessSound];
    [self recOfRecordWaveProcessTick: nil];
    [_driveWaveController startWaveRecording];
}

- (void) animationOutOfWaveRecordProcess {
    _recordWaveProcessState = DriveRecordWaveProcessStateAnimationOut;
    [self animationOutOfRecordWaveProcessTick: nil];
}

- (void) finishRecordingWaveProcess {
    _recordWaveProcessState = DriveRecordWaveProcessStateDefault;
    
    _waveRecordIndicator.progress = 0.0f;
}

- (void) alignLabelsToRightEdge {
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    _driveExp.frame = CGRectMake(screenFrame.size.width - _driveExp.frame.size.width - 16, _driveExp.frame.origin.y, _driveExp.frame.size.width, _driveExp.frame.size.height);
    
    _driveDist.frame = CGRectMake(screenFrame.size.width - _driveDist.frame.size.width - 18, _driveDist.frame.origin.y, _driveDist.frame.size.width, _driveDist.frame.size.height);
    
    _driveTime.frame = CGRectMake(screenFrame.size.width - _driveTime.frame.size.width - 18, _driveTime.frame.origin.y, _driveTime.frame.size.width, _driveTime.frame.size.height);
    
    _driveSpeed.frame = CGRectMake(screenFrame.size.width - _driveSpeed.frame.size.width - 18, _driveSpeed.frame.origin.y, _driveSpeed.frame.size.width, _driveSpeed.frame.size.height);
}

- (void) redrawPath {
    DriveTrackpointCollection *dtc = [DBTransactionManager getDriveTrackpointByRegion:[_driveMap latitudeLongitudeBoundingBox] interval:1 andDriveId: _driveRouteController.driveData.driveId andTranslateToMap: _driveMap];
    
    [_driveStyledPath redrawWithCollection: dtc];
}

#pragma mark - Animation In/Out

- (void) animationIn {
    [super animationIn];
}

- (void) animationInDone {
    [self interfaceUpdateTick:nil];
}

- (void) animationOut {
    [super animationOut];
}

- (void) animationOutDone {
    [self switchToDriveSummaryViewController];
}

-(void) interfaceUpdateTick: (NSTimer *) timer {
    DriveData *dd = _driveRouteController.driveData;
    
    int duration = [dd getTimeSinceDriveStart];
    int distance = dd.distance;
    int experience = [dd getDistanceExperience];
    
    [_driveTime setNumericValue: duration];
    [_driveTime applyFormatValue];
    [_driveTime sizeMe];
    
    [_driveDist setNumericValue: [self.appDataManager convertDistanceToUserUnit: distance]];
    [_driveDist applyFormatValue];
    [_driveDist sizeMe];
    
    [_driveExp setNumericValue: experience];
    [_driveExp displayValue:nil];
    [_driveExp applyFormatValue];
    [_driveExp sizeMe];
    
    [_driveSpeed setNumericValue: [self.appDataManager convertSpeedToUserUnit: _driveRouteController.currentSpeed]];
    [_driveSpeed applyFormatValue];
    [_driveSpeed sizeMe];
    
    [self alignLabelsToRightEdge];
    
    _interfaceUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                target:self
                                selector:@selector(interfaceUpdateTick:)
                                userInfo:nil
                                repeats:NO];
    
    
}

-(void) animationInOfRecordWaveProcessTick: (NSTimer *) timer {
    if(_recordWaveProcessState != DriveRecordWaveProcessStateAnimationIn) {
        return;
    }
    
    if(timer == nil) {
        _recordWaveProcessAnimationInStartTime = [NSDate date];
        _recordWaveProcessAnimationInStartValue = _driveExp.alpha;
    }
    
    float timeDifference = [[NSDate date] timeIntervalSinceDate: _recordWaveProcessAnimationInStartTime];
    
    float duration = _recordWaveProcessAnimationInStartValue * _recordWaveProcessAnimationInDuration;
    float progress = timeDifference / duration;

    if(timeDifference >= duration) {
        _driveExp.alpha = _driveDist.alpha = _driveTime.alpha = _driveSpeed.alpha = _stopButton.alpha =_confirmStopButton.alpha = _declineStopButton.alpha = _driveStyledPath.alpha = 0;
        _waveRecordIndicator.alpha = 1.0f;
        
        [self recordingWaveRecordProcess];
        
        return;
    }
    
    _driveExp.alpha = _driveDist.alpha = _driveTime.alpha = _driveSpeed.alpha = _stopButton.alpha =_confirmStopButton.alpha = _declineStopButton.alpha = _driveStyledPath.alpha = (1 - progress) * _recordWaveProcessAnimationInStartValue;
    _waveRecordIndicator.alpha = 1 - _driveExp.alpha;
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(animationInOfRecordWaveProcessTick:)
                                   userInfo:nil
                                    repeats:NO];
}

-(void) animationOutOfRecordWaveProcessTick: (NSTimer *) timer {
    if(_recordWaveProcessState != DriveRecordWaveProcessStateAnimationOut) {
        return;
    }
    
    if(timer == nil) {
        _recordWaveProcessAnimationOutStartTime = [NSDate date];
        _recordWaveProcessAnimationOutStartValue = _driveExp.alpha;
    }
    
    float timeDifference = [[NSDate date] timeIntervalSinceDate: _recordWaveProcessAnimationOutStartTime];
    
    float duration = (1 - _recordWaveProcessAnimationOutStartValue) * _recordWaveProcessAnimationOutDuration;
    float progress = timeDifference / duration;
    
    if(timeDifference >= duration) {
        _driveExp.alpha = _driveDist.alpha = _driveTime.alpha = _driveSpeed.alpha = _stopButton.alpha =_confirmStopButton.alpha = _declineStopButton.alpha = _driveStyledPath.alpha = 1;
        
        _waveRecordIndicator.alpha = 0;
        
        [self finishRecordingWaveProcess];
        
        return;
    }
    
    _driveExp.alpha = _driveDist.alpha = _driveTime.alpha = _driveSpeed.alpha = _stopButton.alpha =_confirmStopButton.alpha = _declineStopButton.alpha = _driveStyledPath.alpha = _recordWaveProcessAnimationOutStartValue + progress * (1 - _recordWaveProcessAnimationOutStartValue);
    
    _waveRecordIndicator.alpha = 1 - _driveExp.alpha;
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(animationOutOfRecordWaveProcessTick:)
                                   userInfo:nil
                                    repeats:NO];
}

-(void) recOfRecordWaveProcessTick: (NSTimer *) timer {
    if(_recordWaveProcessState != DriveRecordWaveProcessStateRecording) {
        return;
    }
    
    if(timer == nil) {
        _recordWaveProcessRecStartTime = [NSDate date];
        _recordWaveProcessRecStartValue = _driveExp.alpha;
    }
    
    float timeDifference = [[NSDate date] timeIntervalSinceDate: _recordWaveProcessRecStartTime];
    
    float duration = _recordWaveProcessRecDuration;
    float progress = timeDifference / duration;
    
    if(timeDifference >= duration) {
        _waveRecordIndicator.progress = 1.0f;
        
        [self animationOutOfWaveRecordProcess];
        [self finishOfRecordWaveProcessSound];
        [_driveWaveController stopWaveRecording];
        return;
    }
    
    _waveRecordIndicator.progress = progress;
    
    [NSTimer scheduledTimerWithTimeInterval:1/Animation_framesPerSecond
                                     target:self
                                   selector:@selector(recOfRecordWaveProcessTick:)
                                   userInfo:nil
                                    repeats:NO];
}


#pragma mark - Sound

- (void) playWelcomeSound {
    UserData *ud = self.appDataManager.userData;
    NSString *sequenceToPlay = [NSString stringWithFormat:@"vd-hello vd-firstname-%@ vd-wishgooddrive", [ud.firstname lowercaseString]];
    
    [[SoundManager soundManager] playTheSentence: sequenceToPlay];
}

- (void) playRouteSummarySound {
    NSString *sequenceToPlay = [NSString stringWithFormat:@"vd-exp-earned @no%d units-points @pause1 vd-distance %@ units-km vd-time %@", 15, [SoundTools distanceToLiteral: 756], [SoundTools timestampToLiteralHHMMSS: 123456]];
    
    [[SoundManager soundManager] playTheSentence: sequenceToPlay];
}

- (void) playSnapshotSetSound {
    
}

- (void) playSnapshotHitSound {
    
}

- (void) playSpecialStageSetStartSound {
    
}

- (void) playSpecialStageSetFinishSound {
    
}

- (void) playSpecialStageStartSound {
    
}

- (void) playSpecialStageFinishSound {
    
}

- (void) startOfRecordWaveProcessSound {
    NSString *sequenceToPlay = [NSString stringWithFormat:@"vd-wav-rec-start"];
    
    [[SoundManager soundManager] playTheSentence: sequenceToPlay];
}

- (void) finishOfRecordWaveProcessSound {
    NSString *sequenceToPlay = [NSString stringWithFormat:@"vd-wav-rec-finish"];
    
    [[SoundManager soundManager] playTheSentence: sequenceToPlay];
}


#pragma mark - Navigation

- (void) switchToDriveSummaryViewController {
    UINavigationController *nc = self.navigationController;
    
    DriveSummaryViewController *dsvc = [[DriveSummaryViewController alloc] init];
    dsvc.driveRouteController = _driveRouteController;
    
    [nc pushViewController:dsvc animated:NO];
}

@end
