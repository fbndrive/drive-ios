#import "BasicViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "RMMapViewDelegate.h"
#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    DriveRecordWaveProcessStateDefault = 1,
    DriveRecordWaveProcessStateAnimationIn = 2,
    DriveRecordWaveProcessStateRecording = 3,
    DriveRecordWaveProcessStateAnimationOut = 4
} DriveRecordWaveProcessState;

@interface DriveViewController : BasicViewController <CLLocationManagerDelegate, RMMapViewDelegate>

@end
