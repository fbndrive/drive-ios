#import "ASIHTTPRequestDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

@interface DriveWaveController : NSObject <AVAudioRecorderDelegate, AVAudioPlayerDelegate, ASIHTTPRequestDelegate>

@property (nonatomic, strong) AVAudioRecorder *recorder;
@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic) BOOL recording;

- (void) startWaveRecording;
- (void) stopWaveRecording;

@end
