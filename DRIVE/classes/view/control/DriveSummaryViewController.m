#import "AppDataManager.h"
#import "BasicButton.h"
#import "Constants.h"
#import "DBTransactionManager.h"
#import "DriveSummaryViewController.h"
#import "DriveSummaryCountView.h"

@interface DriveSummaryViewController ()

@property (strong, nonatomic) DriveSummaryCountView *countView;
@property (strong, nonatomic) BasicButton *seeDriveStatsButton;
@property (strong, nonatomic) BasicButton *goToDashboardButton;

@end

@implementation DriveSummaryViewController

@synthesize driveRouteController = _driveRouteController;

- (void)viewDidLoad {
    self.appDataManager = [AppDataManager appDataManager];
    
    [super viewDidLoad];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(animationComplete:) name:Animation_complete object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self];
}

#pragma mark - Event Handlers

- (void) seeDriveStatsButtonClick: (id) target {
}

- (void) goToDashboardButtonClick: (id) target {
    [self switchToDriveDashboardViewController];
}

- (void)animationComplete:(NSNotification *) notification {
    
    if (notification.object == _countView) {
        _seeDriveStatsButton.hidden = NO;
        [_seeDriveStatsButton animateIn];
        
        _goToDashboardButton.hidden = NO;
        [_goToDashboardButton animateIn];
        
        DriveData *dd = _driveRouteController.driveData;
        int userId = self.appDataManager.userData.userId;
        
        /*if(driveId >= 0) {
            //[DBTransactionManager updateUserDriveDataAttributes: dd];
            
            //[DBTransactionManager saveExpComponents:_driveRouteController.experienceComponents forUser:userId andDrive:driveId];
        }*/
        
        [[AppDataManager appDataManager] updateTotalDistance];
        [[AppDataManager appDataManager] updateTotalExperience];
        [[AppDataManager appDataManager] updateUserLevelStats];
    }
}

- (void) drawUI {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    int countViewY = roundf(screenRect.size.height * 0.22f);
    CGRect countViewFrame = CGRectMake(0, countViewY, screenRect.size.width, screenRect.size.height - countViewY);
    
    DriveSummaryCountView *countView = [[DriveSummaryCountView alloc] initWithFrame:countViewFrame];
    
    BasicButton *seeDriveStatsButton = [[BasicButton alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];
    [seeDriveStatsButton setTitle:@"see driver statistics"];
    seeDriveStatsButton.frame = CGRectMake((screenRect.size.width - seeDriveStatsButton.frame.size.width)/2, countViewY + 200, seeDriveStatsButton.frame.size.width, seeDriveStatsButton.frame.size.height);
    seeDriveStatsButton.hidden = YES;
    
    BasicButton *goToDashboardButton = [[BasicButton alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];
    [goToDashboardButton setTitle:@"go to dashboard"];
    goToDashboardButton.frame = CGRectMake((screenRect.size.width - goToDashboardButton.frame.size.width)/2, countViewY + 260, goToDashboardButton.frame.size.width, goToDashboardButton.frame.size.height);
    goToDashboardButton.hidden = YES;
    
    [seeDriveStatsButton addTarget:self action:@selector(seeDriveStatsButtonClick:) forControlEvents:UIControlEventTouchDown];
    [goToDashboardButton addTarget:self action:@selector(goToDashboardButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    [self.view addSubview:countView];
    [self.view addSubview:seeDriveStatsButton];
    [self.view addSubview:goToDashboardButton];
    
    _countView = countView;
    _seeDriveStatsButton = seeDriveStatsButton;
    _goToDashboardButton = goToDashboardButton;
    
    //TODO leak?
    [_countView proceedWithExpereinceArray:[[NSMutableArray alloc] initWithArray: _driveRouteController.experienceComponents]];
    
    return;
    // design guide
    
    UIImageView *guideImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DF-Rewards-D1"]];
    [self.view insertSubview:guideImage atIndex:0];
    guideImage.alpha = .3;
}

#pragma mark - Navigation

- (void) switchToDriveDashboardViewController {
    UINavigationController *nc = self.navigationController;
    
    [nc popToRootViewControllerAnimated:NO];
}

@end
