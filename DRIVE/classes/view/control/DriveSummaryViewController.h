#import "BasicViewController.h"
#import "DriveRouteController.h"

@interface DriveSummaryViewController : BasicViewController

@property (nonatomic,strong) DriveRouteController *driveRouteController;

@end
