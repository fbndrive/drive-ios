#import "BasicViewController.h"
#import <UIKit/UIKit.h>

@interface HistoryViewController : BasicViewController <UITableViewDelegate, UITableViewDataSource>

@end
