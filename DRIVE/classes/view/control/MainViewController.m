#import <AVFoundation/AVFoundation.h>
#import "AppDataManager.h"
#import "AppDelegate.h"
#import "ApplicationBackgroundView.h"
#import "BasicButton.h"
#import "BasicNaviController.h"
#import "Constants.h"
#import "CPAnimationSequence.h"
#import "DriveNaviController.h"
#import "DriveDashboardViewController.h"
#import "FBSDKCoreKit/FBSDKCoreKit.h"
#import "FBSDKLoginKit/FBSDKLoginKit.h"
#import "FileSystemManager.h"
#import "HistoryNaviController.h"
#import "HistoryViewController.h"
#import "MainMenuView.h"
#import "MainViewController.h"
#import "SnaphotBlinkViewController.h"

@interface MainViewController ()

@property (nonatomic,strong) FBSDKLoginManager *loginManager;

@property (nonatomic,strong) AVAudioPlayer *testPlr;

@property (nonatomic,strong) ApplicationBackgroundView *appBgView;
@property (nonatomic,strong) UIImageView *logoImage;

@property (nonatomic,strong) BasicButton *logoutButton;
@property (nonatomic,strong) BasicButton *loginButton;

@property (nonatomic,strong) MainMenuView *mainMenu;

@property (nonatomic,strong) BasicNaviController *naviController;

@property (nonatomic,strong) CPAnimationSequence *openCloseMenuAnimSequence;

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self drawUI];
    [self handleLogout];
    
    _loginManager = [[FBSDKLoginManager alloc] init];
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange: YES];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    //Facebook events
    [center addObserver:self selector:@selector(fbAccessTokenDidChange:) name:FBSDKAccessTokenDidChangeNotification object:nil];
    [center addObserver:self selector:@selector(fbProfileDidChange:) name:FBSDKProfileDidChangeNotification object:nil];
    
    //Main Menu events
    [center addObserver:self selector:@selector(mainmenuItemClicked:) name:MainMenu_itemClicked object:nil];
    [center addObserver:self selector:@selector(mainmenuToggled:) name:MainMenu_toggle object:nil];
    
    //View controllers events
    [center addObserver:self selector:@selector(driveDashboardEntered:) name:DriveNavi_dashboardEntered object:nil];
    [center addObserver:self selector:@selector(driveEntered:) name:DriveNavi_driveEntered object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)handleLogin {
    NSLog(@"handleLogin");
    
    AppDataManager *adm = [AppDataManager appDataManager];
    [adm setupUserFromFBData: [FBSDKProfile currentProfile]];
    
    [FileSystemManager createRequiredFolders];
    
    [NSTimer scheduledTimerWithTimeInterval:.5
                                    target:self
                                    selector:@selector(createDefaultNaviController)
                                    userInfo:nil
                                    repeats:NO];
    
    _mainMenu.hidden = NO;
    _loginButton.hidden = YES;
    
    [self hideLogoImage];
    
    [_appBgView refresh];
    [_mainMenu refresh];
}

- (void)handleLogout {
    AppDataManager *adm = [AppDataManager appDataManager];
    [adm reset];
    
    _mainMenu.hidden = YES;
    _loginButton.hidden = NO;
    
    [_loginButton animateIn];
    [self showLogoImage];
    
    [self removeNaviController];
}

#pragma mark - Events

- (void)loginButtonClick: (id) target {
    [_loginManager logOut];
    [_loginManager logInWithReadPermissions: @[@"public_profile"] fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                    if (error) {
                                        NSLog(@"Process error %@", error.debugDescription);
                                    } else if (result.isCancelled) {
                                        NSLog(@"Cancelled");
                                    } else {
                                        NSLog(@"Logged in");
                                    }
     }];
}

- (void)driveDashboardEntered:(NSNotification *) notification {
    //_mainMenu.hidden = NO;
    
    CPAnimationSequence *anim = [CPAnimationSequence sequenceWithSteps:
                                 [CPAnimationStep for:0.3 animate:^{ _mainMenu.alpha = 1;}], nil];
    
    [anim runAnimated:YES];
}

- (void)driveEntered:(NSNotification *) notification {
    //_mainMenu.hidden = YES;
    
    CPAnimationSequence *anim = [CPAnimationSequence sequenceWithSteps:
                                 [CPAnimationStep for:0.3 animate:^{ _mainMenu.alpha = 0.0;}], nil];
    
    [anim runAnimated:YES];
}

- (void)mainmenuItemClicked:(NSNotification *) notification {
    BasicButton *menuButton = (BasicButton *) notification.object;
    
    if(menuButton) {
        
        if([[menuButton.titleLabel.text lowercaseString] isEqualToString:MainMenu_item_drive]) {
            NSLog(@"switch to %@", MainMenu_item_drive);
            [self createDriveNaviController];
        }
        
        if([[menuButton.titleLabel.text lowercaseString] isEqualToString:MainMenu_item_map]) {
            NSLog(@"switch to %@", MainMenu_item_map);
        }
        
        if([[menuButton.titleLabel.text lowercaseString] isEqualToString:MainMenu_item_history]) {
            NSLog(@"switch to %@", MainMenu_item_history);
            [self createHistoryNaviController];
        }
        
        if([[menuButton.titleLabel.text lowercaseString] isEqualToString:MainMenu_item_thropies]) {
            NSLog(@"switch to %@", MainMenu_item_thropies);
        }
        
        if([[menuButton.titleLabel.text lowercaseString] isEqualToString:MainMenu_item_friends]) {
            NSLog(@"switch to %@", MainMenu_item_friends);
        }
        
        if([[menuButton.titleLabel.text lowercaseString] isEqualToString:MainMenu_item_logout]) {
            NSLog(@"switch to %@", MainMenu_item_logout);
            [_loginManager logOut];
        }
        
    }
    
    [_mainMenu toggleState];
}

- (void)mainmenuToggled:(NSNotification *) notification {
    if(_openCloseMenuAnimSequence) {
        [_openCloseMenuAnimSequence cancel];
        _openCloseMenuAnimSequence = nil;
    }
    
    if(_mainMenu.isOpened) {
        _openCloseMenuAnimSequence = [CPAnimationSequence sequenceWithSteps:
                                      [CPAnimationStep for:0.2 animate:^{ _naviController.view.alpha = 0.0;}],
                                      [CPAnimationStep after: .1 for:0.2 animate:^{ _mainMenu.itemList.alpha = 1.0;}], nil];
    } else {
        _openCloseMenuAnimSequence = [CPAnimationSequence sequenceWithSteps:
                                      [CPAnimationStep for:0.2 animate:^{ _mainMenu.itemList.alpha = 0.0;}],
                                      [CPAnimationStep after: .1 for:0.2 animate:^{ _naviController.view.alpha = 1.0;}], nil];
    }
    
    [_openCloseMenuAnimSequence runAnimated:YES];
}

- (void)fbAccessTokenDidChange:(NSNotification *) notification {
    if([FBSDKAccessToken currentAccessToken]) {
        if([notification.userInfo objectForKey: FBSDKAccessTokenChangeOldKey] != [notification.userInfo objectForKey: FBSDKAccessTokenChangeNewKey]) {
            //[self handleLogin];
        }
    } else {
        [self handleLogout];
    }
}

- (void)fbProfileDidChange:(NSNotification *) notification {
    if([FBSDKProfile currentProfile]) {
        [self handleLogin];
    }
}

#pragma mark - User Interface

- (void)drawUI {
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    //main menu
    MainMenuView *mainMenu = [[MainMenuView alloc] initWithFrame:screenFrame];
    
    //background
    ApplicationBackgroundView *appBgView = [[ApplicationBackgroundView alloc] initWithFrame:screenFrame];
    
    //logo
    UIImageView *logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    
    CGRect logoFrame = logoImage.frame;
    float logoXPos = floorf((screenFrame.size.width - logoFrame.size.width) / 2) - 2;
    
    logoImage.frame = CGRectMake(logoXPos, 86, logoFrame.size.width, logoFrame.size.height);
    
    //login with facebook
    BasicButton *loginButton = [[BasicButton alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, screenFrame.size.height)];
    [loginButton setTitle:@"login with facebook"];
    [loginButton refresh];
    
    CGRect buttonFrame = loginButton.frame;
    float buttonXPos = floorf((screenFrame.size.width - buttonFrame.size.width) / 2) - 2;
    loginButton.frame = CGRectMake(buttonXPos, screenFrame.size.height - 85, buttonFrame.size.width, buttonFrame.size.height);
    
    [loginButton animateIn];
    [loginButton addTarget:self action:@selector(loginButtonClick:) forControlEvents:UIControlEventTouchDown];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:appBgView];
    [self.view addSubview:logoImage]; // 121 173
    [self.view addSubview:loginButton];
    [self.view addSubview:mainMenu];
    
    _logoImage = logoImage;
    _mainMenu = mainMenu;
    _appBgView = appBgView;
    _loginButton = loginButton;
    
    _mainMenu.hidden = YES;
}

/*
- (void)transitionToBg {
    float targetAlpha = _backgroundEcoImage.alpha == 1 ? 0 : 1;
    
    [UIView animateWithDuration:4
                          delay:0
                        options: UIViewAnimationCurveLinear
                     animations:^{
                         _backgroundEcoImage.alpha = targetAlpha;
                     }
                     completion:^(BOOL finished){
                         [self transitionToBg];
                     }];
}
 */

- (void) showLogoImage {
    CGRect frameBackup = _logoImage.frame;
    
    _logoImage.hidden = NO;
    _logoImage.alpha = 0;
    _logoImage.frame = CGRectMake(_logoImage.frame.origin.x, _logoImage.frame.origin.y - _logoImage.frame.size.height/2, _logoImage.frame.size.width, _logoImage.frame.size.height);
    
    [UIView animateWithDuration:.4
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _logoImage.alpha = 1;
                         _logoImage.frame = frameBackup;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

- (void) hideLogoImage {
    CGRect frameBackup = _logoImage.frame;
    
    [UIView animateWithDuration:.4
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _logoImage.alpha = 0;
                         _logoImage.frame = CGRectMake(_logoImage.frame.origin.x, _logoImage.frame.origin.y + _logoImage.frame.size.height/2, _logoImage.frame.size.width, _logoImage.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         _logoImage.hidden = YES;
                         _logoImage.frame = frameBackup;
                     }];
}

- (void) showSnaphotBlink {
    SnaphotBlinkViewController *sbvc = [[SnaphotBlinkViewController alloc] init];
    
    [self.view addSubview:sbvc.view];
    
    NSLog(@"blink");
}

#pragma mark - Navigation

- (void) createDefaultNaviController {
    [self createDriveNaviController];
    //[self createHistoryNaviController];
}

- (void) createDriveNaviController {
    if([_naviController isKindOfClass:[DriveNaviController class]]) {
        return;
    } else {
        [self removeNaviController];
    }
    
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    DriveDashboardViewController *ddvc = [[DriveDashboardViewController alloc] init];
    
    DriveNaviController *dnc = [[DriveNaviController alloc] initWithRootViewController:ddvc];
    [self addChildViewController:dnc];
    
    dnc.navigationBarHidden = YES;
    dnc.view.bounds = screenFrame;
    dnc.view.center = self.view.center;
    
    //[self.view dnc];
    [self.view insertSubview:dnc.view aboveSubview:_appBgView];
    [dnc didMoveToParentViewController:self];
    
    _naviController = dnc;
}

- (void) createHistoryNaviController {
    if([_naviController isKindOfClass:[HistoryNaviController class]]) {
        return;
    } else {
        [self removeNaviController];
    }
    
    CGRect screenFrame =[[UIScreen mainScreen] bounds];
    
    HistoryViewController *hvc = [[HistoryViewController alloc] init];
    
    BasicNaviController *hnc = [[HistoryNaviController alloc] initWithRootViewController:hvc];
    [self addChildViewController:hnc];
    
    hnc.navigationBarHidden = YES;
    hnc.view.bounds = screenFrame;
    hnc.view.center = self.view.center;
    
    //[self.view hnc];
    [self.view insertSubview:hnc.view aboveSubview:_appBgView];
    [hnc didMoveToParentViewController:self];
    
    _naviController = hnc;
}

- (void) removeNaviController {
    if(_naviController) {
        [_naviController remove];
        _naviController = nil;
    }
}


@end
