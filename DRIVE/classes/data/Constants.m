#import "Constants.h"

@implementation Constants

float const My_PI = (float)M_PI;

NSString * const General_application_version = @"0.001";

int const Animation_framesPerSecond = 25;
NSString * const Animation_complete = @"Animation_complete";

int const DriveCharacteristic_ECO = -1;
int const DriveCharacteristic_NORMAL = 0;
int const DriveCharacteristic_PERFORMANCE = 1;

NSString * const Database_version = @"0.035";
NSString * const Database_dateTimezone = @"UTC";
NSString * const Database_dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";

// events

NSString * const MainMenu_itemClicked = @"MainMenu_itemClicked";
NSString * const MainMenu_toggle = @"MainMenu_toggle";
NSString * const MainMenu_item_drive = @"dashboard";
NSString * const MainMenu_item_map = @"world of all drives";
NSString * const MainMenu_item_history = @"you drove this";
NSString * const MainMenu_item_thropies = @"lifetime thropies";
NSString * const MainMenu_item_friends = @"people i respect";
NSString * const MainMenu_item_logout = @"give me a break";

NSString * const Distance_oneKmThresholdHit = @"Distance_oneKmThresholdHit";
NSString * const Distance_fiveKmThresholdHit = @"Distance_fiveKmThresholdHit";
NSString * const Distance_tenKmThresholdHit = @"Distance_tenKmThresholdHit";

NSString * const DriveNavi_dashboardEntered = @"DriveNavi_dashboardEntered";
NSString * const DriveNavi_driveEntered = @"DriveNavi_driveEntered";

NSString * const Style_buttonUppercaseGlow = @"Style_buttonUppercaseGlow";
NSString * const Style_labeledValueRegularLabelGlow = @"Style_labeledValueRegularLabelGlow";
NSString * const Style_labeledValueBoldLabelGlow = @"Style_labeledValueBoldLabelGlow";

// font faces

NSString * const Font_lightFace = @"Oswald-Light";
NSString * const Font_regularFace = @"Oswald-Regular";
NSString * const Font_boldFace = @"Oswald-Bold";

// API keys

//NSString * const API_MapBox_projectID = @"saywow.k8cine2a";
NSString * const API_MapBox_projectID = @"xyyp.11781ddc";

int const Screen_globalMargin = 20;

@end
