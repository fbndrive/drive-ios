#import "RouteSpecialStageData.h"

@interface RouteSpecialStageData ()

//@property (strong, nonatomic) GPXRoot *gpxRoot;
//@property (strong, nonatomic) GPXRoute *gpxRoute;

@end

@implementation RouteSpecialStageData

@synthesize id = _id;
@synthesize startCoordinate = _startCoordinate;
@synthesize finishCoordinate = _finishCoordinate;
@synthesize userId = _userId;

- (id)initWithLocation:(CLLocation *) location {
    self = [super init];
    if (self) {
        _startCoordinate = location.coordinate;
        //_gpxRoot = [GPXRoot rootWithCreator:@"Special Stage"];
        //_gpxRoute = [_gpxRoot newRoute];
        
        [self addLocation:location];
    }
    
    return self;
}

- (void) addLocation:(CLLocation *) location {
    //[_gpxRoute newRoutepointWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
}

- (NSString *) getGPXString {
    //return _gpxRoute.gpx;
    return nil;
}

@end
