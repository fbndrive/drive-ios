#import <Foundation/Foundation.h>

@interface AccelerationDescriptionData : NSObject

@property (nonatomic) float bestResult;
@property (nonatomic, retain) NSArray *results;

- (NSDictionary *) getMeAsDictionary;

@end
