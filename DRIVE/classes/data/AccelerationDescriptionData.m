#import "AccelerationDescriptionData.h"

@implementation AccelerationDescriptionData

@synthesize bestResult = _bestResult;
@synthesize results = _results;

- (NSDictionary *) getMeAsDictionary {
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithFloat: _bestResult ], @"bestResult",
                            _results, @"results",
                            nil];
    
    return dict;
}

@end
