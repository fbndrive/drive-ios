#import <Foundation/Foundation.h>

@interface DriveDescriptionData : NSObject

@property (strong, nonatomic) UIColor *color;
@property (strong, nonatomic) UIColor *colorAlpha;
@property (strong, nonatomic) UIColor *glow;

+(DriveDescriptionData *) performanceSet;
+(DriveDescriptionData *) neutralSet;
+(DriveDescriptionData *) ecoSet;

@end
