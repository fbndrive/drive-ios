#import "Mapbox.h"
#import "DriveTrackpointCollection.h"

@implementation DriveTrackpointCollection

@synthesize trackpoints = _trackpoints;

- (id) init {
    self = [super init];
    
    if(self) {
        _trackpoints = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void) translateToMap: (RMMapView *) mapView {
    int i = -1;
    int q = (int)[_trackpoints count];
    
    DriveTrackpointData *dtd;
    
    while (++i < q) {
        dtd = [_trackpoints objectAtIndex:i];
        dtd.mapPosition = [mapView coordinateToPixel: CLLocationCoordinate2DMake(dtd.latitude, dtd.longitude)];
    }
}

- (NSArray *) optimiseForMapWithZoom: (RMMapView *) mapView {
    //2-16
    
    NSMutableArray *optimised = [[NSMutableArray alloc] init];
    
    int i = 0;
    int q = (int)[_trackpoints count];
    
    CGPoint pt;
    DriveTrackpointData *dtd;
    
    int interval = (int)((1 - (mapView.zoom - mapView.minZoom) / (mapView.maxZoom - mapView.minZoom)) * 50) + 1;
    
    NSLog(@"interval %d", interval);
    
    dtd = [_trackpoints firstObject];
    pt = [mapView coordinateToPixel: CLLocationCoordinate2DMake(dtd.latitude, dtd.longitude)];
    [optimised addObject:[NSValue valueWithCGPoint:pt]];
    
    while (i < q) {
        dtd = [_trackpoints objectAtIndex:i];
        
        pt = [mapView coordinateToPixel: CLLocationCoordinate2DMake(dtd.latitude, dtd.longitude)];
        
        [optimised addObject:[NSValue valueWithCGPoint:pt]];
        
        i += interval;
    }
    
    if(i >= q) {
        dtd = [_trackpoints lastObject];
        pt = [mapView coordinateToPixel: CLLocationCoordinate2DMake(dtd.latitude, dtd.longitude)];
        
        [optimised addObject:[NSValue valueWithCGPoint:pt]];
    }
    
    
    NSLog(@"zoom final count: %lu", (unsigned long)[optimised count]);
    
    
    return [optimised copy];
}

- (NSArray *) optimiseForMap: (RMMapView *) mapView {
    NSMutableArray *optimised = [self douglasPeuckerReduction:_trackpoints withTolerance: 0.005f];
    NSArray *optimisedSorted = [optimised sortedArrayUsingSelector:@selector(compare:)];
    
    NSMutableArray *finalArray = [[NSMutableArray alloc] init];
    
    int i = -1;
    int q = (int)[optimisedSorted count];
    
    CGPoint pt;
    DriveTrackpointData *dtd;
    
    while (++i < q) {
        dtd = _trackpoints[[optimisedSorted[i] intValue]];
        //dtd = _trackpoints[i];
        
        pt = [mapView coordinateToPixel: CLLocationCoordinate2DMake(dtd.latitude, dtd.longitude)];
        
        [finalArray addObject:[NSValue valueWithCGPoint:pt]];
    }
    
    
    NSLog(@"final count: %lu", (unsigned long)[finalArray count]);
    
    
    return [finalArray copy];
}

/*- (void) pushWithTrackpoint: (DriveTrackpointData *) dtd {
    [_trackpoints addObject: dtd];
}*/

- (NSMutableArray *)douglasPeuckerReduction:(NSMutableArray *)points withTolerance:(float)tolerance {
    if (points == nil || [points count] < 3)
        return points;
    
    NSUInteger firstPoint = 0;
    NSUInteger lastPoint = [points count] - 1;
    
    NSMutableArray * pointIndicesToKeep = [[NSMutableArray alloc] init];
    
    //Add the first and last index to the keepers
    [pointIndicesToKeep addObject:[NSNumber numberWithUnsignedInteger:firstPoint]];
    [pointIndicesToKeep addObject:[NSNumber numberWithUnsignedInteger:lastPoint]];
    
    //The first and the last point cannot be the same
    while ([[points objectAtIndex:firstPoint] equalsTo:[points objectAtIndex:lastPoint]])
    {
        lastPoint--;
    }
    
    [self douglasPeuckerReduction:points toNew: pointIndicesToKeep withFirstPoint:firstPoint lastPoint:lastPoint andTolerance:tolerance];
    
    // Sort the points.
    //NSSortDescriptor * sortOptions = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES selector:@selector(compare:)];
    //[pointIndicesToKeep sortUsingDescriptors:[NSArray arrayWithObject:sortOptions]];
    
    return pointIndicesToKeep;
}

- (void)douglasPeuckerReduction:(NSMutableArray *)points toNew: (NSMutableArray *) newArray withFirstPoint:(NSUInteger)firstPoint lastPoint:(NSUInteger)lastPoint andTolerance:(float)tolerance {
    
    if (lastPoint <= firstPoint + 1) {
        // overlapping indexes, just return
        return;
    }
    
    float maxDistance = 0.0f;
    
    
    
    NSUInteger indexFarthest = 0;
    
    
    
    for (NSUInteger index = firstPoint + 1; index < lastPoint; index++) {
        
        
        float distance = [self perpendicularDistanceOf:[points objectAtIndex:index] from:[points objectAtIndex:firstPoint] to:[points objectAtIndex:lastPoint]];
        
        
        if (distance > maxDistance) {
            maxDistance = distance;
            indexFarthest = index;
        }
    }
    
    if (maxDistance > tolerance && indexFarthest != 0) {
        //Add the largest point that exceeds the tolerance
        [newArray addObject:[NSNumber numberWithUnsignedInteger:indexFarthest]];
        
        [self douglasPeuckerReduction:points toNew: newArray withFirstPoint:firstPoint lastPoint:indexFarthest andTolerance:tolerance];
        [self douglasPeuckerReduction:points toNew: newArray withFirstPoint:indexFarthest lastPoint:lastPoint andTolerance:tolerance];
        
        //[pointIndicesToKeep addObjectsFromArray:leftSide];
        //[pointIndicesToKeep addObjectsFromArray:rightSide];
    }
}

- (float)perpendicularDistanceOf:(DriveTrackpointData *)point from:(DriveTrackpointData *)pointA to:(DriveTrackpointData *)pointB {
    //Area = |(1/2)(x1y2 + x2y3 + x3y1 - x2y1 - x3y2 - x1y3)|   *Area of triangle
    //Base = v((x1-x2)²+(x1-x2)²)                               *Base of Triangle*
    //Area = .5*Base*H                                          *Solve for height
    //Height = Area/.5/Base
    
    float area = fabsf(.5f * (
                                   pointA.latitude * pointB.longitude
                                   + pointB.latitude * point.longitude
                                   + point.latitude * pointA.longitude
                                   - pointB.latitude * pointA.longitude
                                   - point.latitude * pointB.longitude
                                   - pointA.latitude * point.longitude));
    
    float bottom = sqrt(pow(pointA.latitude - pointB.latitude, 2.0f) + pow(pointA.longitude - pointB.longitude, 2.0f));
    
    float height = area / bottom * 2.0f;
    
    /*
     double area = fabsl(<#float#>)
    abs(
                (
                 $lineStart->lat * $lineEnd->lng
                 + $lineEnd->lat * $point->lng
                 + $point->lat * $lineStart->lng
                 - $lineEnd->lat * $lineStart->lng
                 - $point->lat * $lineEnd->lng
                 - $lineStart->lat * $point->lng
                 ) / 2
                );
    
    $bottom = sqrt(pow($lineStart->lat - $lineEnd->lat, 2) + pow($lineStart->lng - $lineEnd->lng, 2));
    
    return $area / $bottom * 2.0;*/
    
    return height;
}

@end
