#import <Foundation/Foundation.h>

@interface UserLevelData : NSObject

@property (nonatomic) int level;
@property (nonatomic) int currentThreshold;
@property (nonatomic) int nextTreshold;

+ (UserLevelData *) userLevelDataWithLevel: (int) level currentTreshold: (int) ct andNextTreshold: (int) nt;

@end
