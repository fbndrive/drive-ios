#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface RoutePoint : NSObject

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) RoutePoint *prev;
@property (nonatomic) CLLocationDistance distanceToPrev;
@property (nonatomic, retain) RoutePoint *next;
@property (nonatomic) CLLocationDistance distanceToNext;

@end
