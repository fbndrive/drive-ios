#import "AccelerationTimer.h"
#import "AppDataManager.h"
#import "AppDelegate.h"
#import "Constants.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>
#import "DateTools.h"
#import "DBTransactionManager.h"
#import "DriveRouteController.h"
#import "DriveTrackpointData.h"
#import "ExperienceComponentData.h"
#import "math.h"
#import "NumberTools.h"

#define kDataParseTriggerValue 5.0

@interface DriveRouteController ()

@property (nonatomic) BOOL recording;
@property (nonatomic) BOOL recordingSpecialStage;

@property (nonatomic) int oneKmThreshold;
@property (nonatomic) int fiveKmThreshold;
@property (nonatomic) int tenKmThreshold;

@property (nonatomic) int lastSavedTrackpointId;
@property (nonatomic, strong) NSMutableArray *driveTrackpointsToParse;

//helpers of drive attributes, update every interval to minimise cpu consumption

/*@property (nonatomic) int pDuration;
@property (nonatomic) int pDistance;
@property (nonatomic) int pAvgSpeed;
@property (nonatomic) int pTopSpeed;
@property (nonatomic) float pAcc0_100;
@property (nonatomic) float pAcc80_120;
@property (nonatomic) float pAcc100_0;
@property (nonatomic) float pOverloadBreaking;
@property (nonatomic) float pOverloadAcc;
@property (nonatomic) float pOverloadLeft;
@property (nonatomic) float pOverloadRight;*/

@property (strong, nonatomic) CMMotionManager *coreMotionManager;

@property (strong, nonatomic) RouteSpecialStageData *setupSpecialStageData;

@property (nonatomic) float dataParseCounter;

// TODO debug

@property (nonatomic) float debugSpeed;

@end

@implementation DriveRouteController

//@synthesize driveData = _driveData;
//@synthesize points = _points;
@synthesize driveData = _driveData;
@synthesize currentSpeed = _currentSpeed;
@synthesize currentLocation = _currentLocation;
@synthesize previousLocation = _previousLocation;
@synthesize experienceComponents = _experienceComponents;

- (void) start {
    AppDelegate *myApplication = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _debugSpeed = 0;
    
    _oneKmThreshold = 0;
    _fiveKmThreshold = 0;
    _tenKmThreshold = 0;
    
    _recording = YES;
    _recordingSpecialStage = NO;
    
    _driveData = [DriveData driveDataWithDefaultValues];
    _experienceComponents = [[NSMutableArray alloc] init];
    _previousLocation = nil;
    _setupSpecialStageData = nil;
    _lastSavedTrackpointId = -1;
    _dataParseCounter = 0.0f;
    
    [self clearDriveTrackpoints];
    
    _coreMotionManager = myApplication.coreMotionManager;
    _coreMotionManager.deviceMotionUpdateInterval = .2;
    [_coreMotionManager startDeviceMotionUpdatesUsingReferenceFrame: CMAttitudeReferenceFrameXArbitraryZVertical];
    
    _driveData.driveId = [DBTransactionManager saveDriveDataOnStart:_driveData forUser: _driveData.userId];
}

- (void) pause {
    
}

- (void) stop {
    _oneKmThreshold = 0;
    _fiveKmThreshold = 0;
    _tenKmThreshold = 0;
    
    _recording = NO;
    _recordingSpecialStage = NO;
    
    [self parseDriveTrackpoints];
    [_driveData finish];
    
    [_coreMotionManager stopDeviceMotionUpdates];
    
    ExperienceComponentData *ecd = [[ExperienceComponentData alloc] init];
    ecd.description = @"drive experience";
    ecd.points = [_driveData getDistanceExperience];
    [_experienceComponents addObject:ecd];
    
    [DBTransactionManager updateDriveDataOnFinish:_driveData];
}

- (CLLocation *) addLocation:(CLLocation *) location {
    if(_recording == NO) {
        return nil;
    }
    
    _currentLocation = location;
    double distanceSinceLastLocation = 0.0f;
    
    if(_previousLocation) {
        distanceSinceLastLocation = [_currentLocation distanceFromLocation: _previousLocation];
        //distanceSinceLastLocation *= 10;
    }
    
    // local variables update
    
    //_currentSpeed = _currentLocation.speed;
    _currentSpeed = [self getDebugSpeed];
    
    // TODO tmp acceleration measurement
    
    // drive data update
    
    [_driveData updateDistance: distanceSinceLastLocation];
    [_driveData updateTopSpeed: _currentSpeed];
    [_driveData updateRouteRectWithLocation: location];
    
    [self recordTrackpointWithLocation: location];
    
    // overloads calculations
    
    /*CMDeviceMotion *dm = _coreMotionManager.deviceMotion;
    
    float accelMagnitude = sqrt(dm.userAcceleration.x * dm.userAcceleration.x + dm.userAcceleration.y * dm.userAcceleration.y + dm.userAcceleration.z * dm.userAcceleration.z);
    
    NSLog(@"motion: %.3f:%.3f:%.3f %f", dm.userAcceleration.x, dm.userAcceleration.y, dm.userAcceleration.z, accelMagnitude);*/
    
    // threshold events
    
    _oneKmThreshold += distanceSinceLastLocation;
    _fiveKmThreshold += distanceSinceLastLocation;
    _tenKmThreshold += distanceSinceLastLocation;
    
    if(_setupSpecialStageData) {
        [_setupSpecialStageData addLocation:location];
    }
    
    _dataParseCounter++;
    if(_dataParseCounter > kDataParseTriggerValue) {
        [self parseDriveTrackpoints];
        _dataParseCounter = 0.0f;
    }
    
    [self checkDistanceThresholdsAndDispatchEvents];
    
    _previousLocation = location;
    
    return location;
}

- (void) setupSpecialStage {
    if(_setupSpecialStageData == nil) {
        _setupSpecialStageData = [[RouteSpecialStageData alloc] initWithLocation:_currentLocation];
    } else {
        _setupSpecialStageData = nil;
    }
}

- (void) checkDistanceThresholdsAndDispatchEvents {
    if(_oneKmThreshold >= 1000) {
        _oneKmThreshold = 0;
        
        NSNotification *distanceOneKmThresholdHitNotification = [[NSNotification alloc] initWithName:Distance_oneKmThresholdHit object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotification:distanceOneKmThresholdHitNotification];
    }
    
    if(_fiveKmThreshold >= 5000) {
        _fiveKmThreshold = 0;
        
        NSNotification *distanceFiveKmThresholdHitNotification = [[NSNotification alloc] initWithName:Distance_fiveKmThresholdHit object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotification:distanceFiveKmThresholdHitNotification];
    }
    
    if(_tenKmThreshold >= 10000) {
        _tenKmThreshold = 0;
        
        NSNotification *distanceTenKmThresholdHitNotification = [[NSNotification alloc] initWithName:Distance_tenKmThresholdHit object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotification:distanceTenKmThresholdHitNotification];
    }
}

- (void) recordTrackpointWithLocation: (CLLocation *) location {
    float speed = _currentSpeed;
    
    DriveTrackpointData *dtd = [[DriveTrackpointData alloc] init];
    dtd.driveId = _driveData.driveId;
    dtd.latitude = location.coordinate.latitude;
    dtd.longitude = location.coordinate.longitude;
    //dtd.elevation = location.altitude;
    dtd.elevation = [NumberTools randomNumberFromRange:NSMakeRange(1, 800)];
    dtd.distance = _driveData.distance;
    dtd.course = location.course;
    dtd.speed = speed;
    dtd.createDate = [NSDate date];
    
    [_driveTrackpointsToParse addObject: dtd];
}

- (void) clearDriveTrackpoints {
    if(_driveTrackpointsToParse) {
        _driveTrackpointsToParse = nil;
    }
    
    _driveTrackpointsToParse = [[NSMutableArray alloc] init];
}

- (void) parseDriveTrackpoints {
    NSArray *tpArray = [_driveTrackpointsToParse copy];
    
    int i = -1;
    int q = (int)[tpArray count];
    DriveTrackpointData *dtd;
    
    while (++i < q) {
        dtd = [tpArray objectAtIndex: i];
        
        [_driveData updateWholeDriveAttributesTrackpoint: dtd];
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
            _lastSavedTrackpointId = [DBTransactionManager saveDriveTrackpoints: tpArray andLastSavedTrackpointId: _lastSavedTrackpointId];
        });
    
    [self clearDriveTrackpoints];
}

/*
 
 dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
 dispatch_async(queue, ^{
                    [NSThread sleepForTimeInterval:5];
 
 
 
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.status.text = @"Done";
                    });
 });
 
 */


// debug methods

- (void) adjustDebugAcc: (float) acc {
    if(acc > 0) {
        _debugSpeed -= .2;
    } else {
        _debugSpeed += .2;
    }
    
    _debugSpeed = fmaxf(0.0f, _debugSpeed);
}

- (float) getDebugSpeed {
    return _debugSpeed;
}

@end
