#import "DriveTrackpointData.h"
#import <Foundation/Foundation.h>
#import "Mapbox.h"

@interface DriveTrackpointCollection : NSObject

@property (nonatomic, retain) NSArray *trackpoints;

- (void) translateToMap: (RMMapView *) mapView;
- (NSArray *) optimiseForMapWithZoom: (RMMapView *) mapView;
- (NSArray *) optimiseForMap: (RMMapView *) mapView;
//- (void) pushWithTrackpoint: (DriveTrackpointData *) dtd;

@end
