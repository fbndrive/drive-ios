#import "UserLevelData.h"

@implementation UserLevelData

@synthesize level = _level;
@synthesize currentThreshold = _currentThreshold;
@synthesize nextTreshold = _nextTreshold;

+ (UserLevelData *) userLevelDataWithLevel: (int) level currentTreshold: (int) ct andNextTreshold: (int) nt {
    UserLevelData *uld = [[UserLevelData alloc] init];
    
    [uld setLevel:level];
    [uld setCurrentThreshold:ct];
    [uld setNextTreshold:nt];
    
    return uld;
}

@end
