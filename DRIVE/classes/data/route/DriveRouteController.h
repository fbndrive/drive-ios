#import <CoreLocation/CoreLocation.h>
#import "DriveData.h"
#import "ExperienceComponentData.h"
#import <Foundation/Foundation.h>
#import "RouteSnapshotData.h"
#import "RouteSpecialStageData.h"

@interface DriveRouteController : NSObject

@property (nonatomic, strong) DriveData *driveData;
@property (nonatomic, strong) CLLocation *previousLocation;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) NSMutableArray *experienceComponents;
@property (nonatomic) float currentSpeed;

- (void) start;
- (void) pause;
- (void) stop;

- (CLLocation *) addLocation:(CLLocation *) location;
- (void) setupSpecialStage;

- (void) adjustDebugAcc: (float) acc;

@end
