#import "DriveTrackpointData.h"

@implementation DriveTrackpointData

- (BOOL) equalsTo:(DriveTrackpointData *) tp {
    if (tp == nil)
        return NO;
    
    return (tp.latitude == self.latitude && tp.longitude == self.longitude);
}

@end
