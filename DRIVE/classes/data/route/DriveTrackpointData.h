#import <Foundation/Foundation.h>

@interface DriveTrackpointData : NSObject

@property (nonatomic) int driveId;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) float elevation;
@property (nonatomic) float distance;
@property (nonatomic) float course;
@property (nonatomic) float speed;
@property (nonatomic) int parentTrackpoint;
@property (nonatomic, retain) NSDate *createDate;

@property (nonatomic) CGPoint mapPosition;

- (BOOL) equalsTo:(DriveTrackpointData *) tp;

@end
