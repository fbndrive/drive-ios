#import "RoutePoint.h"

@implementation RoutePoint

@synthesize coordinate;
@synthesize prev = _prev;
@synthesize distanceToPrev;
@synthesize next = _next;
@synthesize distanceToNext;

- (void) setNext:(RoutePoint *)next {
    _next = next;
}

- (void) setPrev:(RoutePoint *)prev {
    _prev = prev;
}

@end
