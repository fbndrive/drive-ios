#import <Foundation/Foundation.h>

@interface Constants : NSObject

extern float const My_PI;

extern NSString * const General_application_version;

extern int const Animation_framesPerSecond;
extern NSString * const Animation_complete;

extern NSString * const Database_version;
extern NSString * const Database_dateTimezone;
extern NSString * const Database_dateFormat;

extern int const DriveCharacteristic_ECO;
extern int const DriveCharacteristic_NORMAL;
extern int const DriveCharacteristic_PERFORMANCE;

extern NSString * const MainMenu_itemClicked;
extern NSString * const MainMenu_toggle;
extern NSString * const MainMenu_item_drive;
extern NSString * const MainMenu_item_map;
extern NSString * const MainMenu_item_history;
extern NSString * const MainMenu_item_thropies;
extern NSString * const MainMenu_item_friends;
extern NSString * const MainMenu_item_logout;
//extern NSString * const MainMenu_opened;
//extern NSString * const MainMenu_closed;

extern NSString * const DriveNavi_dashboardEntered;
extern NSString * const DriveNavi_driveEntered;

extern NSString * const Distance_oneKmThresholdHit;
extern NSString * const Distance_fiveKmThresholdHit;
extern NSString * const Distance_tenKmThresholdHit;

extern NSString * const Style_buttonUppercaseGlow;
extern NSString * const Style_labeledValueRegularLabelGlow;
extern NSString * const Style_labeledValueBoldLabelGlow;

extern NSString * const Font_lightFace;
extern NSString * const Font_regularFace;
extern NSString * const Font_boldFace;

extern NSString * const API_MapBox_projectID;

extern int const Screen_globalMargin;

@end
