#import "DriveDescriptionData.h"

@implementation DriveDescriptionData

@synthesize color;
@synthesize colorAlpha;
@synthesize glow;

+(DriveDescriptionData *) performanceSet {
    DriveDescriptionData *ddd = [[DriveDescriptionData alloc] init];
    //ddd.color = [UIColor colorWithRed:240.0f/255.0f green:0.0f/255.0f blue:62.0f/255.0f alpha:1.0f];
    ddd.color = [UIColor colorWithHue:.96f saturation:1 brightness:1.0f alpha:1.0f];
    ddd.colorAlpha = [UIColor colorWithRed:240.0f/255.0f green:0.0f/255.0f blue:62.0f/255.0f alpha:0.5f];
    ddd.glow = [UIColor colorWithRed:255.0f/255.0f green:95.0f/255.0f blue:136.0f/255.0f alpha:1.0f];
    
    return ddd;
}

+(DriveDescriptionData *) neutralSet {
    DriveDescriptionData *ddd = [[DriveDescriptionData alloc] init];
    //ddd.color = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    ddd.color = [UIColor colorWithHue:.96f saturation:0 brightness:.95f alpha:1.0f];
    ddd.colorAlpha = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:0.5f];
    ddd.glow = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    
    return ddd;
}

+(DriveDescriptionData *) ecoSet {
    DriveDescriptionData *ddd = [[DriveDescriptionData alloc] init];
    //ddd.color = [UIColor colorWithRed:120.0f/255.0f green:237.0f/255.0f blue:108.0f/255.0f alpha:1.0f];
    ddd.color = [UIColor colorWithHue:.32f saturation:.54f brightness:.95f alpha:1.0f];
    ddd.colorAlpha = [UIColor colorWithRed:120.0f/255.0f green:237.0f/255.0f blue:108.0f/255.0f alpha:0.5f];
    ddd.glow = [UIColor colorWithRed:120.0f/255.0f green:237.0f/255.0f blue:108.0f/255.0f alpha:1.0f];
    
    return ddd;
}

@end
