#import "UserData.h"

@implementation UserData

@synthesize userId = _userId;
@synthesize fbId = _fbId;
@synthesize firstname = _firstname;
@synthesize surname = _surname;
@synthesize createdDate = _createdDate;

@end
