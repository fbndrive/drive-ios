#import <CoreLocation/CoreLocation.h>
#import "DriveTrackpointData.h"
#import <Foundation/Foundation.h>

@interface DriveData : NSObject

// drive attributes

extern NSString * const DriveDataAttributeComment;
extern NSString * const DriveDataAttributeExperienceComponents;
extern NSString * const DriveDataAttributeAvgSpeed;
extern NSString * const DriveDataAttributeTopSpeed;
extern NSString * const DriveDataAttributeAcc0_100;
extern NSString * const DriveDataAttributeAcc80_120;
extern NSString * const DriveDataAttributeAcc100_0;
extern NSString * const DriveDataAttributeOverloadBreaking;
extern NSString * const DriveDataAttributeOverloadAcc;
extern NSString * const DriveDataAttributeOverloadLeft;
extern NSString * const DriveDataAttributeOverloadRight;
extern NSString * const DriveDataAttributeDirection;

@property (nonatomic) int driveId;
@property (nonatomic, retain) NSString *gpx;
@property (nonatomic, retain) NSDictionary *attributes;
@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *finishDate;
@property (nonatomic) int distance;
@property (nonatomic) int experience;
@property (nonatomic) int finished;
@property (nonatomic) int parsed;
@property (nonatomic) int userId;
@property (nonatomic) int origin;
@property (nonatomic, retain) NSDate *createdDate;

#pragma mark - GPX data

@property (nonatomic) CLLocationCoordinate2D startLocation;
@property (nonatomic) CLLocationCoordinate2D finishLocation;
@property (nonatomic) CLLocationCoordinate2D southWest;
@property (nonatomic) CLLocationCoordinate2D northEast;
@property (nonatomic) CGRect routeRect;
@property (nonatomic, retain) NSArray* routePath;

+ (DriveData *) driveDataWithDefaultValues;

- (void) finish;
- (void) updateWholeDriveAttributesTrackpoint: (DriveTrackpointData *) trackpoint;
- (int) updateDistance: (int) distance;
- (int) updateTopSpeed: (int) speed;
- (float) updateAvgSpeed;
- (void) updateRouteRectWithLocation: (CLLocation *) location;

- (float) getHighestElevation;
- (NSArray *) getElevationDataSet;
- (float) getBestAccelerationByKey:(NSString *) key;
- (int) getTimeSinceDriveStart;
- (int) getExperience;
- (int) getDistanceExperience;

@end
