#import <Foundation/Foundation.h>

@interface UserData : NSObject

@property (nonatomic) int userId;
@property (nonatomic, retain) NSString *fbId;
@property (nonatomic, retain) NSString *firstname;
@property (nonatomic, retain) NSString *surname;
@property (nonatomic) int origin;
@property (nonatomic, strong) NSDate *createdDate;

@end
