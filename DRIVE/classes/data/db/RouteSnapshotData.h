#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface RouteSnapshotData : NSObject

@property (nonatomic) int id;
@property (nonatomic) float speed;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) int userId;

@end
