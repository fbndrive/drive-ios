#import "ExperienceComponentData.h"

@implementation ExperienceComponentData

@synthesize description = _description;
@synthesize points = _points;

- (id) init {
    self = [super init];
    
    if(self) {
        _points = 0;
    }
    
    return self;
}

@end
