#import "RouteSnapshotData.h"

@implementation RouteSnapshotData

@synthesize id = _id;
@synthesize speed = _speed;
@synthesize coordinate = _coordinate;
@synthesize userId = _userId;

@end
