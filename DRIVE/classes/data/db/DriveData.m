#import "AccelerationTimer.h"
#import "AccelerationDescriptionData.h"
#import "AppDataManager.h"
#import "DateTools.h"
#import "DriveData.h"
#import "DriveTrackpointData.h"
#import "ElevationGraphCreator.h"
#import "ElevationChartVertex.h"

@interface DriveData ()

@property (nonatomic, retain) AccelerationTimer *accTimer0_100;
@property (nonatomic, retain) AccelerationTimer *accTimer80_120;
@property (nonatomic, retain) ElevationGraphCreator *elevationCreator;

@end

@implementation DriveData

NSString * const DriveDataAttributeComment = @"comment";
NSString * const DriveDataAttributeExperienceComponents = @"experienceComponents";
NSString * const DriveDataAttributeAvgSpeed = @"avgSpeed"; // speed is kept as m/s
NSString * const DriveDataAttributeTopSpeed = @"topSpeed"; // speed is kept as m/s
NSString * const DriveDataAttributeHighestElevation = @"highestElevation";
NSString * const DriveDataAttributeAcc0_100 = @"acc0_100";
NSString * const DriveDataAttributeAcc80_120 = @"acc80_120";
NSString * const DriveDataAttributeAcc100_0 = @"acc100_0";
NSString * const DriveDataAttributeElevation = @"elevation";
NSString * const DriveDataAttributeOverloadBreaking = @"overloadBreaking";
NSString * const DriveDataAttributeOverloadAcc = @"overloadAcc";
NSString * const DriveDataAttributeOverloadLeft = @"overloadLeft";
NSString * const DriveDataAttributeOverloadRight = @"overloadRight";
NSString * const DriveDataAttributeDirection = @"direction";

@synthesize driveId = _driveId;
@synthesize gpx = _gpx;
@synthesize startDate = _startDate;
@synthesize finishDate = _finishDate;
@synthesize distance = _distance;
@synthesize experience = _experience;
@synthesize finished = _finished;
@synthesize parsed = _parsed;
@synthesize userId = _userId;
@synthesize origin = _origin;
@synthesize createdDate = _createdDate;

#pragma mark - GPX data

@synthesize startLocation = _startLocation;
@synthesize finishLocation = _finishLocation;
@synthesize southWest = _southWest;
@synthesize northEast = _northEast;
@synthesize routePath = _routePath;
@synthesize routeRect = _routeRect;

+ (DriveData *) driveDataWithDefaultValues {
    AppDataManager *adm = [AppDataManager appDataManager];
    DriveData *dd = [[DriveData alloc] init];
    
    dd.driveId = -1;
    dd.gpx = @"";
    dd.attributes = [[NSMutableDictionary alloc] init];
    dd.startDate = [NSDate date];
    dd.finishDate = nil;
    dd.distance = 0;
    dd.experience = 0;
    dd.finished = 0;
    dd.parsed = 0;
    dd.userId = adm.userData.userId;
    dd.origin = 1;
    dd.createdDate = [NSDate date];
    dd.southWest = CLLocationCoordinate2DMake(1000.f, 1000.f);
    dd.northEast = CLLocationCoordinate2DMake(-1000.f, -1000.f);
    
    AccelerationDescriptionData * add = [[AccelerationDescriptionData alloc] init];
    add.bestResult = 0.0f;
    add.results = [[NSArray alloc] init];
    
    [dd.attributes setValue:@"" forKey:DriveDataAttributeComment];
    [dd.attributes setValue:[[NSMutableArray alloc] init] forKey:DriveDataAttributeExperienceComponents];
    [dd.attributes setValue:[NSNumber numberWithInt: 0] forKey:DriveDataAttributeAvgSpeed];
    [dd.attributes setValue:[NSNumber numberWithInt: 0] forKey:DriveDataAttributeTopSpeed];
    [dd.attributes setValue:[NSNumber numberWithInt: 0] forKey:DriveDataAttributeHighestElevation];
    [dd.attributes setValue:[NSNumber numberWithFloat: 0.0f] forKey:DriveDataAttributeAcc0_100];
    [dd.attributes setValue:[add getMeAsDictionary] forKey:DriveDataAttributeAcc80_120];
    [dd.attributes setValue:[NSNumber numberWithFloat: 0.0f] forKey:DriveDataAttributeAcc100_0];
    [dd.attributes setValue:[[NSMutableArray alloc] init] forKey:DriveDataAttributeElevation];
    [dd.attributes setValue:[NSNumber numberWithFloat: 0.0f] forKey:DriveDataAttributeOverloadAcc];
    [dd.attributes setValue:[NSNumber numberWithFloat: 0.0f] forKey:DriveDataAttributeOverloadBreaking];
    [dd.attributes setValue:[NSNumber numberWithFloat: 0.0f] forKey:DriveDataAttributeOverloadLeft];
    [dd.attributes setValue:[NSNumber numberWithFloat: 0.0f] forKey:DriveDataAttributeOverloadRight];
    [dd.attributes setValue:[NSNumber numberWithInt: 0] forKey:DriveDataAttributeDirection];
    
    return dd;
}

- (id) init {
    self = [super init];
    
    if(self) {
        _accTimer0_100 = nil;
        _accTimer80_120 = nil;
        _elevationCreator = nil;
    }
    
    return self;
}

- (void) finish {
    _finishDate = [NSDate date];
    _experience = [self getExperience];
    _finished = 1;
    [self updateAvgSpeed];
    
    [self.attributes setValue:[_accTimer0_100 getResults]  forKey:DriveDataAttributeAcc0_100];
    [self.attributes setValue:[_accTimer80_120 getResults]  forKey:DriveDataAttributeAcc80_120];
    [self.attributes setValue:[NSNumber numberWithFloat: _elevationCreator.highest] forKey:DriveDataAttributeHighestElevation];
    [self.attributes setValue:[_elevationCreator getElevationVertexArray]  forKey:DriveDataAttributeElevation];
}

- (float) getBestAccelerationByKey:(NSString *) key {
    NSArray *accs = [_attributes valueForKey: key];
    
    float best = 100;
    
    int i = -1;
    int q = (int) [accs count];
    
    while(++i < q) {
        if(best > [[[accs objectAtIndex: i] valueForKey: @"value"] floatValue]) {
            best = [[[accs objectAtIndex: i] valueForKey: @"value"] floatValue];
        }
    }
    
    return best;
}

- (float) getDirection {
    return [[_attributes valueForKey: DriveDataAttributeDirection] floatValue];
}

- (float) getHighestElevation {
    return [[_attributes valueForKey: DriveDataAttributeHighestElevation] floatValue];
}

- (NSArray *) getElevationDataSet {
    NSArray *elevations = [_attributes valueForKey: DriveDataAttributeElevation];
    
    NSMutableArray *vertexes = [[NSMutableArray alloc] init];
    ElevationChartVertex *ecv;
    
    int i = -1;
    int q = (int) [elevations count];
    
    while(++i < q) {
        ecv = [ElevationChartVertex initWithElevation:[[elevations[i] valueForKey: @"elevation"] floatValue] andDistance:[[elevations[i] valueForKey: @"distance"] floatValue]];
        
        [vertexes addObject: ecv];
    }
    
    return vertexes;
}

- (void) updateWholeDriveAttributesTrackpoint: (DriveTrackpointData *) trackpoint {
    if(_accTimer0_100 == nil) {
        _accTimer0_100 = [[AccelerationTimer alloc] initWithFrom: 2.0f AndTo: 27.7];
    }
    
    if(_accTimer80_120 == nil) {
        _accTimer80_120 = [[AccelerationTimer alloc] initWithFrom: 22.2f AndTo: 33.3];
    }
    
    if(_elevationCreator == nil) {
        _elevationCreator = [[ElevationGraphCreator alloc] init];
    }
    
    [_accTimer0_100 tickTimerWithSpeed: trackpoint.speed AndTime: trackpoint.createDate];
    [_accTimer80_120 tickTimerWithSpeed: trackpoint.speed AndTime: trackpoint.createDate];
    
    [_elevationCreator pushWithElevation: trackpoint.elevation atDistance: trackpoint.distance];
}

- (int) updateDistance: (int) distance {
    _distance += distance;
    
    return _distance;
}

- (int) updateTopSpeed: (int) speed {
    int topSpeed = 0;
    
    NSNumber *currentTopSpeed = [self.attributes valueForKey: DriveDataAttributeTopSpeed];
    
    if(currentTopSpeed != nil) {
        if([currentTopSpeed intValue] < speed) {
            topSpeed = speed;
            
            [self.attributes setValue:[NSNumber numberWithInt: topSpeed] forKey: DriveDataAttributeTopSpeed];
        } else {
            topSpeed = [currentTopSpeed intValue];
        }
    }
    
    return topSpeed;
}

- (float) updateAvgSpeed {
    if(_finishDate == nil) {
        return 0.0f;
    }
    
    int duration = [_finishDate timeIntervalSinceDate: _startDate];
    float avgSpeed = _distance / duration;
    
    [self.attributes setValue:[NSNumber numberWithFloat: avgSpeed] forKey: DriveDataAttributeAvgSpeed];
    
    return avgSpeed;
}

- (void) updateRouteRectWithLocation: (CLLocation *) location {
    _southWest = CLLocationCoordinate2DMake(MIN(_southWest.latitude, location.coordinate.latitude), MIN(_southWest.longitude, location.coordinate.longitude));
    _northEast = CLLocationCoordinate2DMake(MAX(_northEast.latitude, location.coordinate.latitude), MAX(_northEast.longitude, location.coordinate.longitude));
    
    //NSLog(@"SW lat: %f long: %f", _southWest.latitude, _southWest.longitude);
    //NSLog(@"NE lat: %f long: %f", _northEast.latitude, _northEast.longitude);
}

- (int) getTimeSinceDriveStart {
    int sinceStart = -1 * [_startDate timeIntervalSinceNow];
    
    return sinceStart;
}

- (int) getExperience {
    return [self getDistanceExperience];
}

- (int) getDistanceExperience {
    int exp = (int)(_distance * 0.02f);
    
    return exp;
}

/*
 - (int) getTotalExperience {
 int texp = 0;
 int i = -1;
 int q = (int)[_experienceComponents count];
 
 ExperienceComponentData *ecd;
 
 while(++i < q) {
 ecd = [_experienceComponents objectAtIndex: i];
 texp += ecd.points;
 }
 
 if(_recording == YES) {
 texp += [_driveData getDistanceExperience];
 }
 
 return texp;
 }
 
 - (void) extractDataFromGPXString:(NSString *)gpxString {
 GPXRoot *root = [GPXParser parseGPXWithString:gpxString];
 
 GPXTrack *track;
 GPXTrackSegment *segment;
 NSArray *trackpoints;
 
 track = (GPXTrack *)[root.tracks objectAtIndex:0];
 if(track) {
 segment = (GPXTrackSegment *)[track.tracksegments objectAtIndex:0];
 
 if(segment) {
 trackpoints = segment.trackpoints;
 }
 }
 
 if(trackpoints == nil) {
 NSLog(@"no points in the given root!!");
 return;
 }
 
 int i = -1;
 int q = (int)[trackpoints count];
 
 GPXTrackPoint *ctp;
 CLLocation *cLoc;
 CLLocation *pLoc;
 CLLocationCoordinate2D ctpCoordincate = CLLocationCoordinate2DMake(0, 0);
 float ctpSpeed = 0;
 
 NSMutableArray *path = [[NSMutableArray alloc] init];
 
 float currentDistance = 0.0f;
 
 //acceleration
 
 AccelerationTimer *at0_100 = [[AccelerationTimer alloc] initWithFrom: 2.0f AndTo: 27.7];
 AccelerationTimer *at80_120 = [[AccelerationTimer alloc] initWithFrom: 22.2f AndTo: 33.3];
 
 NSDate *tpTime;
 
 // elevation
 
 ElevationGraphCreator *egc = [[ElevationGraphCreator alloc] init];
 [egc setRouteDistance: self.distance];
 
 //TODO: validate coordinates, what if min is negative and max positive value? boundaries fails
 
 while(++i < q) {
 ctp = (GPXTrackPoint *)[trackpoints objectAtIndex:i];
 cLoc = [[CLLocation alloc] initWithLatitude:ctp.latitude longitude:ctp.longitude];
 ctpCoordincate = cLoc.coordinate;
 
 ctpSpeed = [ctp.extensions.speed floatValue];
 
 if (i == 0) {
 //_southWest = CLLocationCoordinate2DMake(ctpCoordincate.latitude, ctpCoordincate.longitude);
 //_northEast = CLLocationCoordinate2DMake(ctpCoordincate.latitude, ctpCoordincate.longitude);
 } else {
 //_southWest = CLLocationCoordinate2DMake(MIN(_southWest.latitude, ctpCoordincate.latitude), MIN(_southWest.longitude, ctpCoordincate.longitude));
 //_northEast = CLLocationCoordinate2DMake(MAX(_northEast.latitude, ctpCoordincate.latitude), MAX(_northEast.longitude, ctpCoordincate.longitude));
 
 currentDistance += [cLoc distanceFromLocation: pLoc];
 }
 
 [path addObject:[[CLLocation alloc] initWithLatitude: ctpCoordincate.latitude longitude: ctpCoordincate.longitude]];
 
 tpTime = [DateTools getDateFromTimestampString: ctp.extensions.time];
 
 // acceleration
 
 [at0_100 tickTimerWithSpeed: ctpSpeed AndTime: tpTime];
 [at80_120 tickTimerWithSpeed: ctpSpeed AndTime: tpTime];
 
 // elevation
 
 [egc pushWithElevation:ctp.elevation atDistance: currentDistance];
 
 // direction
 
 pLoc = cLoc;
 }
 
 _routePath = [path copy];
 
 
 AccelerationDescriptionData *add0_100 = [[AccelerationDescriptionData alloc] init];
 add0_100.bestResult = at0_100.bestTime;
 add0_100.results = [at0_100 getResults];
 
 AccelerationDescriptionData *add80_120 = [[AccelerationDescriptionData alloc] init];
 add80_120.bestResult = at80_120.bestTime;
 add80_120.results = [at80_120 getResults];
 
 [self.attributes setValue:[add0_100 getMeAsDictionary]  forKey:DriveDataAttributeAcc0_100];
 [self.attributes setValue:[add80_120 getMeAsDictionary]  forKey:DriveDataAttributeAcc80_120];
 
 [self.attributes setValue:[egc getElevationVertexArray]  forKey:DriveDataAttributeElevation];
 
 }


 */

@end
