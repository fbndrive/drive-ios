#import <Foundation/Foundation.h>

@interface ExperienceComponentData : NSObject

@property (nonatomic, strong) NSString *description;
@property (nonatomic) int points;

@end
