#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface RouteSpecialStageData : NSObject

@property (nonatomic) int id;
@property (nonatomic) CLLocationCoordinate2D startCoordinate;
@property (nonatomic) CLLocationCoordinate2D finishCoordinate;
@property (nonatomic) int userId;

- (id)initWithLocation:(CLLocation *) location;
- (void) addLocation:(CLLocation *) location;

- (NSString *) getGPXString;

@end
