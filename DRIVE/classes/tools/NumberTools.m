#import "NumberTools.h"

@implementation NumberTools

+ (int) randomNumberFromRange:(NSRange) range {
    int rnd = rand() % ((NSMaxRange(range) - range.location) + range.location);
    
    return rnd;
}

+ (float) degreesToRadians:(float) degrees {
    return ((degrees) / 180.0 * M_PI);
}

@end
