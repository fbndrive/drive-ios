#import <Foundation/Foundation.h>

@interface AccelerationTimer : NSObject

@property (nonatomic) float fromThreshold;
@property (nonatomic) float toThreshold;
@property (nonatomic) float bestTime;

- (id) initWithFrom: (float) from AndTo: (float) to;
- (NSArray *) getResults;
- (float) tickTimerWithSpeed: (float) speed AndTime: (NSDate *) time;

@end
