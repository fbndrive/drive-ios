#import "AppDataManager.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import "SoundTools.h"

@implementation SoundTools

+(NSString *) timestampToLiteralHHMMSS:(int) number {
    NSString *literal = @"";
    NSInteger seconds = number % 60;
    NSInteger minutes = (number / 60) % 60;
    NSInteger hours = (number / 3600);
    
    //@no2 units-hours @no31 units-minutes g-and @no41 units-seconds
    if (hours > 0) {
        if (hours > 1) {
            literal = [NSString stringWithFormat:@"%@ @no%ld units-hours", literal, (long)hours];
        } else {
            literal = [NSString stringWithFormat:@"%@ @no%ld units-hour", literal, (long)hours];
        }
    }
    
    if (minutes > 0) {
        if (minutes > 1) {
            literal = [NSString stringWithFormat:@"%@ @no%ld units-minutes", literal, (long)minutes];
        } else {
            literal = [NSString stringWithFormat:@"%@ @no%ld units-minute", literal, (long)minutes];
        }
    }
    
    if (seconds > 0) {
        if (seconds > 1) {
            literal = [NSString stringWithFormat:@"%@ @no%ld units-seconds", literal, (long)seconds];
        } else {
            literal = [NSString stringWithFormat:@"%@ @no%ld units-second", literal, (long)seconds];
        }
    }
    //NSLog(@"time: %d:%d:%d", hours, minutes, seconds);
    //NSLog(@"literal: %@", literal);
    
    return [literal stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+(NSString *) distanceToLiteral:(int) distance {
    AppDataManager *adm = [AppDataManager appDataManager];
    float distanceInUnits = [adm convertDistanceToUserUnit: distance];
    
    int v1 = (int)floorf(distanceInUnits);
    int v2 = (int)floorf((distanceInUnits - v1) * 10);
    
    NSString *literal = @"";
    if(v2 > 0) {
        literal = [NSString stringWithFormat:@"@no%d g-point @no%d", v1, v2];
    } else {
        literal = [NSString stringWithFormat:@"@no%d", v1];
    }
    
    return literal;
}

+(NSString *) keyCountNumberToLiteral:(int) number {
    NSMutableArray *words = [[NSMutableArray alloc] init];
    [words insertObject:@"no-zero" atIndex:0];
    [words insertObject:@"no-one" atIndex:1];
    [words insertObject:@"no-two" atIndex:2];
    [words insertObject:@"no-three" atIndex:3];
    [words insertObject:@"no-four" atIndex:4];
    [words insertObject:@"no-five" atIndex:5];
    [words insertObject:@"no-six" atIndex:6];
    [words insertObject:@"no-seven" atIndex:7];
    [words insertObject:@"no-eight" atIndex:8];
    [words insertObject:@"no-nine" atIndex:9];
    [words insertObject:@"no-ten" atIndex:10];
    [words insertObject:@"no-eleven" atIndex:11];
    [words insertObject:@"no-twelve" atIndex:12];
    [words insertObject:@"no-thirteen" atIndex:13];
    [words insertObject:@"no-fourteen" atIndex:14];
    [words insertObject:@"no-fifteen" atIndex:15];
    [words insertObject:@"no-sixteen" atIndex:16];
    [words insertObject:@"no-seventeen" atIndex:17];
    [words insertObject:@"no-eighteen" atIndex:18];
    [words insertObject:@"no-nineteen" atIndex:19];
    [words insertObject:@"no-twenty" atIndex:20];
    
    if(number <= 20) {
        return [words objectAtIndex:number];
    }
    
    if(number == 30) return @"no-thirty";
    if(number == 40) return @"no-fourty";
    if(number == 50) return @"no-fifty";
    if(number == 60) return @"no-sixty";
    if(number == 70) return @"no-seventy";
    if(number == 80) return @"no-eighty";
    if(number == 90) return @"no-ninety";
    if(number == 100) return @"no-hundred";
    if(number == 1000) return @"no-thousand";
    
    return nil;
}

+(NSString *) floatToLiteral:(float) number withPrecision: (int) precision {    
    int v1 = (int)floorf(number);
    int v2 = (int)floorf((number - v1) * precision);
    
    NSString *literal = @"";
    if(v2 > 0) {
        literal = [NSString stringWithFormat:@"%@ g-point %@", [SoundTools integerToLiteral:v1], [SoundTools integerToLiteral:v2]];
    } else {
        literal = [NSString stringWithFormat:@"%@", [SoundTools integerToLiteral:v1]];
    }
    
    return literal;
}

+(NSString *) integerToLiteral:(int) number {
    //NSLog(@"%d.%d - %f", intVal, decimalVal, number);
    
    NSString *literalNumber = @"";
    int hundreds;
    int tens;
    int units;
    
    if (number < 0) {
        number = -number;
        literalNumber = @"minus ";
    }
    
    if (number > 1000) {
        literalNumber = [NSString stringWithFormat:@"%@ %@ %@", literalNumber, [SoundTools integerToLiteral:(int)floor(number/1000)], [SoundTools keyCountNumberToLiteral:1000]];
        
        hundreds = number % 1000;
        tens = hundreds % 100;
        
        if (hundreds > 100) {
            literalNumber = [NSString stringWithFormat:@"%@ %@", literalNumber, [SoundTools integerToLiteral:hundreds]];
        } else if (tens) {
            //literalNumber = [NSString stringWithFormat:@"%@ and %@", literalNumber, [SoundTools numberToLiteral:tens]];
            literalNumber = [NSString stringWithFormat:@"%@ %@", literalNumber, [SoundTools integerToLiteral:tens]];
        }
    } else if (number > 100) {
        literalNumber = [NSString stringWithFormat:@"%@ %@ %@", literalNumber, [SoundTools integerToLiteral:(int) floor(number/100)], [SoundTools keyCountNumberToLiteral:100]];
        
        tens = number % 100;
        if (tens) {
            //literalNumber = [NSString stringWithFormat:@"%@ and %@", literalNumber, [SoundTools numberToLiteral:tens]];
            literalNumber = [NSString stringWithFormat:@"%@ %@", literalNumber, [SoundTools integerToLiteral:tens]];
        }
    } else if (number > 20) {
        literalNumber = [NSString stringWithFormat:@"%@ %@", literalNumber, [SoundTools keyCountNumberToLiteral:10 * floor (number/10)]];
        
        units = number % 10;
        
        if (units){
            literalNumber = [NSString stringWithFormat:@"%@ %@", literalNumber, [SoundTools integerToLiteral:units]];
        }
    } else {
        literalNumber = [NSString stringWithFormat:@"%@ %@", literalNumber, [SoundTools keyCountNumberToLiteral:number]];
    }
    
    return [literalNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end
