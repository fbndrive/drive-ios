#import "UserLevelData.h"
#import "UserLevelTools.h"

@implementation UserLevelTools

float const MULTIPLY_FACTOR = 1.0f;

+(UserLevelData *) userLevelDataForExp:(int) number {
    NSArray *levelArray = [UserLevelTools getLevelArray];
    
    int i = -1;
    int q = (int)[levelArray count];
    UserLevelData *uld;
    
    while(++i < q) {
        uld = (UserLevelData *)[levelArray objectAtIndex:i];
        
        if(uld) {
            if((float)(uld.nextTreshold * MULTIPLY_FACTOR) > (float)number ) {
                break;
            }
        }
    }
    //NSLog(@"uld %i %f %f", uld.nextTreshold, uld.nextTreshold*MULTIPLY_FACTOR, (float)number);
    return uld;
}

+(NSArray *) getLevelArray {
    NSArray *levelArray = [[NSArray alloc] initWithObjects:
    [UserLevelData userLevelDataWithLevel:1 currentTreshold:0 andNextTreshold:1000],
    [UserLevelData userLevelDataWithLevel:2 currentTreshold:1000 andNextTreshold:8000],
    [UserLevelData userLevelDataWithLevel:3 currentTreshold:8000 andNextTreshold:18000],
    [UserLevelData userLevelDataWithLevel:4 currentTreshold:18000 andNextTreshold:29000],
    [UserLevelData userLevelDataWithLevel:5 currentTreshold:29000 andNextTreshold:41000],
    [UserLevelData userLevelDataWithLevel:6 currentTreshold:41000 andNextTreshold:54000],
    [UserLevelData userLevelDataWithLevel:7 currentTreshold:54000 andNextTreshold:67000],
    [UserLevelData userLevelDataWithLevel:8 currentTreshold:67000 andNextTreshold:81000],
    [UserLevelData userLevelDataWithLevel:9 currentTreshold:81000 andNextTreshold:96000],
    [UserLevelData userLevelDataWithLevel:10 currentTreshold:96000 andNextTreshold:111000],
    [UserLevelData userLevelDataWithLevel:11 currentTreshold:111000 andNextTreshold:130000],
    [UserLevelData userLevelDataWithLevel:12 currentTreshold:130000 andNextTreshold:150000],
    [UserLevelData userLevelDataWithLevel:13 currentTreshold:150000 andNextTreshold:170000],
    [UserLevelData userLevelDataWithLevel:14 currentTreshold:170000 andNextTreshold:190000],
    [UserLevelData userLevelDataWithLevel:15 currentTreshold:190000 andNextTreshold:220000],
    [UserLevelData userLevelDataWithLevel:16 currentTreshold:220000 andNextTreshold:250000],
    [UserLevelData userLevelDataWithLevel:17 currentTreshold:250000 andNextTreshold:280000],
    [UserLevelData userLevelDataWithLevel:18 currentTreshold:280000 andNextTreshold:310000],
    [UserLevelData userLevelDataWithLevel:19 currentTreshold:310000 andNextTreshold:340000],
    [UserLevelData userLevelDataWithLevel:20 currentTreshold:340000 andNextTreshold:370000],
    [UserLevelData userLevelDataWithLevel:21 currentTreshold:370000 andNextTreshold:400000],
    [UserLevelData userLevelDataWithLevel:22 currentTreshold:400000 andNextTreshold:430000],
    [UserLevelData userLevelDataWithLevel:23 currentTreshold:430000 andNextTreshold:470000],
    [UserLevelData userLevelDataWithLevel:24 currentTreshold:470000 andNextTreshold:510000],
    [UserLevelData userLevelDataWithLevel:25 currentTreshold:510000 andNextTreshold:550000],
    [UserLevelData userLevelDataWithLevel:26 currentTreshold:550000 andNextTreshold:590000],
    [UserLevelData userLevelDataWithLevel:27 currentTreshold:590000 andNextTreshold:630000],
    [UserLevelData userLevelDataWithLevel:28 currentTreshold:630000 andNextTreshold:670000],
    [UserLevelData userLevelDataWithLevel:29 currentTreshold:670000 andNextTreshold:710000],
    [UserLevelData userLevelDataWithLevel:30 currentTreshold:710000 andNextTreshold:760000],
    [UserLevelData userLevelDataWithLevel:31 currentTreshold:760000 andNextTreshold:810000],
    [UserLevelData userLevelDataWithLevel:32 currentTreshold:810000 andNextTreshold:860000],
    [UserLevelData userLevelDataWithLevel:33 currentTreshold:860000 andNextTreshold:910000],
    [UserLevelData userLevelDataWithLevel:34 currentTreshold:910000 andNextTreshold:960000],
    [UserLevelData userLevelDataWithLevel:35 currentTreshold:960000 andNextTreshold:1010000],
    [UserLevelData userLevelDataWithLevel:36 currentTreshold:1010000 andNextTreshold:1060000],
    [UserLevelData userLevelDataWithLevel:37 currentTreshold:1060000 andNextTreshold:1110000],
    [UserLevelData userLevelDataWithLevel:38 currentTreshold:1110000 andNextTreshold:1165000],
    [UserLevelData userLevelDataWithLevel:39 currentTreshold:1165000 andNextTreshold:1220000],
    [UserLevelData userLevelDataWithLevel:40 currentTreshold:1220000 andNextTreshold:1280000],
    [UserLevelData userLevelDataWithLevel:41 currentTreshold:1280000 andNextTreshold:1340000],
    [UserLevelData userLevelDataWithLevel:42 currentTreshold:1340000 andNextTreshold:1400000],
    [UserLevelData userLevelDataWithLevel:43 currentTreshold:1400000 andNextTreshold:1460000],
    [UserLevelData userLevelDataWithLevel:44 currentTreshold:1460000 andNextTreshold:1520000],
    [UserLevelData userLevelDataWithLevel:45 currentTreshold:1520000 andNextTreshold:1600000],nil];
    
    /*
     NSArray *levelArray = [[NSArray alloc] initWithObjects:
     [UserLevelData userLevelDataWithLevel:1 currentTreshold:0 andNextTreshold:1000],
     [UserLevelData userLevelDataWithLevel:2 currentTreshold:1000 andNextTreshold:8000],
     [UserLevelData userLevelDataWithLevel:3 currentTreshold:8000 andNextTreshold:18000],
     [UserLevelData userLevelDataWithLevel:4 currentTreshold:11000 andNextTreshold:29000],
     [UserLevelData userLevelDataWithLevel:5 currentTreshold:12000 andNextTreshold:41000],
     [UserLevelData userLevelDataWithLevel:6 currentTreshold:13000 andNextTreshold:54000],
     [UserLevelData userLevelDataWithLevel:7 currentTreshold:13000 andNextTreshold:67000],
     [UserLevelData userLevelDataWithLevel:8 currentTreshold:14000 andNextTreshold:81000],
     [UserLevelData userLevelDataWithLevel:9 currentTreshold:15000 andNextTreshold:96000],
     [UserLevelData userLevelDataWithLevel:10 currentTreshold:15000 andNextTreshold:111000],
     [UserLevelData userLevelDataWithLevel:11 currentTreshold:19000 andNextTreshold:130000],
     [UserLevelData userLevelDataWithLevel:12 currentTreshold:20000 andNextTreshold:150000],
     [UserLevelData userLevelDataWithLevel:13 currentTreshold:20000 andNextTreshold:170000],
     [UserLevelData userLevelDataWithLevel:14 currentTreshold:20000 andNextTreshold:190000],
     [UserLevelData userLevelDataWithLevel:15 currentTreshold:30000 andNextTreshold:220000],
     [UserLevelData userLevelDataWithLevel:16 currentTreshold:30000 andNextTreshold:250000],
     [UserLevelData userLevelDataWithLevel:17 currentTreshold:30000 andNextTreshold:280000],
     [UserLevelData userLevelDataWithLevel:18 currentTreshold:30000 andNextTreshold:310000],
     [UserLevelData userLevelDataWithLevel:19 currentTreshold:30000 andNextTreshold:340000],
     [UserLevelData userLevelDataWithLevel:20 currentTreshold:30000 andNextTreshold:370000],
     [UserLevelData userLevelDataWithLevel:21 currentTreshold:30000 andNextTreshold:400000],
     [UserLevelData userLevelDataWithLevel:22 currentTreshold:30000 andNextTreshold:430000],
     [UserLevelData userLevelDataWithLevel:23 currentTreshold:40000 andNextTreshold:470000],
     [UserLevelData userLevelDataWithLevel:24 currentTreshold:40000 andNextTreshold:510000],
     [UserLevelData userLevelDataWithLevel:25 currentTreshold:40000 andNextTreshold:550000],
     [UserLevelData userLevelDataWithLevel:26 currentTreshold:40000 andNextTreshold:590000],
     [UserLevelData userLevelDataWithLevel:27 currentTreshold:40000 andNextTreshold:630000],
     [UserLevelData userLevelDataWithLevel:28 currentTreshold:40000 andNextTreshold:670000],
     [UserLevelData userLevelDataWithLevel:29 currentTreshold:40000 andNextTreshold:710000],
     [UserLevelData userLevelDataWithLevel:30 currentTreshold:50000 andNextTreshold:760000],
     [UserLevelData userLevelDataWithLevel:31 currentTreshold:50000 andNextTreshold:810000],
     [UserLevelData userLevelDataWithLevel:32 currentTreshold:50000 andNextTreshold:860000],
     [UserLevelData userLevelDataWithLevel:33 currentTreshold:50000 andNextTreshold:910000],
     [UserLevelData userLevelDataWithLevel:34 currentTreshold:50000 andNextTreshold:960000],
     [UserLevelData userLevelDataWithLevel:35 currentTreshold:50000 andNextTreshold:1010000],
     [UserLevelData userLevelDataWithLevel:36 currentTreshold:50000 andNextTreshold:1060000],
     [UserLevelData userLevelDataWithLevel:37 currentTreshold:50000 andNextTreshold:1110000],
     [UserLevelData userLevelDataWithLevel:38 currentTreshold:55000 andNextTreshold:1165000],
     [UserLevelData userLevelDataWithLevel:39 currentTreshold:55000 andNextTreshold:1220000],
     [UserLevelData userLevelDataWithLevel:40 currentTreshold:60000 andNextTreshold:1280000],
     [UserLevelData userLevelDataWithLevel:41 currentTreshold:60000 andNextTreshold:1340000],
     [UserLevelData userLevelDataWithLevel:42 currentTreshold:60000 andNextTreshold:1400000],
     [UserLevelData userLevelDataWithLevel:43 currentTreshold:60000 andNextTreshold:1460000],
     [UserLevelData userLevelDataWithLevel:44 currentTreshold:60000 andNextTreshold:1520000],
     [UserLevelData userLevelDataWithLevel:45 currentTreshold:80000 andNextTreshold:1600000],nil]; */
    
    return levelArray;
}

@end