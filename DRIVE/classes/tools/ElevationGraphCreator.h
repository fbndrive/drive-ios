#import <Foundation/Foundation.h>

@interface ElevationGraphCreator : NSObject

@property (nonatomic) float highest;
@property (nonatomic) float routeDistance;

- (void) pushWithElevation: (float) elevation atDistance: (float) distance;
- (NSArray *) getElevationVertexArray;

@end
