#import "AccelerationTimer.h"
#import "DateTools.h"

@interface AccelerationTimer ()

@property (nonatomic) BOOL currentlyUnderFromThreshold;
@property (nonatomic) BOOL currentlyUnderTopThreshold;
@property (nonatomic) BOOL previouslyUnderFromThreshold;
@property (nonatomic) BOOL previouslyUnderTopThreshold;
@property (nonatomic) BOOL measuringAcceleration;
@property (nonatomic, retain) NSDate *measurmentStartTime;
@property (nonatomic, retain) NSMutableArray *results;

@end

@implementation AccelerationTimer

@synthesize fromThreshold = _fromThreshold;
@synthesize toThreshold = _toThreshold;
@synthesize bestTime = _bestTime;

- (id) init {
    self = [super init];
    if(self) {
        _results = [[NSMutableArray alloc] init];
        _bestTime = INFINITY;
        
        [self resetValues];
    }
    
    return self;
}

- (id) initWithFrom: (float) from AndTo: (float) to {
    id me = [self init];
    
    _fromThreshold = from;
    _toThreshold = to;
    
    return me;
}

- (NSArray *) getResults {
    return [_results copy];
}

- (float) bestTime {
    if(_bestTime == INFINITY) _bestTime = -1.0f;
    
    return _bestTime;
}

- (float) tickTimerWithSpeed: (float) speed AndTime: (NSDate *) time {
    float accelerationTime = -1.0f;
    
    _currentlyUnderFromThreshold = speed <= _fromThreshold;
    _currentlyUnderTopThreshold = speed <= _toThreshold;
    
    //NSLog(@"measuring: %s, speed: %f", _measuringAcceleration ? "true" : "false", speed);
    
    if(_measuringAcceleration) {
        
        if(_currentlyUnderFromThreshold) {
            _measuringAcceleration = NO;
        } else if(_currentlyUnderTopThreshold) {
            
        } else {
            if(_previouslyUnderTopThreshold) {
                //measurement done
                accelerationTime = -1 * [_measurmentStartTime timeIntervalSinceDate: time];
                
                _bestTime = fminf(_bestTime, accelerationTime);
                
                NSLog(@"best time: %f", _bestTime);
                
                [_results addObject: [[NSDictionary alloc] initWithObjectsAndKeys:
                                      [NSNumber numberWithFloat: accelerationTime], @"value",
                                      [DateTools getTimestampStringFromDate: time], @"timestamp",
                                      nil]];
                
                [self resetValues];
            } else {
                _measuringAcceleration = NO;
            }
        }
    } else {
        if(_currentlyUnderFromThreshold) {
            _measuringAcceleration = NO;
        } else {
            if(_previouslyUnderFromThreshold) {
                _measuringAcceleration = YES;
                //_measurmentStartTime = [NSDate date];
                _measurmentStartTime = time;
            }
        }
    }
    
    if(accelerationTime < 0) {
        _previouslyUnderFromThreshold = _currentlyUnderFromThreshold;
        _previouslyUnderTopThreshold = _currentlyUnderTopThreshold;
    } else {
        //NSLog(@"accelerationTime: %f", accelerationTime);
    }
    
    return accelerationTime;
}

- (void) resetValues {
    _currentlyUnderFromThreshold = NO;
    _currentlyUnderTopThreshold = NO;
    _previouslyUnderFromThreshold = NO;
    _previouslyUnderTopThreshold = NO;
    _measuringAcceleration = NO;
    
    _measurmentStartTime = nil;
}

@end
