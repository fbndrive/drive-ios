#import "DriveTrackpointData.h"
#import "RouteCompass.h"

@implementation RouteCompass

@synthesize start = _start;
@synthesize finish = _finish;

- (id) init {
    self = [super init];
    
    if(self) {
        _start = nil;
        _finish = nil;
    }
    
    return self;
}

- (float) getDirection {
    if(_start == nil || _finish == nil) {
        return -1.0;
    }
    
    float deltaLon = _finish.longitude - _start.longitude;
    float deltaLat = _finish.latitude - _start.latitude;
    
    return atan2(deltaLon, deltaLat) * 180 / M_PI;
}

@end
