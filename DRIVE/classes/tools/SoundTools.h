#import <Foundation/Foundation.h>

@interface SoundTools : NSObject

+(NSString *) timestampToLiteralHHMMSS:(int) number;
+(NSString *) distanceToLiteral:(int) distance;
+(NSString *) floatToLiteral:(float) number withPrecision: (int) precision;
+(NSString *) integerToLiteral:(int) number;

@end
