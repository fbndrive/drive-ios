#import "Constants.h"
#import "DateTools.h"

@implementation DateTools

+ (NSString *) getTimestampStringFromDate: (NSDate *) date {
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:Database_dateTimezone];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:timeZone];
    [format setDateFormat:Database_dateFormat];
    
    return [format stringFromDate:date];
}

+ (NSDate *) getDateFromTimestampString: (NSString *) tstr {
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:Database_dateTimezone];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:timeZone];
    [format setDateFormat:Database_dateFormat];
    
    return [format dateFromString:tstr];
}

@end
