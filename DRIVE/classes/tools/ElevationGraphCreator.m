#import "ElevationGraphCreator.h"

@interface ElevationGraphCreator ()

@property (nonatomic, retain) NSMutableArray *vertexArray;
@property (nonatomic) int previousElevationObjSign;
@property (nonatomic, retain) NSDictionary *previousElevationObj;

@end

@implementation ElevationGraphCreator

@synthesize highest = _highest;
@synthesize routeDistance = _routeDistance;

- (id) init {
    self = [super init];
    if(self) {
        _highest = 0;
        _previousElevationObjSign = 100;
        _previousElevationObj = nil;
        _vertexArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void) pushWithElevation: (float) elevation atDistance: (float) distance {
    if(elevation > _highest) {
        _highest = elevation;
    }
    
    if(_previousElevationObj == nil) {
        _previousElevationObj = [self getDictionaryForElevation: elevation AndDistance: distance];
        [_vertexArray addObject: _previousElevationObj];
        return;
    }
    
    /*if(_previousElevationObjSign == INFINITY) {
        if(elevation > [[_previousElevationObj valueForKey: @"elevation"] floatValue]) {
            _previousElevationObjSign = 1;
        } else {
            _previousElevationObjSign = -1;
        }
    }*/
    
    int currentSign;
    
    if(elevation > [[_previousElevationObj valueForKey: @"elevation"] floatValue]) {
        currentSign = 1;
    } else {
        currentSign = -1;
    }
    
    if(_previousElevationObjSign != 100 && currentSign != _previousElevationObjSign) {
        [_vertexArray addObject: _previousElevationObj];
        //NSLog(@"!!! change in elevation direction %f", elevation);
    } else {
        //NSLog(@"no change in elevation direction %f", elevation);
    }
    
    _previousElevationObjSign = currentSign;
    _previousElevationObj = [self getDictionaryForElevation: elevation AndDistance: distance];
}

- (NSArray *) getElevationVertexArray {
    return [_vertexArray copy];
}

- (NSDictionary *) getDictionaryForElevation: (float) elevation AndDistance: (float) distance {
    return [[NSDictionary alloc] initWithObjectsAndKeys:
                                [NSNumber numberWithFloat: elevation], @"elevation",
                                [NSNumber numberWithFloat: distance], @"distance",
                                nil];
}

@end
