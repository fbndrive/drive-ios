#import "DriveTrackpointData.h"
#import <Foundation/Foundation.h>

@interface RouteCompass : NSObject

@property (nonatomic, retain) DriveTrackpointData *start;
@property (nonatomic, retain) DriveTrackpointData *finish;

- (float) getDirection;

@end
