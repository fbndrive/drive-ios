#import <Foundation/Foundation.h>
#import "RoutePoint.h"

@interface RoutePathData : NSObject

// distance in meters
@property (nonatomic, retain) RoutePoint *firstPoint;
@property (nonatomic, retain) RoutePoint *lastPoint;
@property (nonatomic) CLLocationCoordinate2D topLeftCoordinate;
@property (nonatomic) CLLocationCoordinate2D bottomRightCoordinate;
@property (nonatomic) int latitudeDistance;
@property (nonatomic) int longitudeDistance;

- (float) getDrawFactorForRectangle: (CGRect) rect;
- (RoutePoint *) getLinkForAreaMin:(CLLocationCoordinate2D) topleftCoord andMax:(CLLocationCoordinate2D) bottomrightCoord;
//- (void) parseGPXRoot:(GPXRoot *) root;

@end
