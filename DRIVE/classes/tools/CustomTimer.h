#import <Foundation/Foundation.h>

@interface CustomTimer : NSObject;

@property (strong, nonatomic) NSDate *start;
@property (strong, nonatomic) NSDate *end;

- (void) startTimer;
- (void) stopTimer;
- (double) timeElapsedInSeconds;
- (double) timeElapsedInMilliseconds;
- (double) timeElapsedInMinutes;

@end