#import <Foundation/Foundation.h>

@interface DateTools : NSObject

+ (NSString *) getTimestampStringFromDate: (NSDate *) date;
+ (NSDate *) getDateFromTimestampString: (NSString *) tstr;

@end
