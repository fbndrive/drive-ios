#import <CoreLocation/CoreLocation.h>
#import "RoutePoint.h"
#import "RoutePathData.h"

// RouteDetails

@interface RoutePathData ()

@property (nonatomic, retain) NSDictionary *routeChainHelper;
@property (nonatomic, retain) NSDictionary *routeChain;

@end


@implementation RoutePathData

@synthesize firstPoint;
@synthesize lastPoint;
@synthesize topLeftCoordinate = _topLeftCoordinate;
@synthesize bottomRightCoordinate = _bottomRightCoordinate;
@synthesize latitudeDistance = _latitudeDistance;
@synthesize longitudeDistance = _longitudeDistance;

- (id) init {
    self = [super init];
    
    if(self) {
        _latitudeDistance = 0;
        _longitudeDistance = 0;
        _topLeftCoordinate = CLLocationCoordinate2DMake(0, 0);
        _bottomRightCoordinate = CLLocationCoordinate2DMake(0, 0);
    }
    
    return self;
}

- (float) getDrawFactorForRectangle: (CGRect) rect {
    float factor = 0.0f;
    
    //TODO validate with equator and Greenwich
    
    if(_longitudeDistance > _latitudeDistance) { //LANDSCAPE
        factor = (rect.size.width * 0.0001f) / (_bottomRightCoordinate.longitude - _topLeftCoordinate.longitude);
    } else { //PORTRAIT
        factor = (rect.size.height * 0.0001f) / (_bottomRightCoordinate.latitude - _topLeftCoordinate.latitude);
    }
    
    return factor;
}

- (RoutePoint *) getLinkForAreaMin:(CLLocationCoordinate2D) topleftCoord andMax:(CLLocationCoordinate2D) bottomrightCoord {
    RoutePoint *rp = nil;
    
    NSNumber *routeDictionaryKey;
    
    int integer;
    int tenths;
    int hundredths;
    int thousandths;
    int tenthousandths;
    
    //float val = round(topleftCoord.longitude * 10000);
    //float val = round(topleftCoord.longitude * 10000)/1000;
    float val = topleftCoord.longitude;
    NSLog(@"just val1 %f", val);
    
    integer = (int)val;
    val = (val - integer) * 10;
    
    tenths = (int)val;
    val = (val - tenths) * 10;
    
    hundredths = (int)val;
    val = (val - hundredths) * 10;
    
    thousandths = (int)val;
    val = (val - thousandths) * 10;
    
    tenthousandths = (int)val;
    
    while(true) {
        routeDictionaryKey = [NSNumber numberWithInt:integer];
        
        val = [[NSString stringWithFormat:@"%d.%d%d%d%d", integer, tenths, hundredths, thousandths, tenthousandths] floatValue];
        
        NSLog(@"------ val %f", val);
        NSLog(@"longitude broke apart: %d.%d.%d.%d.%d", integer, tenths, hundredths, thousandths, tenthousandths);
        
        if([_routeChainHelper objectForKey:[NSString stringWithFormat:@"%d", integer]]) {
            if([_routeChainHelper objectForKey:[NSString stringWithFormat:@"%d.%d", integer, tenths]]) {
                
                if([_routeChainHelper objectForKey:[NSString stringWithFormat:@"%d.%d%d", integer, tenths, hundredths]]) {
            
                    if([_routeChainHelper objectForKey:[NSString stringWithFormat:@"%d.%d%d%d", integer, tenths, hundredths, thousandths]]) {
            
                        if([_routeChainHelper objectForKey:[NSString stringWithFormat:@"%d.%d%d%d%d", integer, tenths, hundredths, thousandths, tenthousandths]]) {
                            
                            rp = [_routeChain objectForKey: routeDictionaryKey][tenths][hundredths][thousandths][tenthousandths][0];
                            
                            if(rp.coordinate.latitude >= topleftCoord.latitude && rp.coordinate.latitude <= bottomrightCoord.latitude) {
                                break;
                            }
                            val += 0.0001f;
                            NSLog(@"+1 %f",val);
                        } else {
                            NSLog(@"+2 %f",val);
                            val += 0.0001f;
                        }
                    } else {
                        val += 0.0010f;
                    }
                } else {
                    val += 0.0100f;
                }
            } else {
                val += 0.1000f;
            }
            
        } else {
            val += 1.0000f;
        }
        
        if(val > bottomrightCoord.longitude) {
            break;
        }
        
        NSLog(@"------ %f", val);
        NSLog(@"int val %f", val);
        integer = (int)val;
        val = (val - integer) * 10;
        NSLog(@"int val %f", val);
        tenths = (int)val;
        val = (val - tenths) * 10;
        NSLog(@"tenths val %f", val);
        hundredths = (int)val;
        val = (val - hundredths) * 10;
        NSLog(@"hundredths val %f", val);
        thousandths = (int)val;
        val = (val - thousandths) * 10;
        NSLog(@"thousandths val %f", val);
        tenthousandths = (int)val;
        NSLog(@"tenthousandths val %f", val);
    }
    
    return rp;
}

/*- (void) parseGPXRoot:(GPXRoot *) root {
  
    GPXTrack *track;
    GPXTrackSegment *segment;
    NSArray *trackpoints;
    
    track = (GPXTrack *)[root.tracks objectAtIndex:0];
    if(track) {
        segment = (GPXTrackSegment *)[track.tracksegments objectAtIndex:0];
        
        if(segment) {
            trackpoints = segment.trackpoints;
        }
    }
    
    if(trackpoints == nil) {
        NSLog(@"no points in the given root!!");
        return;
    }
    
    int i = -1;
    int q = (int)[trackpoints count];
    
    GPXTrackPoint *point;
    
    int integer;
    int tenths;
    int hundredths;
    int thousandths;
    int tenthousandths;
    
    float val;
    
    NSMutableDictionary *routeDictionary = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *routeChainHelper = [[NSMutableDictionary alloc] init];
    NSNumber *routeDictionaryKey;
    RoutePoint *routePoint;
    RoutePoint *prevRoutePoint;
    
    NSString *helperKey;
    
    firstPoint = nil;
    lastPoint = nil;
    
    while(++i < q) {
        if(routePoint) {
            prevRoutePoint = routePoint;
        }
        
        point = (GPXTrackPoint *)[trackpoints objectAtIndex:i];
        
        val = point.longitude;
        
        if(i == 0) {
            _topLeftCoordinate.latitude = _bottomRightCoordinate.latitude = point.latitude;
            _topLeftCoordinate.longitude = _bottomRightCoordinate.longitude = point.longitude;
        } else {
            _topLeftCoordinate.latitude = MAX(_topLeftCoordinate.latitude, point.latitude);
            _topLeftCoordinate.longitude = MIN(_topLeftCoordinate.longitude, point.longitude);
            _bottomRightCoordinate.latitude = MIN(_bottomRightCoordinate.latitude, point.latitude);
            _bottomRightCoordinate.longitude = MAX(_bottomRightCoordinate.longitude, point.longitude);
        }
        
        integer = (int)val;
        val = (val - integer) * 10;
        
        tenths = (int)val;
        val = (val - tenths) * 10;
        
        hundredths = (int)val;
        val = (val - hundredths) * 10;
        
        thousandths = (int)val;
        val = (val - thousandths) * 10;
        
        tenthousandths = (int)val;
        
        routeDictionaryKey = [NSNumber numberWithInt:integer];
        
        //routeDictionaryHelper = [NSNumber setO:integer];
        
        helperKey = [NSString stringWithFormat:@"%d", integer];
        if([routeChainHelper objectForKey: helperKey] == nil) {
            [routeChainHelper setObject: [NSNumber numberWithBool:YES] forKey:helperKey];
            //NSLog(@"test: %@", helperKey);
        }
        
        helperKey = [NSString stringWithFormat:@"%d.%d", integer, tenths];
        if([routeChainHelper objectForKey: helperKey] == nil) {
            [routeChainHelper setObject: [NSNumber numberWithBool:YES] forKey:helperKey];
            //NSLog(@"test: %@", helperKey);
        }
        
        helperKey = [NSString stringWithFormat:@"%d.%d%d", integer, tenths, hundredths];
        if([routeChainHelper objectForKey: helperKey] == nil) {
            [routeChainHelper setObject: [NSNumber numberWithBool:YES] forKey:helperKey];
            //NSLog(@"test: %@", helperKey);
        }
        
        helperKey = [NSString stringWithFormat:@"%d.%d%d%d", integer, tenths, hundredths, thousandths];
        if([routeChainHelper objectForKey: helperKey] == nil) {
            [routeChainHelper setObject: [NSNumber numberWithBool:YES] forKey:helperKey];
            //NSLog(@"test: %@", helperKey);
        }
        
        helperKey = [NSString stringWithFormat:@"%d.%d%d%d%d", integer, tenths, hundredths, thousandths, tenthousandths];
        if([routeChainHelper objectForKey: helperKey] == nil) {
            [routeChainHelper setObject: [NSNumber numberWithBool:YES] forKey:helperKey];
        }
        
        if([routeDictionary objectForKey: routeDictionaryKey] == nil) {
            [routeDictionary setObject: [self getEmptyTreeWithDepth: 4] forKey: routeDictionaryKey];
        }
        
        routePoint = [[RoutePoint alloc] init];
        routePoint.coordinate = CLLocationCoordinate2DMake(point.latitude, point.longitude);
        if(prevRoutePoint) {
            routePoint.prev = prevRoutePoint;
            prevRoutePoint.next = routePoint;
        }
        
        [[routeDictionary objectForKey: routeDictionaryKey][tenths][hundredths][thousandths][tenthousandths] addObject: routePoint];
        
        if(firstPoint == nil) {
            firstPoint = routePoint;
        }
        
        //NSLog(@"%@, %d, %d, %d, %d", [[routeDictionary objectForKey:routeDictionaryKey] count], tenths, hundredths, thousandths, tenthousandths);
        //NSLog(@"count: %d%d%d%d %d", tenths, hundredths, thousandths, tenthousandths, [[routeDictionary objectForKey: routeDictionaryKey][tenths][hundredths][thousandths][tenthousandths] count]);
        
    }
    
    lastPoint = routePoint;
    
    CLLocation *l1, *l2;
    l1 = [[CLLocation alloc] initWithLatitude:_topLeftCoordinate.latitude longitude:_topLeftCoordinate.longitude];
    l2 = [[CLLocation alloc] initWithLatitude:_bottomRightCoordinate.latitude longitude:_topLeftCoordinate.longitude];
    
    _latitudeDistance = [l1 distanceFromLocation:l2];
    
    l1 = nil;
    l2 = nil;
    l1 = [[CLLocation alloc] initWithLatitude:_topLeftCoordinate.latitude longitude:_topLeftCoordinate.longitude];
    l2 = [[CLLocation alloc] initWithLatitude:_topLeftCoordinate.latitude longitude:_bottomRightCoordinate.longitude];
    
    _longitudeDistance = [l1 distanceFromLocation:l2];
    
    _routeChain = [routeDictionary copy];
    _routeChainHelper = [routeChainHelper copy];
    
    NSLog(@"dict cout: %lu", (unsigned long)[[routeDictionary objectForKey:routeDictionaryKey][0][0][0][1] count]);
    NSLog(@"min %f %f, max %f %f", _topLeftCoordinate.latitude, _topLeftCoordinate.longitude, _bottomRightCoordinate.latitude, _bottomRightCoordinate.longitude);
    NSLog(@"latitudeRange %d, longitudeRange %d", _latitudeDistance, _longitudeDistance);

}*/

- (NSArray *) getEmptyTreeWithDepth: (int) depth {
    NSMutableArray *tree = [NSMutableArray arrayWithCapacity: 10];
    
    int i = -1;
    int q = 10;
    
    while (++i < q) {
        if(depth <= 1) {
            [tree addObject:[[NSMutableArray alloc] init]];
        } else {
            [tree addObject:[self getEmptyTreeWithDepth: depth - 1]];
        }
    }

    return tree;
}

@end
