#import <Foundation/Foundation.h>

@interface NumberTools : NSObject

+ (int) randomNumberFromRange:(NSRange) range;
+ (float) degreesToRadians:(float) degrees;

@end
