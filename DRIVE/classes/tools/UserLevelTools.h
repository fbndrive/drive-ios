#import <Foundation/Foundation.h>

@interface UserLevelTools : NSObject

extern float const MULTIPLY_FACTOR;

+(UserLevelData *) userLevelDataForExp:(int) number;

@end
