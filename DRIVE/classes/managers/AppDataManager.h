#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import "DriveDescriptionData.h"
#import "FBSDKCoreKit/FBSDKCoreKit.h"
#import "UserData.h"
#import "UserLevelData.h"

@interface AppDataManager : NSObject

//private

@property (nonatomic) int filteredSnapshotsArray;

//public

@property (nonatomic) UserData *userData;
@property (nonatomic) int driveCharacteristic;
@property (nonatomic, strong) DriveDescriptionData *driveDescription;
@property (nonatomic) int totalDistance;
@property (nonatomic) int totalExperience;
@property (nonatomic) UserLevelData *levelData;

@property (strong, nonatomic) NSArray *snapshotArray;
@property (strong, nonatomic) NSArray *narrowedSnapshotArray;

- (void) setupUserFromFBData:(FBSDKProfile *) fbData;
//- (void) setupForUser:(int) userId;
- (void) reset;
- (void) updateTotalDistance;
- (void) updateTotalExperience;
- (void) updateUserLevelStats;

- (NSArray *) getHistoryOfDrives;

- (void) narrowSnaphotsToLocation:(CLLocation *) location;

// UNITS
- (NSString *) getUserDistanceUnitLabel;
- (float) convertDistanceToUserUnit: (int) distance;
- (float) convertSpeedToUserUnit: (float) speed; // meter per second input

- (void) updateDriveDescription;

+ (id) appDataManager;

@end
