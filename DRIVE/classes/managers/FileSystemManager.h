#import "DriveData.h"
#import <Foundation/Foundation.h>

@interface FileSystemManager : NSObject

+(void) createRequiredFolders;

+(BOOL) saveHistoryDriveMapImage: (UIImage *) image forDriveData:(DriveData *) driveData;
+(BOOL) saveHistoryListMapImage: (UIImage *) image forDriveData:(DriveData *) driveData;

+(UIImage *) getHistoryDriveMapImageForDriveData:(DriveData *) driveData;
+(UIImage *) getHistoryListMapImageForDriveData:(DriveData *) driveData;

@end
