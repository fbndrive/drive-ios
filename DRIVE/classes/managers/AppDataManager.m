#import "AppDataManager.h"
#import "Constants.h"
#import "DriveDescriptionData.h"
#import "DBTransactionManager.h"
#import "RouteSnapshotData.h"
#import "UserLevelTools.h"

@implementation AppDataManager

//private



//public

@synthesize userData = _userData;
@synthesize driveCharacteristic = _driveCharacteristic;
@synthesize driveDescription = _driveDescription;
@synthesize totalDistance = _totalDistance;
@synthesize totalExperience = _totalExperience;
@synthesize levelData = _levelData;
// REMOVE @synthesize currentLevel = _currentLevel;
// REMOVE @synthesize currentLevelThreshold = _currentLevelThreshold;
// REMOVE @synthesize nextLevelThreshold = _nextLevelThreshold;

#pragma mark Singleton Methods

+ (id) appDataManager {
    static AppDataManager *appDataManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appDataManager = [[self alloc] init];
    });
    
    return appDataManager;
}

#pragma mark Public Methods

- (void) setupUserFromFBData:(FBSDKProfile *) fbData {
    UserData *ud = [DBTransactionManager getUserDataByFBUserId:fbData.userID];
    
    if(ud == nil) {
        ud = [[UserData alloc] init];
        ud.fbId = fbData.userID;
        ud.firstname = fbData.firstName;
        ud.surname = fbData.lastName;
        ud.userId = [DBTransactionManager saveUser:ud];
    }
    
    self.userData = ud;
    
    self.driveCharacteristic = DriveCharacteristic_PERFORMANCE;
    
    self.snapshotArray = [DBTransactionManager getSavedSnapshotsForUser: self.userData.userId];
    [self updateTotalExperience];
    [self updateTotalDistance];
    [self updateUserLevelStats];
    [self updateDriveDescription];
}

/*- (void) setupForUser:(int) userId {
    self.userId = userId;
    self.driveCharacteristic = DriveCharacteristic_ECO;
    
    self.snapshotArray = [DBTransactionManager getSavedSnapshotsForUser:userId];
    [self updateTotalExperience];
    [self updateTotalDistance];
    [self updateUserLevelStats];
    [self updateDriveDescription];
}*/

- (void) reset {
    self.userData = nil;
    self.driveCharacteristic = DriveCharacteristic_PERFORMANCE;
    
    self.snapshotArray = nil;
    self.totalExperience = 0;
    
    [self updateDriveDescription];
}

- (void) updateTotalExperience {
    self.totalExperience = [DBTransactionManager countExperiencePointsForUser:self.userData.userId];
}

- (void) updateTotalDistance {
    self.totalDistance = [DBTransactionManager countTotalDistanceForUser:self.userData.userId];
}

- (void) updateUserLevelStats {
    self.levelData = [UserLevelTools userLevelDataForExp:self.totalExperience];
    
    // REMOVE self.currentLevel = (int)ceil(self.totalExperience / 100.0f);
    // REMOVE self.currentLevelThreshold = self.currentLevel * 100 - 100;
    // REMOVE self.nextLevelThreshold = self.currentLevelThreshold + 100;
}

- (void) narrowSnaphotsToLocation: (CLLocation *) location {
    int distance;
    RouteSnapshotData *rsd;
    NSMutableArray *narrowedSnaps = [[NSMutableArray alloc] init];
    
    int i = -1;
    int q = (int)[self.snapshotArray count];
    
    while (++i < q) {
        rsd = [self.snapshotArray objectAtIndex:i];
        distance = [location distanceFromLocation:[[CLLocation alloc] initWithLatitude:rsd.coordinate.latitude longitude:rsd.coordinate.longitude]];
        
        if(distance < 2000) {
            [narrowedSnaps addObject:rsd];
        }
    }
    
    self.narrowedSnapshotArray = nil;
    self.narrowedSnapshotArray = [narrowedSnaps copy];
}

//- (DriveDescriptionData *) driveDescription {
- (void) updateDriveDescription {
    DriveDescriptionData *ddd = nil;
    
    if(self.driveCharacteristic < 0) {
        ddd = [DriveDescriptionData ecoSet];
    } else if (self.driveCharacteristic > 0) {
        ddd = [DriveDescriptionData performanceSet];
    } else {
        ddd = [DriveDescriptionData neutralSet];
    }
    
    NSLog(@"updateDriveDescription driveCharacteristic: %d", self.driveCharacteristic);
    
    /*CGFloat hue;
    CGFloat saturation;
    CGFloat brightness;
    CGFloat alpha;
    BOOL success = [ddd.color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    NSLog(@"success: %i hue: %0.2f, saturation: %0.2f, brightness: %0.2f, alpha: %0.2f", success, hue, saturation, brightness, alpha);*/
    
    _driveDescription = ddd;
}

- (NSArray *) getHistoryOfDrives {
    return [DBTransactionManager getLightDriveHistoryForUser:self.userData.userId];
}

#pragma mark - Units

- (NSString *) getUserDistanceUnitLabel {
    return [NSString stringWithFormat:@"km"];
}

- (float) convertDistanceToUserUnit: (int) distance {
    float ud = (distance / 1000.0f);
    return ud;
}

- (float) convertSpeedToUserUnit: (float) speed { // meter per second input
    float us = speed * 3.6f;
    
    if(us < 0.0f) us = 0.0f;
    
    //NSLog(@"c speed: %f %f", us, speed);
    
    return us;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
