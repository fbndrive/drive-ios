#import <AVFoundation/AVFoundation.h>
#import "SoundManager.h"
#import "SoundTools.h"

@interface SoundManager ()

@property (nonatomic,strong) AVQueuePlayer *avPlayer;

@end

@implementation SoundManager

#pragma mark Singleton Methods

+ (id) soundManager {
    static SoundManager *soundManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        soundManager = [[self alloc] init];
    });
    
    return soundManager;
}

#pragma mark Public Methods

/*- (void) playTheNumber: (int) number {
    AVMutableComposition *composition = [self convertInt:number intoComposition:nil];
    AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithAsset:composition];
    
    [self playItem:playerItem];
}*/

- (void) playTheSentence: (NSString *) sentence {
    AVMutableComposition *composition = [self convertString:sentence intoComposition:nil];
    AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithAsset:composition];
    
    [self playItem:playerItem];
}

#pragma mark Private Methods

- (void) playItem: (AVPlayerItem *) item {
    if(_avPlayer == nil) {
        _avPlayer = [[AVQueuePlayer alloc] init];
    }
    
    [_avPlayer removeAllItems];
    [_avPlayer insertItem:item afterItem:nil];
    [_avPlayer play];
}

/*- (AVMutableComposition *) convertInt: (int) integer intoComposition: (AVMutableComposition *) composition {
    NSString *literal = [SoundTools numberToLiteral:integer];
    AVMutableCompositionTrack *audioTrack;
    
    if(composition == nil) {
        composition = [[AVMutableComposition alloc] init];
        audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    } else {
        audioTrack = [composition.tracks objectAtIndex:0];
        
        if(audioTrack == nil) {
            audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        }
    }
    
    NSArray *wordArray = [literal componentsSeparatedByString:@" "];
    NSString *word;
    
    NSString *sound;
    NSURL *url;
    
    NSError *error = nil;
    CMTime nextClipStartTime = audioTrack.timeRange.duration;
    AVAsset *avAsset;
    CMTime avAssetDuration;
    CMTimeRange timeRangeInAsset;
    AVAssetTrack *clipAudioTrack;
    
    int i = -1;
    int q = (int)[wordArray count];
    
    while (++i < q) {
        word = [wordArray objectAtIndex:i];
        
        sound = [[NSBundle mainBundle] pathForResource: @"no-%@" ofType: @"wav"];
        if(sound == nil) {
            NSLog(@"no sound file of a name %@ found in resources", [NSString stringWithFormat:@"no-%@", word]);
            continue;
        }
        
        url = [NSURL fileURLWithPath:sound];
        
        avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
        avAssetDuration = [avAsset duration];
        timeRangeInAsset = CMTimeRangeMake(kCMTimeZero, avAssetDuration);
        clipAudioTrack = [[avAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
        
        [audioTrack insertTimeRange:timeRangeInAsset ofTrack:clipAudioTrack atTime:nextClipStartTime error:&error];
        
        nextClipStartTime = audioTrack.timeRange.duration;
    }
    
    return composition;
}*/

- (AVMutableComposition *) convertString: (NSString *) string intoComposition: (AVMutableComposition *) composition {
    
    AVMutableCompositionTrack *audioTrack;
    
    if(composition == nil) {
        composition = [[AVMutableComposition alloc] init];
        audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    } else {
        audioTrack = [composition.tracks objectAtIndex:0];
        
        if(audioTrack == nil) {
            audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        }
    }
    
    NSArray *wordArray = [string componentsSeparatedByString:@" "];
    NSString *word;
    
    NSString *sound;
    NSURL *url;
    
    NSError *error = nil;
    CMTime nextClipStartTime = audioTrack.timeRange.duration; //kCMTimeZero;
    AVAsset *avAsset;
    CMTime avAssetDuration;
    CMTimeRange timeRangeInAsset;
    AVAssetTrack *clipAudioTrack;
    
    int i = -1;
    int q = (int)[wordArray count];
    
    while (++i < q) {
        word = [wordArray objectAtIndex:i];
        
        if ([[word substringToIndex:3] isEqualToString:@"@no"]) {
            [self convertString:[SoundTools integerToLiteral:[[word substringFromIndex:3] intValue]] intoComposition:composition];
            nextClipStartTime = audioTrack.timeRange.duration;
            continue;
        }
        
        sound = [[NSBundle mainBundle] pathForResource: word ofType: @"wav"];
        if(sound == nil) {
            sound = [[NSBundle mainBundle] pathForResource: word ofType: @"m4a"];
            if(sound == nil) {
                NSLog(@"no sound file of a name %@ found in resources", word);
                continue;
            }
        }
        
        url = [NSURL fileURLWithPath:sound];
        
        avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
        avAssetDuration = [avAsset duration];
        timeRangeInAsset = CMTimeRangeMake(kCMTimeZero, avAssetDuration);
        clipAudioTrack = [[avAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
        
        [audioTrack insertTimeRange:timeRangeInAsset ofTrack:clipAudioTrack atTime:nextClipStartTime error:&error];
        
        nextClipStartTime = audioTrack.timeRange.duration; //CMTimeAdd(nextClipStartTime, avAssetDuration);
    }
    
    return composition;
}

@end
