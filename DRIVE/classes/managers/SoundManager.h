#import <Foundation/Foundation.h>

@interface SoundManager : NSObject

+ (id) soundManager;

//- (void) playTheNumber: (int) number;
- (void) playTheSentence: (NSString *) sentence;

@end
