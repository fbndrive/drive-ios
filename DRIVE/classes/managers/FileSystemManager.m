#import "FileSystemManager.h"

@implementation FileSystemManager

NSString * const Map_images_subfolder = @"/MapImages";

+(void) createRequiredFolders {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent: Map_images_subfolder];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
}

+(BOOL) saveHistoryDriveMapImage: (UIImage *) image forDriveData:(DriveData *) driveData {
    NSString  *imagePath = [FileSystemManager getHistoryDriveMapImagePathForDriveData: driveData];
    
    return [UIImagePNGRepresentation(image) writeToFile:imagePath atomically:YES];
}

+(BOOL) saveHistoryListMapImage: (UIImage *) image forDriveData:(DriveData *) driveData {
    NSString  *imagePath = [FileSystemManager getHistoryListMapImagePathForDriveData: driveData];
    
    return [UIImagePNGRepresentation(image) writeToFile:imagePath atomically:YES];
}

+(UIImage *) getHistoryDriveMapImageForDriveData:(DriveData *) driveData {
    NSString  *imagePath = [FileSystemManager getHistoryDriveMapImagePathForDriveData: driveData];
    
    return [UIImage imageWithContentsOfFile:imagePath];
}

+(UIImage *) getHistoryListMapImageForDriveData:(DriveData *) driveData {
    NSString  *imagePath = [FileSystemManager getHistoryListMapImagePathForDriveData: driveData];
    
    return [UIImage imageWithContentsOfFile:imagePath];
}

+(NSString *) getHistoryDriveMapImagePathForDriveData:(DriveData *) driveData {
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    NSString *filename = [NSString stringWithFormat:@"%@%@/HistoryDriveMapImg_%d_%d%@.png", docsPath, Map_images_subfolder, driveData.driveId, driveData.userId, [self getSuffixForScreenScale]];
    
    //NSLog(@"filename: %@", filename);
    
    return filename;
}

+(NSString *) getHistoryListMapImagePathForDriveData:(DriveData *) driveData {
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    NSString *filename = [NSString stringWithFormat:@"%@%@/HistoryListMapImg_%d_%d%@.png", docsPath, Map_images_subfolder, driveData.driveId, driveData.userId, [self getSuffixForScreenScale]];
    
    //NSLog(@"filename: %@", filename);
    
    return filename;
}

+(NSString *) getSuffixForScreenScale {
    NSString *scaleSuffix = @"";
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2) {
        scaleSuffix = @"@2x";
    }
    
    return scaleSuffix;
}

@end
