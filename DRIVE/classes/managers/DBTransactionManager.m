#import "Constants.h"
#import <CoreLocation/CoreLocation.h>
#import "DBTransactionManager.h"
#import "DriveData.h"
#import "DriveTrackpointData.h"
#import "DriveTrackpointCollection.h"
#import "ExperienceComponentData.h"
#import "DateTools.h"
#import "RouteSpecialStageData.h"
#import "RouteSnapshotData.h"
#import "FMDB.h"

@implementation DBTransactionManager

// RouteData

+(int) saveUser:(UserData *) userData {
    BOOL dbResult = NO;
    int addedRecordId = -1;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSString *nowDatetime = [DateTools getTimestampStringFromDate: [[NSDate alloc] init]];
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              userData.fbId, @"fbId",
                              userData.firstname, @"firstname",
                              userData.surname, @"surname",
                              [NSNumber numberWithInt:1], @"origin",
                              nowDatetime, @"createdDate",
                              nil];
    
    dbResult = [db executeUpdate:@"INSERT INTO users (fbId, firstname, surname, origin, createdDate) VALUES (:fbId, :firstname, :surname, :origin, :createdDate)" withParameterDictionary:argsDict];
    
    if(dbResult) {
        NSLog(@"New user with Facebook ID (%@) was sucesfully added", userData.fbId);
        addedRecordId = (int)[db lastInsertRowId];
    }
    
    [DBTransactionManager logDB:db tableCount:@"users"];
    
    [db close];
    
    return addedRecordId;
}

+(UserData *) getUserDataByFBUserId:(NSString *) fbId {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    
    if (![db open]) {
        return nil;
    }
    
    UserData *ud = nil;
    
    NSString *query = [NSString stringWithFormat:@"SELECT rowid, fbId, firstname, surname, createdDate FROM users where fbId = '%@'", fbId];
    FMResultSet *s = [db executeQuery:query];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:Database_dateTimezone];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:timeZone];
    [format setDateFormat:Database_dateFormat];
    
    while ([s next]) {
        ud = [[UserData alloc] init];
        ud.userId = [s intForColumn:@"rowid"];
        ud.fbId = [s stringForColumn:@"fbId"];
        ud.firstname = [s stringForColumn:@"firstname"];
        ud.surname = [s stringForColumn:@"surname"];
        ud.createdDate = [format dateFromString:[s stringForColumn:@"createdDate"]];
        
        break;
    }
    
    return ud;
}

+(int) saveDriveDataOnStart:(DriveData *) driveData forUser: (int) userId {
    int addedRecordId = -1;
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSString *nowDatetime = [DateTools getTimestampStringFromDate:[[NSDate alloc] init]];
    NSString *startDatetime = [DateTools getTimestampStringFromDate: driveData.startDate];
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              startDatetime , @"startDate",
                              [NSNumber numberWithInt: driveData.distance], @"distance",
                              [NSNumber numberWithInt: driveData.experience], @"experience",
                              [NSNumber numberWithFloat: driveData.southWest.latitude], @"sw_lat",
                              [NSNumber numberWithFloat: driveData.southWest.longitude], @"sw_lon",
                              [NSNumber numberWithFloat: driveData.northEast.latitude], @"ne_lat",
                              [NSNumber numberWithFloat: driveData.northEast.longitude], @"ne_lon",
                              [NSNumber numberWithInt: driveData.finished], @"finished",
                              [NSNumber numberWithInt: driveData.parsed], @"parsed",
                              [NSNumber numberWithInt: userId], @"userId",
                              [NSNumber numberWithInt: 1], @"origin",
                              nowDatetime, @"createdDate",
                              nil];
    
    dbResult = [db executeUpdate:@"INSERT INTO drives (startDate, distance, experience, sw_lat, sw_lon, ne_lat, ne_lon, finished, parsed, userId, origin, createdDate) VALUES (:startDate, :distance, :experience, :sw_lat, :sw_lon, :ne_lat, :ne_lon, :finished, :parsed, :userId, :origin, :createdDate)" withParameterDictionary:argsDict];
    
    if(dbResult) {
        addedRecordId = (int)[db lastInsertRowId];
        driveData.driveId = addedRecordId;
        NSLog(@"Initial DriveData successfully saved with id %d", addedRecordId);
    } else {
        NSLog(@"Initial DriveData failed with error: %@", [db lastErrorMessage]);
    }
    
    [db close];
    
    return addedRecordId;
}

/*+(int) saveDriveData:(DriveData *) driveData forUser: (int) userId {
    int addedRecordId = -1;
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSString *nowDatetime = [DateTools getTimestampStringFromDate:[[NSDate alloc] init]];
    NSString *startDatetime = [DateTools getTimestampStringFromDate: driveData.startDate];
    NSString *finishDatetime = [DateTools getTimestampStringFromDate: driveData.finishDate];
    
    NSError *jsonError;
    NSData *jsonAttributesData = [NSJSONSerialization dataWithJSONObject:driveData.attributes
                                                       options:NSJSONWritingPrettyPrinted error:&jsonError];
    NSString *jsonAttributesString = [[NSString alloc] initWithData:jsonAttributesData encoding:NSUTF8StringEncoding];
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              jsonAttributesString, @"attributes",
                              startDatetime , @"startDate",
                              finishDatetime , @"finishDate",
                              [NSNumber numberWithInt: driveData.distance], @"distance",
                              [NSNumber numberWithInt: driveData.experience], @"experience",
                              [NSNumber numberWithFloat: driveData.southWest.latitude], @"sw_lat",
                              [NSNumber numberWithFloat: driveData.southWest.longitude], @"sw_lon",
                              [NSNumber numberWithFloat: driveData.northEast.latitude], @"ne_lat",
                              [NSNumber numberWithFloat: driveData.northEast.longitude], @"ne_lon",
                              [NSNumber numberWithInt: driveData.finished], @"finished",
                              [NSNumber numberWithInt: driveData.parsed], @"parsed",
                              [NSNumber numberWithInt:userId], @"userId",
                              [NSNumber numberWithInt:1], @"origin",
                              nowDatetime, @"createdDate",
                              nil];
    
    dbResult = [db executeUpdate:@"INSERT INTO drives (attributes, startDate, finishDate, distance, experience, sw_lat, sw_lon, ne_lat, ne_lon, finished, parsed, userId, origin, createdDate) VALUES (:attributes, :startDate, :finishDate, :distance, :experience, :sw_lat, :sw_lon, :ne_lat, :ne_lon, :finished, :parsed, :userId, :origin, :createdDate)" withParameterDictionary:argsDict];
    
    if(dbResult) {
        addedRecordId = (int)[db lastInsertRowId];
        driveData.driveId = addedRecordId;
        NSLog(@"Route started on %@ successfully saved", driveData.startDate);
    } else {
        NSLog(@"Method saveDriveData query failed with error: %@", [db lastErrorMessage]);
    }
    
    //[DBTransactionManager logDB:db tableCount:@"drives"];
    
    [db close];
    
    return addedRecordId;
}*/

+(BOOL) updateDriveDataOnFinish: (DriveData *) driveData {
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSError *jsonError;
    NSData *jsonAttributesData = [NSJSONSerialization dataWithJSONObject:driveData.attributes
                                                                 options:NSJSONWritingPrettyPrinted error:&jsonError];
    NSString *jsonAttributesString = [[NSString alloc] initWithData:jsonAttributesData encoding:NSUTF8StringEncoding];
    
    NSString *finishDatetime = [DateTools getTimestampStringFromDate: driveData.finishDate];
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithInt: driveData.driveId], @"driveId",
                              jsonAttributesString, @"attributes",
                              finishDatetime , @"finishDate",
                              [NSNumber numberWithInt: driveData.distance], @"distance",
                              [NSNumber numberWithInt: driveData.experience], @"experience",
                              [NSNumber numberWithFloat: driveData.southWest.latitude], @"sw_lat",
                              [NSNumber numberWithFloat: driveData.southWest.longitude], @"sw_lon",
                              [NSNumber numberWithFloat: driveData.northEast.latitude], @"ne_lat",
                              [NSNumber numberWithFloat: driveData.northEast.longitude], @"ne_lon",
                              [NSNumber numberWithInt: driveData.finished], @"finished",
                              [NSNumber numberWithInt: driveData.parsed], @"parsed",
                              nil];
    
    dbResult = [db executeUpdate:@"UPDATE drives SET attributes = :attributes, finishDate = :finishDate, distance = :distance, experience = :experience, sw_lat = :sw_lat, sw_lon = :sw_lon, ne_lat = :ne_lat, ne_lon = :ne_lon, finished = :finished, parsed = :parsed WHERE rowId = :driveId" withParameterDictionary:argsDict];
    
    if(dbResult) {
        NSLog(@"Drive successfully updated %d", driveData.driveId);
    } else {
        NSLog(@"Drive failed update with error: %@", [db lastErrorMessage]);
    }
    
    [db close];
    
    return dbResult;
}

+(BOOL) updateUserDriveDataAttributes:(DriveData *) driveData {
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSError *jsonError;
    NSData *jsonAttributesData = [NSJSONSerialization dataWithJSONObject:driveData.attributes
                                                                 options:NSJSONWritingPrettyPrinted error:&jsonError];
    NSString *jsonAttributesString = [[NSString alloc] initWithData:jsonAttributesData encoding:NSUTF8StringEncoding];
    NSLog(@"json: %@", jsonAttributesString);
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithInt: driveData.driveId], @"driveId",
                              jsonAttributesString, @"attributes",
                              nil];
    dbResult = [db executeUpdate:@"UPDATE drives SET attributes = :attributes WHERE rowId = :driveId" withParameterDictionary:argsDict];
    
    if(dbResult) {
        //addedRecordId = (int)[db lastInsertRowId];
        NSLog(@"Attributes updated successfully");
    } else {
        NSLog(@"Attributes updated failed with error: %@", [db lastErrorMessage]);
    }
    
    [db close];
    
    return dbResult;
}

+(int) saveDriveTrackpoints:(NSArray *) trackpoints andLastSavedTrackpointId: (int) tid {
    BOOL dbResult = NO;
    
    int addedRecordId = tid;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    int i = -1;
    int q = (int)[trackpoints count];
    DriveTrackpointData *dtd;
    NSDictionary *argsDict;
    NSString *createDate;
    
    [db beginTransaction];
    
    while(++i < q) {
        dtd = [trackpoints objectAtIndex: i];
        
        createDate = [DateTools getTimestampStringFromDate: dtd.createDate];
        
        argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                    [NSNumber numberWithInt: dtd.driveId], @"driveId",
                    [NSNumber numberWithFloat: dtd.latitude], @"latitude",
                    [NSNumber numberWithFloat: dtd.longitude], @"longitude",
                    [NSNumber numberWithFloat: dtd.elevation], @"elevation",
                    [NSNumber numberWithFloat: dtd.course], @"course",
                    [NSNumber numberWithFloat: dtd.speed], @"speed",
                    [NSNumber numberWithInt: addedRecordId], @"parentTrackpoint",
                    createDate, @"createDate",
                    nil];
        
        dbResult = [db executeUpdate:@"INSERT INTO driveTrackpoints (driveId, latitude, longitude, elevation, course, speed, parentTrackpoint, createDate) VALUES (:driveId, :latitude, :longitude, :elevation, :course, :speed, :parentTrackpoint, :createDate)" withParameterDictionary:argsDict];
        
        if(!dbResult) {
            NSLog(@"Method saveDriveTrackpoints failed with error: %@", [db lastErrorMessage]);
            break;
        }
        
        addedRecordId = (int)[db lastInsertRowId];
    }
    
    if(dbResult) {
        addedRecordId = (int)[db lastInsertRowId];
        [db commit];
        
        NSLog(@"Saved %d trackpoints", q);
    } else {
        [db rollback];
    }
    
    [db close];
    
    return addedRecordId;
}

+(NSArray *) getLightDriveHistoryForUser:(int) userId {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return nil;
    }
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    DriveData *dd;
    
    NSString *query = [NSString stringWithFormat:@"SELECT rowid, attributes, startDate, finishDate, distance, experience, sw_lat, sw_lon, ne_lat, ne_lon, finished, parsed, userId, origin, createdDate FROM drives where userId = %d ORDER BY datetime(startDate) DESC", userId];
    FMResultSet *s = [db executeQuery:query];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:Database_dateTimezone];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:timeZone];
    [format setDateFormat:Database_dateFormat];
    
    //int randNum = rand() % ([max intValue] - [min intValue]) + [min intValue];
    
    NSError *jsonError;
    NSData *jsonData;
    
    while ([s next]) {
        dd = [[DriveData alloc] init];
        
        dd = [[DriveData alloc] init];
        dd.driveId = [s intForColumn:@"rowid"];
        
        jsonData = [[s stringForColumn:@"attributes"] dataUsingEncoding:NSUTF8StringEncoding];
        if(jsonData != nil) {
            dd.attributes = [NSJSONSerialization JSONObjectWithData:jsonData
                                        options:NSJSONReadingMutableContainers
                                          error:&jsonError];
        }
        
        dd.startDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"startDate"]];
        dd.finishDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"finishDate"]];
        dd.distance = [s intForColumn:@"distance"];
        dd.experience = [s intForColumn:@"experience"];
        dd.southWest = CLLocationCoordinate2DMake([s doubleForColumn:@"sw_lat"], [s doubleForColumn:@"sw_lon"]);
        dd.northEast = CLLocationCoordinate2DMake([s doubleForColumn:@"ne_lat"], [s doubleForColumn:@"ne_lon"]);
        dd.finished = [s intForColumn:@"finished"];
        dd.parsed = [s intForColumn:@"parsed"];
        dd.userId = [s intForColumn:@"userId"];
        dd.createdDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"createdDate"]];
        
        [resultArray addObject:dd];
    }
    
    NSLog(@"There are %lu drives saved for this user", (unsigned long)[resultArray count]);
    
    return [resultArray copy];
}

+(DriveData *) getLightDriveDataByDriveId:(int) driveId {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    
    if (![db open]) {
        return nil;
    }
    
    DriveData *dd = nil;
    
    NSString *query = [NSString stringWithFormat:@"SELECT rowid, attributes, startDate, finishDate, distance, experience, sw_lat, sw_lon, ne_lat, ne_lon, finished, parsed, userId, origin, createdDate FROM drives where rowid = %d", driveId];
    FMResultSet *s = [db executeQuery:query];
    
    while ([s next]) {
        dd = [[DriveData alloc] init];
        dd.driveId = [s intForColumn:@"rowid"];
        dd.attributes = [s objectForColumnName:@"attributes"];
        dd.startDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"startDate"]];
        dd.finishDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"finishDate"]];
        dd.distance = [s intForColumn:@"distance"];
        dd.experience = [s intForColumn:@"experience"];
        dd.southWest = CLLocationCoordinate2DMake([s doubleForColumn:@"sw_lat"], [s doubleForColumn:@"sw_lon"]);
        dd.northEast = CLLocationCoordinate2DMake([s doubleForColumn:@"ne_lat"], [s doubleForColumn:@"ne_lon"]);
        dd.finished = [s intForColumn:@"finished"];
        dd.parsed = [s intForColumn:@"parsed"];
        dd.userId = [s intForColumn:@"userId"];
        dd.createdDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"createdDate"]];
        
        break;
    }
    
    return dd;
}

+ (DriveTrackpointData *) getFirstDriveTrackpointByDriveId: (int) driveId {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    
    if (![db open]) {
        return nil;
    }
    
    DriveTrackpointData *dtd = nil;
    
    NSString *query = [NSString stringWithFormat:@"SELECT latitude, longitude, createDate FROM driveTrackpoints where driveId = %d order by createDate limit 1", driveId];
    FMResultSet *s = [db executeQuery:query];
    
    while ([s next]) {
        dtd = [[DriveTrackpointData alloc] init];
        //dtd.driveId = [s intForColumn:@"rowid"];
        //dd.attributes = [s objectForColumnName:@"attributes"];
        //dd.startDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"startDate"]];
        dtd.latitude = [s doubleForColumn: @"latitude"];
        dtd.longitude = [s doubleForColumn: @"longitude"];
        
        //NSLog(@"lat %f lon %f", dtd.latitude, dtd.longitude);
        
        break;
    }
    
    return dtd;
}

+ (DriveTrackpointData *) getLastDriveTrackpointByDriveId: (int) driveId {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    
    if (![db open]) {
        return nil;
    }
    
    DriveTrackpointData *dtd = nil;
    
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM driveTrackpoints where driveId = %d order by createDate desc limit 1", driveId];
    FMResultSet *s = [db executeQuery:query];
    
    while ([s next]) {
        dtd = [[DriveTrackpointData alloc] init];
        //dtd.driveId = [s intForColumn:@"rowid"];
        //dd.attributes = [s objectForColumnName:@"attributes"];
        //dd.startDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"startDate"]];
        dtd.latitude = [s doubleForColumn: @"latitude"];
        dtd.longitude = [s doubleForColumn: @"longitude"];
        
        //NSLog(@"lat %f lon %f", dtd.latitude, dtd.longitude);
        
        break;
    }
    
    return dtd;
}


+(DriveTrackpointCollection *) getDriveTrackpointCollectionByDriveId:(int) driveId {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    
    if (![db open]) {
        return nil;
    }
    
    DriveTrackpointData *dtd = nil;
    DriveTrackpointCollection * dtc = [[DriveTrackpointCollection alloc] init];
    
    NSString *query = [NSString stringWithFormat:@"SELECT latitude, longitude, createDate FROM driveTrackpoints where driveId = %d order by createDate", driveId];
    FMResultSet *s = [db executeQuery:query];
    
    NSMutableArray *tp = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        dtd = [[DriveTrackpointData alloc] init];
        //dtd.driveId = [s intForColumn:@"rowid"];
        //dd.attributes = [s objectForColumnName:@"attributes"];
        //dd.startDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"startDate"]];
        dtd.latitude = [s doubleForColumn: @"latitude"];
        dtd.longitude = [s doubleForColumn: @"longitude"];
        
        //NSLog(@"lat %f lon %f", dtd.latitude, dtd.longitude);
        
        [tp addObject: dtd];
    }
    
    dtc.trackpoints = [tp copy];
    
    return dtc;
}

+(DriveTrackpointCollection *) getDriveTrackpointByRegion:(RMSphericalTrapezium) region interval: (int) interval andDriveId: (int) driveId andTranslateToMap: (RMMapView *) mapView {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    
    if (![db open]) {
        return nil;
    }
    
    DriveTrackpointData *dtd = nil;
    DriveTrackpointCollection * dtc = [[DriveTrackpointCollection alloc] init];
    
    NSString *query = [NSString stringWithFormat:@"SELECT latitude, longitude, parentTrackpoint, createDate FROM driveTrackpoints where driveId=%d and latitude <= %f and latitude >= %f and longitude <= %f and longitude >= %f and (rowid %% %d = 0)", driveId, region.northEast.latitude, region.southWest.latitude, region.northEast.longitude, region.southWest.longitude, interval];
    
    FMResultSet *s = [db executeQuery:query];
    
    NSMutableArray *tp = [[NSMutableArray alloc] init];
    
    while ([s next]) {
        dtd = [[DriveTrackpointData alloc] init];
        //dtd.driveId = [s intForColumn:@"rowid"];
        //dd.attributes = [s objectForColumnName:@"attributes"];
        //dd.startDate = [DateTools getDateFromTimestampString: [s stringForColumn:@"startDate"]];
        dtd.latitude = [s doubleForColumn: @"latitude"];
        dtd.longitude = [s doubleForColumn: @"longitude"];
        dtd.parentTrackpoint = [s intForColumn:@"parentTrackpoint"];
        
        if(mapView) {
            dtd.mapPosition = [mapView coordinateToPixel: CLLocationCoordinate2DMake(dtd.latitude, dtd.longitude)];
        }
               
        [tp addObject: dtd];
    }
    
    dtc.trackpoints = [tp copy];
    
    //NSLog(@"db returned %lu trackpoints within the given region", (unsigned long)[dtc.trackpoints count]);
    
    return dtc;
}

+(int) countTotalDistanceForUser:(int) userId {
    //TODO: validate snapshot
    
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    int totalDistance = 0;
    
    NSString *query = [NSString stringWithFormat:@"SELECT SUM(distance) from drives where userId = %d", userId];
    FMResultSet *s = [db executeQuery:query];
    
    if ([s next]) {
        totalDistance = [s intForColumnIndex:0];
    }
    
    NSLog(@"Total distance is %d", totalDistance);
    
    [db close];
    
    return totalDistance;
}

//RouteSnapshotData

+(BOOL) saveSnapshot:(RouteSnapshotData *) rsd {
    //TODO: validate snapshot
    
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSString *nowDatetime = [DateTools getTimestampStringFromDate: [[NSDate alloc] init]];
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithFloat:rsd.coordinate.latitude], @"latitude",
                              [NSNumber numberWithFloat:rsd.coordinate.longitude], @"longitude",
                              [NSNumber numberWithInt:rsd.userId], @"userId",
                              [NSNumber numberWithInt:1], @"origin",
                              nowDatetime, @"createdDate",
                              nil];
    
    dbResult = [db executeUpdate:@"INSERT INTO snapshots (latitude, longitude, userId, origin, createdDate) VALUES (:latitude, :longitude, :userId, :origin, :createdDate)" withParameterDictionary:argsDict];
    
    if(dbResult) {
        NSLog(@"Snapshot (%f , %f) successfully saved", rsd.coordinate.latitude, rsd.coordinate.longitude);
    }
    
    //[DBTransactionManager logDB:db tableCount:@"snapshots"];
    
    [db close];
    
    return dbResult;
}

+(NSArray *) getSavedSnapshotsForUser:(int) userId {
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return nil;
    }
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    RouteSnapshotData *rsd;
    
    NSString *query = [NSString stringWithFormat:@"SELECT rowid, latitude, longitude, userId FROM snapshots where userId = %d", userId];
    FMResultSet *s = [db executeQuery:query];
    
    while ([s next]) {
        rsd = [[RouteSnapshotData alloc] init];
        rsd.id = [s intForColumn:@"rowid"];
        rsd.coordinate = CLLocationCoordinate2DMake([s doubleForColumn:@"latitude"], [s doubleForColumn:@"longitude"]);
        
        [resultArray addObject:rsd];
    }
    
    //NSLog(@"There are %lu snapshots saved for this user", (unsigned long)[resultArray count]);
    
    return [resultArray copy];
}

+(BOOL) saveSpecialStage:(RouteSpecialStageData *) rssd {
    //TODO: validate special stage
    
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSString *nowDatetime = [DateTools getTimestampStringFromDate:[[NSDate alloc] init]];
    
    NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithFloat:rssd.startCoordinate.latitude], @"startLatitude",
                              [NSNumber numberWithFloat:rssd.startCoordinate.longitude], @"startLongitude",
                              [NSNumber numberWithFloat:rssd.finishCoordinate.latitude], @"finishLatitude",
                              [NSNumber numberWithFloat:rssd.finishCoordinate.longitude], @"finishLongitude",
                              rssd.getGPXString, @"gpx",
                              [NSNumber numberWithInt:rssd.userId], @"userId",
                              [NSNumber numberWithInt:1], @"origin",
                              nowDatetime, @"createdDate",
                              nil];
    
    dbResult = [db executeUpdate:@"INSERT INTO specialstages (startLatitude, startLongitude, finishLatitude, finishLongitude, gpx, userId, origin, createdDate) VALUES (:startLatitude, :startLongitude, :finishLatitude, :finishLongitude,, :gpx, :userId, :origin, :createdDate)" withParameterDictionary:argsDict];
    
    if(dbResult) {
        NSLog(@"Special stage (%f , %f) successfully saved", rssd.startCoordinate.latitude, rssd.startCoordinate.longitude);
    }
    
    [DBTransactionManager logDB:db tableCount:@"specialstages"];
    
    [db close];
    
    return dbResult;
}


//EXPERIENCE COMPONENT

+(BOOL) saveExpComponents:(NSMutableArray *) components forUser:(int) userId andDrive: (int) driveId {
    //TODO: validate snapshot
    
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    NSString *nowDatetime = [DateTools getTimestampStringFromDate: [[NSDate alloc] init]];
    
    int i = -1;
    long q = [components count];
    ExperienceComponentData *ecd;
    NSString *query = @"";
    
    while(++i < q) {
        ecd = [components objectAtIndex:i];
        
        query = [query stringByAppendingString:[NSString stringWithFormat:@"INSERT INTO experienceComponents (points, description, userId, driveId, origin, createdDate) VALUES (%d, '%@', %d, %d, 1, '%@')", ecd.points, ecd.description, userId, driveId, nowDatetime]];
    }
    
    dbResult = [db executeStatements:query];
    
    if(dbResult) {
        NSLog(@"Experience components (%ld) successfully saved", q);
    }
    
    //[DBTransactionManager logDB:db tableCount:@"experienceComponents"];
    
    [db close];
    
    return dbResult;

}

+(int) countExperiencePointsForUser:(int) userId {
    //TODO: validate snapshot
    
    BOOL dbResult = NO;
    
    FMDatabase *db = [DBTransactionManager getDBConnection];
    if (![db open]) {
        return dbResult;
    }
    
    int pointsSum = 0;
    
    //NSString *query = [NSString stringWithFormat:@"SELECT SUM(points) from experienceComponents where userId = %d", userId];
    NSString *query = [NSString stringWithFormat:@"SELECT SUM(experience) from drives where userId = %d", userId];
    FMResultSet *s = [db executeQuery:query];
    
    if ([s next]) {
        pointsSum = [s intForColumnIndex:0];
    }
    
    [db close];
    
    return pointsSum;
}

+(BOOL) updateDBFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSString *dbPath = [DBTransactionManager getDBPath];
    
    if ([fileManager fileExistsAtPath:dbPath] == YES) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *appDBVersion = [defaults objectForKey:@"dbversion"];
        NSString *currentVersion = Database_version;
        
        if (appDBVersion == nil || [appDBVersion compare:currentVersion options:NSNumericSearch] == NSOrderedAscending) {
            
            //TODO here we should run update insted of removal
            
            [fileManager removeItemAtPath:dbPath error:&error];
            
            [defaults setObject:currentVersion forKey:@"dbversion"];
            [defaults synchronize];
        } else {
            return NO;
        }
    }
    
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"local-db" ofType:@"db"];
    [fileManager copyItemAtPath:resourcePath toPath:dbPath error:&error];
    
    return YES;
}

+(NSString *) getDBPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"local-db.db"];
    
    return dbPath;
}

// private static

+ (float) getDBVersion {
    return 0.1f;
}

+ (FMDatabase *) getDBConnection {
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *dbPath   = [docsPath stringByAppendingPathComponent:@"local-db.db"];
    FMDatabase *db     = [FMDatabase databaseWithPath:dbPath];
    
    return db;
}

+ (void) logDB: (FMDatabase*)db tableCount: (NSString *) table {
    NSString *query = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@", table];
    FMResultSet *s = [db executeQuery:query];
    
    if ([s next]) {
        int totalCount = [s intForColumnIndex:0];
        NSLog(@"Table %@ count is: %d", table, totalCount);
    }

}

@end
