#import <Foundation/Foundation.h>
#import "DriveData.h"
#import "DriveTrackpointCollection.h"
#import "Mapbox.h"
#import "UserData.h"
#import "RouteSnapshotData.h"
#import "RouteSpecialStageData.h"

@interface DBTransactionManager : NSObject

extern NSString * const Database_dateFormat;

+(int) saveUser:(UserData *) userData;
+(UserData *) getUserDataByFBUserId:(NSString *) fbId;

+(int) saveDriveDataOnStart:(DriveData *) driveData forUser: (int) userId;
//+(int) saveDriveData:(DriveData *) driveData forUser: (int) userId;
+(BOOL) updateDriveDataOnFinish: (DriveData *) driveData;
+(BOOL) updateUserDriveDataAttributes:(DriveData *) driveData;
+(int) saveDriveTrackpoints:(NSArray *) trackpoints andLastSavedTrackpointId: (int) tid;
+(NSArray *) getLightDriveHistoryForUser:(int) userId;
+(DriveData *) getLightDriveDataByDriveId:(int) driveId;
+ (DriveTrackpointData *) getFirstDriveTrackpointByDriveId: (int) driveId;
+ (DriveTrackpointData *) getLastDriveTrackpointByDriveId: (int) driveId;
+(DriveTrackpointData *) getDriveTrackpointByParentId:(int) parentId;
+(DriveTrackpointCollection *) getDriveTrackpointCollectionByDriveId:(int) driveId;
+(DriveTrackpointCollection *) getDriveTrackpointByRegion:(RMSphericalTrapezium) region interval: (int) interval andDriveId: (int) driveId andTranslateToMap: (RMMapView *) mapView;
+(int) countTotalDistanceForUser:(int) userId;

+(BOOL) saveSnapshot:(RouteSnapshotData *) rsd;
+(NSArray *) getSavedSnapshotsForUser:(int) userId;

+(BOOL) saveSpecialStage:(RouteSpecialStageData *) rssd;

+(BOOL) saveExpComponents:(NSMutableArray *) components forUser:(int) userId andDrive: (int) driveId;
+(int) countExperiencePointsForUser:(int) userId;

+(BOOL) updateDBFile;
+(NSString *) getDBPath;

@end