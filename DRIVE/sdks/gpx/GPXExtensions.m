//
//  GPXExtensions.m
//  GPX Framework
//
//  Created by NextBusinessSystem on 12/04/06.
//  Copyright (c) 2012 NextBusinessSystem Co., Ltd. All rights reserved.
//

#import "GPXExtensions.h"
#import "GPXElementSubclass.h"


@interface GPXExtensions()

@property (nonatomic, strong) NSString *courseString;
@property (nonatomic, strong) NSString *speedString;
@property (nonatomic, strong) NSString *timeString;

@end


@implementation GPXExtensions

@synthesize course = _course;
@synthesize speed = _speed;
@synthesize time = _time;

- (NSNumber *)course {
    return [NSNumber numberWithFloat:[GPXType decimal: _courseString]];
}

- (void)setCourse:(NSNumber *)course {
    _courseString = [NSString stringWithFormat:@"%.2f", [course floatValue]];
    //NSLog(@"course: %@ %f", _courseString, [course floatValue]);
}

- (NSNumber *)speed {
    return [NSNumber numberWithFloat:[GPXType decimal:_speedString]];
}

- (void)setSpeed:(NSNumber *)speed {
    _speedString = [NSString stringWithFormat:@"%f", [speed floatValue]];
}

- (NSString *)time {
    return _timeString;
}

- (void)setTime:(NSString *)time {
    _timeString = time;
}

#pragma mark - Instance

- (id)initWithXMLElement:(GPXXMLElement *)element parent:(GPXElement *)parent
{
    self = [super initWithXMLElement:element parent:parent];
    if (self) {
        _courseString = [self textForSingleChildElementNamed:@"course" xmlElement:element];
        _speedString = [self textForSingleChildElementNamed:@"speed" xmlElement:element];
        _timeString = [self textForSingleChildElementNamed:@"time" xmlElement:element];
    }
    return self;
}


#pragma mark - Public methods



#pragma mark - tag

+ (NSString *)tagName {
    return @"extensions";
}


#pragma mark - GPX

- (void)addChildTagToGpx:(NSMutableString *)gpx indentationLevel:(NSInteger)indentationLevel
{
    [super addChildTagToGpx:gpx indentationLevel:indentationLevel];
    
    [self gpx:gpx addPropertyForValue:_courseString tagName:@"course" indentationLevel:indentationLevel];
    [self gpx:gpx addPropertyForValue:_speedString tagName:@"speed" indentationLevel:indentationLevel];
    [self gpx:gpx addPropertyForValue:_timeString tagName:@"time" indentationLevel:indentationLevel];
}

@end
