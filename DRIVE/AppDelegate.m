#import "AppDelegate.h"
#import "AppDataManager.h"
#import <CoreLocation/CoreLocation.h>
#import "DBTransactionManager.h"
#import "FBSDKCoreKit/FBSDKCoreKit.h"
#import "MainViewController.h"
#import "UserData.h"

@implementation AppDelegate

@synthesize userData = _userData;
@synthesize locationManager = _locationManager;
@synthesize coreMotionManager = _coreMotionManager;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    AppDataManager *adm = [AppDataManager appDataManager];
    [adm reset];
    
    if(self.locationManager == nil) {
        _locationManager = [[CLLocationManager alloc] init];
        
        //_locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        _locationManager.distanceFilter = 1;
        
        //if([CLLocationManager locationServicesEnabled]){
            if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_locationManager requestWhenInUseAuthorization];
            }
            [_locationManager startUpdatingLocation];
        //}

        self.locationManager = _locationManager;
    }
    
    if(self.coreMotionManager == nil) {
        _coreMotionManager = [[CMMotionManager alloc] init];
        
        self.coreMotionManager = _coreMotionManager;
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    MainViewController *mainViewController = [[MainViewController alloc] init];
    
    [self.window setRootViewController:mainViewController];
    [self.window makeKeyAndVisible];
    
    //NSMutableArray *arr = [[NSMutableArray alloc] init];
    //arr addObject:<#(id)#>
    
    
    //TMP
    [DBTransactionManager updateDBFile];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    //return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication withSession:self.session];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:application];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"will terminate");
    
    
    //[self.session close];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"applicationDidBecomeActive");
    
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)userDataChangedWith:(UserData *)userData {
    self.userData = userData;
}

@end
